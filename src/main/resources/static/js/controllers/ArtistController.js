angular.module('MetronicApp').controller('ArtistController', function($rootScope, $scope, $http, $timeout, $state, $cookies) {
    $scope.$on('$viewContentLoaded', function() {
        App.initAjax(); // initialize core components
        Layout.setAngularJsSidebarMenuActiveLink('set', $('#sidebar_menu_link_profile'), $state); // set profile link active in sidebar menu
    });

    // set sidebar closed and body solid melyx mode
    $rootScope.settings.layout.pageBodySolid = true;
    $rootScope.settings.layout.pageSidebarClosed = true;

    $('#birthDay').datepicker({
        format: "dd-mm-yyyy"
    });
    $('#deathDay').datepicker({
        format: "dd-mm-yyyy"
    });

    //$scope.ulrImg = "/css/melyx/img/default_avatar.jpg";
    $scope.ulrImg = '';
    $scope.lcStatusCdx = '';
    $http.get("/artistRole/list")
        .then(function(response) {
            $scope.dataArtistRole = response.data;
            console.log(response);
        });

    $http.get("/nationality/list")
        .then(function(response) {
            $scope.dataNationality = response.data;
            console.log(response);
        });

    $http.get("/genre/list")
        .then(function(response) {
            $scope.dataGenre = response.data;
            console.log(response);
        });

    $scope.page = 1;
    //$scope.gender = 'M';
    $scope.artistType='single';

    $scope.getArtistList = function () {
        var srcCategory = document.getElementById("srcSelect").value;
        var srch = "";
        if($scope.mdlSearch != undefined){
            srch = $scope.mdlSearch;
        }
        if($scope.page == 1){
            var offsets = 0;
        }else{
            var offsets = ($scope.page - 1) * 10 ;
        }
        //$scope.loadingStart();
        $http.get("/artist/list",   {
            params: {
                offset: offsets,
                search : srch,
                labelCd : $cookies.get("labelCd"),
                category : srcCategory
            }
        })
            .then(function(response) {
                if ( response.data.code != "0") {
                    $scope.dataError = 'No Data Found';
                }else{
                    $scope.data = response.data.data;
                    $scope.totalPage = Math.ceil(parseInt(response.data.totalSize) / 10)
                }
            }, function(x) {
                $scope.dataError = 'No Data Found';
            });
    }

    $scope.getArtistList2 = function () {
        var srcCategory = document.getElementById("srcSelect").value;
        var srch = "";
        if($scope.mdlSearch != undefined){
            srch = $scope.mdlSearch;
        }
        if($scope.page == 1){
            var offsets = 0;
        }else{
            var offsets = ($scope.page - 1) * 10 ;
        }
        $scope.loadingStart();
        $http.get("/artist/list",   {
            params: {
                offset: offsets,
                search : srch,
                labelCd : $cookies.get("labelCd"),
                category : srcCategory
            }
        })
            .then(function(response) {
                if ( response.data.code != "0") {
                    $scope.dataError = 'No Data Found';
                }else{
                    $scope.data = response.data.data;
                    $scope.totalPage = Math.ceil(parseInt(response.data.totalSize) / 10)
                }
                $scope.loadingStop();
            }, function(x) {
                $scope.loadingStop();
                $scope.dataError = 'No Data Found';
            });
    }
    $scope.artistCollapse = function () {
        console.log($scope.artistName.length)
        if($scope.artistName.length >= 3 ){
            $http.get("/artist/list",   {
                params: {
                    offset: 0,
                    search : $scope.artistName,
                    labelCd : $cookies.get("labelCd"),
                    category : 'artist_name'
                }
            })
                .then(function(response) {
                    if ( response.data.code != "0") {
                        $scope.dataError = 'No Data Found';
                    }else{
                        if(response.data.data.length > 1){
                            $scope.dataCollapse = response.data.data;
                            $(".collapse").collapse('show');
                        }else{
                            $(".collapse").collapse('hide');
                        }
                    }
                }, function(x) {
                    $scope.dataError = 'No Data Found';
                });
        }else{
            $(".collapse").collapse('hide');
        }
    }

    $scope.collapseClick = function (el) {
        alert('Artist dengan nama '+ el +' Sudah terserdia silahkan lanjutkan membuat album ');
        $('#artistModal').modal('hide');
    }

    $scope.loadingStart = function () {
        $('#loaderModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#loaderModal').modal('show');
    }

    $scope.loadingStop = function () {
        $('#loaderModal').modal('hide');
    }

    $scope.getArtistList2();

    $scope.searchButton = function () {
        $scope.page = 1;
        $scope.getArtistList2();
    }

    $scope.getNextPage = function(){
        $scope.page = $scope.page + 1;
        $scope.getArtistList2();
    }

    $scope.getPrevPage = function(){
        $scope.page = $scope.page - 1;
        $scope.getArtistList2();
    }
    $scope.getAwalPage = function(){
        $scope.page = 1;
        $scope.getArtistList2();
    }

    $scope.getAkhirPage = function(){
        $scope.page = $scope.totalPage;
        $scope.getArtistList2();
    }


    $scope.showModalArtist = function (el) {
        $scope.clearForm();
        $scope.isEdit = false;
        if(el == 'New'){
            console.log(el);
            console.log($('#artistModal'))
            $('#artistModal').modal({
                backdrop: 'static',
                keyboard: false
            })
            $scope.isEdit == false;
            $('#artistModal').modal('show');
        }
    }

    $scope.convertDate = function (el) {
        console.log(el)
        var d = new Date(el),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return year + month + day;
    }

    $scope.releaseDate = function (el) {
        if(el == null){
            return null;
        }
        var month = el.substring(4, 6),
            day = el.substring(6, 8),
            year = el.substring(0,4);

        console.log(el)
        console.log(day)
        console.log(month)
        console.log(year)

        var datte = year + "-" + month + "-" + day;
        console.log(datte)
        return new Date(datte);
    }

    $scope.clearForm = function () {
        $scope.artistName = "" ;
        $scope.artistNameOrigin = "" ;
        $scope.artistRole = "" ;
        $scope.genre = "" ;
        $scope.nationality = "NC0100" ;
        $scope.gender = "M" ;
        $scope.artistId = "";
        $scope.birthday = "";
        $scope.deathday = "";
        $scope.twitterURL = "";
        $scope.facebookURL = "";
        $scope.searchKeyword = "";
        $scope.homepage = "";
        $scope.profile = "";
        $scope.ulrImg = "";
        $scope.lcStatusCdx = "";
    }

    $scope.showModalArtistEdit = function (el) {
        // console.log($scope.getImgUrl(el.artistLImgPath));
        $scope.artistName = el.artistName ;
        $scope.artistNameOrigin = el.artistNameOrigin ;
        $scope.artistRole = el.artistRole ;
        $scope.genre = el.genreId ;
        $scope.nationality = el.nationalityCd ;
        $scope.artistRole = el.artistRoleTypeCd;
        $scope.gender = el.gender ;
        $scope.isEdit = true;
        $scope.artistId = el.artistId;
        $scope.birthday = $scope.releaseDate(el.birthday);
        $scope.deathday = $scope.releaseDate(el.deathday);
        $scope.twitterURL = el.twitterUrl;
        $scope.facebookURL = el.facebookUrl;
        $scope.searchKeyword = el.searchKeyword;
        $scope.homepage = el.homepage;
        $scope.profile = el.profile;
        $scope.ulrImg = el.artistLImgPath;
        $scope.lcStatusCdx = el.lcStatusCd;
        var pp =[];
        if(el.performYears != null){
            console.log(el.performYears)
            pp = el.performYears.split(",");
        }

        $scope.performYearFrom = pp[0];
        $scope.performYearTo = pp[1];
        $('#artistModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#artistModal').modal('show');
    }

    $scope.validationForm = function () {

        var rslt = "";

        if($scope.artistName ==  '' || $scope.artistName  == undefined) rslt += "Artist Name \n";
        // if($scope.artistNameOrigin ==  '' || $scope.artistNameOrigin  == undefined) rslt += "Artist Name Origin \n";
        if($scope.artistType ==  '' || $scope.artistType  == undefined) rslt += "Artist Type \n";
        if($scope.artistRole ==  '' || $scope.artistRole  == undefined) rslt += "Artist Role \n";
        if($scope.genre ==  '' || $scope.genre  == undefined) rslt += "Genre \n";
        if($scope.nationality ==  '' || $scope.nationality  == undefined) rslt += "Nasionality \n";
        // if($scope.birthday ==  '' || $scope.birthday  == undefined) rslt += "Birthday \n";
        // if($scope.performYearFrom ==  '' || $scope.performYearFrom  == undefined) rslt += "Perform Years \n";
        // if($scope.performYearTo ==  '' || $scope.performYearTo  == undefined) rslt += "Perform Years \n";
        if($scope.gender ==  '' || $scope.gender  == undefined) rslt += "Gender \n";
        // if($scope.homepage ==  '' || $scope.homepage  == undefined) rslt += "Homepage \n";
        // if($scope.twitterURL ==  '' || $scope.twitterURL  == undefined) rslt += "Twitter Url \n";
        // if($scope.facebookURL ==  '' || $scope.facebookURL  == undefined) rslt += "Facebook Url \n";
        // if($scope.searchKeyword ==  '' || $scope.searchKeyword  == undefined) rslt += "Search Keyword \n";
        // if($scope.profile ==  '' || $scope.profile  == undefined) rslt += "Profile \$scope.profile = "";
        if($scope.ulrImg ==  '' || $scope.ulrImg  == undefined) rslt += "Image \n";
        if($scope.profile ==  '' || $scope.profile  == undefined) rslt += "Profile Artist \n";
        return rslt;
    }

    $scope.saveArtist = function(){
        var birthDay = null;
        var deathDay = null;
        console.log($scope.deathday);
        console.log($scope.homepage)
        if($scope.deathday != "NaNNaNNaN" || $scope.deathday != undefined || $scope.deathday != ""){
            deathDay = $scope.convertDate($scope.deathday);
            if(deathDay =="NaNNaNNaN"){
                deathDay = null;
            }
        }
        if($scope.birthday != "NaNNaNNaN" || $scope.birthday != undefined || $scope.birthday != ""){
            birthDay = $scope.convertDate($scope.birthday);
            if(birthDay =="NaNNaNNaN"){
                birthDay = null;
            }
        }
        if($scope.performYearFrom == undefined){
            $scope.performYearFrom = "";
        }
        if($scope.performYearTo == undefined){
            $scope.performYearTo = "";
        }
        // if($scope.birthday != "" || $scope.birthday != undefined){
        //     birthDay = $scope.convertDate($scope.birthday);
        // }
        // var ulrImg = "";
        // if($scope.ulrImg != "/css/melyx/img/default_avatar.jpg"){
        //     ulrImg = $scope.ulrImg;
        // }

        if($scope.isEdit == false){
            var data = $.param({
                artistName : $scope.artistName,
                artistNameOrigin : $scope.artistNameOrigin,
                artistType :$scope.artistType,
                artistRoleTypeCd : $scope.artistRole,
                genreId : $scope.genre,
                nationalityCd : $scope.nationality,
                birthday : birthDay,
                deathday : deathDay,
                performYears : $scope.performYearFrom +','+$scope.performYearTo,
                gender : $scope.gender,
                homepage : $scope.homepage,
                twitterUrl : $scope.twitterURL,
                facebookUrl : $scope.facebookURL,
                searchKeyword : $scope.searchKeyword,
                profile : $scope.profile,
                artistLImgPath : $scope.ulrImg,
                labelCd : $cookies.get("labelCd"),
                workerId : $cookies.get("username")
            });
        }else{
            var data = $.param({
                artistId : $scope.artistId,
                artistName : $scope.artistName,
                artistNameOrigin : $scope.artistNameOrigin,
                artistType :$scope.artistType,
                artistRoleTypeCd : $scope.artistRole,
                genreId : $scope.genre,
                nationalityCd : $scope.nationality,
                birthday : birthDay,
                deathday : deathDay,
                performYears : $scope.performYearFrom +','+$scope.performYearTo,
                gender : $scope.gender,
                homepage : $scope.homepage,
                twitterUrl : $scope.twitterURL,
                facebookUrl : $scope.facebookURL,
                searchKeyword : $scope.searchKeyword,
                profile : $scope.profile,
                artistLImgPath : $scope.ulrImg,
                labelCd : $cookies.get("labelCd"),
                workerId : $cookies.get("username")
            });
        }

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        var valForm = $scope.validationForm();
        console.log(valForm);
        if(valForm  == '') {
            $scope.loadingStart();
            $http.post('/artist/add', data, config)
                .success(function (data, status, headers, config) {
                    alert(data.message);
                    $scope.loadingStop();
                    $('#artistModal').modal('hide');
                    $scope.getArtistList();

                })
                .error(function (data, status, header, config) {
                    $scope.loadingStop();
                    alert(data.message);
                });
        }else{
            alert("Field : \n"+valForm+"Can't be Empty, Please input !");
        }
    }

    $scope.showImage = function () {
        $('#imageModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#imageModal').modal('show');
    }

    $scope.uploadFile = function(){
        var file = $scope.myFile;
        var img;
        var uploadUrl = "/fileUpload";
        var rslt = {};
        var fileUpload = document.getElementById("upload-photo");

        var reader = new FileReader();

        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
        var isValid;
        if (regex.test(fileUpload.value.toLowerCase())) {
                var reader = new FileReader();

                reader.readAsDataURL(fileUpload.files[0]);
                reader.onload = function (e) {
                    //Initiate the JavaScript Image object.
                    var image = new Image();

                    //Set the Base64 string return from FileReader as source.
                    image.src = e.target.result;

                    //Validate the File Height and Width.
                    image.onload = function () {
                        var height = this.height;
                        var width = this.width;
                        console.log(height);
                        console.log(width);
                        if (height < 100 ) {
                            alert("Height and Width must not exceed 100px.");
                            isValid =  false;
                        }else{
                            //isValid = true;
                            $scope.loadingStart();
                            var fd = new FormData();
                            angular.forEach(file, function (files) {

                                fd.append('file', files);
                            });
                                $http.post(uploadUrl, fd, {
                                    transformRequest: angular.identity,
                                    headers: {'Content-Type': undefined}
                                })

                                    .success(function (data) {
                                        console.log(data)
                                        //rslt = data;
                                        $scope.ulrImg = data.message;
                                        $scope.loadingStop();
                                    })

                                    .error(function (data) {
                                        alert(data.message)
                                        $scope.loadingStop();
                                    });

                        }
                        //alert("Uploaded image has valid Height and Width.");

                    };

                }
        } else {
            alert("Please select a valid Image file.");
            isValid = false;
        }

        // console.log(isValid)
        // if(isValid == true){
        //     if(file != undefined){
        //
        //     }else{
        //         alert("Please Browse the Image File")
        //     }
        // }

    };

    $scope.getImgUrl = function (ss) {
        // if(ss == ''){
        //     return '';
        // }else{
        //     var sp = ss.split("/");
        //     var ssl = "/image/";
        //     if(sp[1] == 'mediaNASX'){
        //         ssl = "/imagex/";
        //     }else if(sp[1] == 'mediaNAS1'){
        //         ssl =  "/image1/";
        //     }
        //     console.log(sp[1]);
        //     return ssl+sp[sp.length - 1];
        // }
        return ss;
    }

    $scope.deleteImg = function () {
        var data = $.param({
            filename : $scope.ulrImg
        });

        var config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        var msg = confirm("Apakah anda ingin menghapus data ini !");
        if(msg == true){
            $scope.loadingStart();
            $http.post('/fileDelete', data, config)
                .success(function (data, status, headers, config) {
                    if(data.code == 1){
                        $scope.ulrImg = "";
                        $scope.getAlbumList();
                    }
                    alert(data.message);
                    $scope.loadingStop();
                })
                .error(function (data, status, header, config) {
                    alert(data.message);
                    $scope.loadingStop();
                });
        }
    }
});
