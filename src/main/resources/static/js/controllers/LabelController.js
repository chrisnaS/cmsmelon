angular.module('MetronicApp').controller('LabelController', function($rootScope, $scope, $http, $timeout, $state, $cookies) {
    $scope.$on('$viewContentLoaded', function() {
        App.initAjax(); // initialize core components
        Layout.setAngularJsSidebarMenuActiveLink('set', $('#sidebar_menu_link_profile'), $state); // set profile link active in sidebar menu
    });

    // set sidebar closed and body solid melyx mode
    $rootScope.settings.layout.pageBodySolid = true;
    $rootScope.settings.layout.pageSidebarClosed = true;
    $scope.page = 1;
    $scope.Mdlsearch = '';
    $scope.isDisable = false;
    $scope.getList = function () {
        if($scope.Mdlsearch == undefined){
            $scope.Mdlsearch = '';
        }
        if($scope.page == 1){
            var offsets = 0;
        }else{
            var offsets = ($scope.page - 1) * 10 ;
        }
        $http.get("/label/mlb/list",   {
            params: {
                offset: offsets,
                search : $scope.Mdlsearch
            }
        })
            .then(function(response) {
                if ( response.data.code != "0") {
                    $scope.dataError = 'No Data Found';
                }else{
                    $scope.data = response.data.data;
                    $scope.totalPage = Math.ceil(parseInt(response.data.totalSize) / 10)
                }
            }, function(x) {
                $scope.dataError = 'No Data Found';
            });
    }

    $scope.getList();

    $scope.loadingStart = function () {
        $('#loaderModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#loaderModal').modal('show');
    }

    $scope.loadingStop = function () {
        $('#loaderModal').modal('hide');
    }
    $scope.searchButton = function () {
        $scope.page = 1;
        $scope.getList();
    }
    $scope.getNextPage = function(){
        $scope.page = $scope.page + 1;
        $scope.getList();
    }
    $scope.getPrevPage = function(){
        $scope.page = $scope.page - 1;
        $scope.getList();
    }
    $scope.getAwalPage = function(){
        $scope.page = 1;
        $scope.getList();
    }

    $scope.getAkhirPage = function(){
        $scope.page = $scope.totalPage;
        $scope.getList();
    }

    $scope.showModalLabel = function () {
        $scope.initialS = "";
        $scope.labelCode = "";
        $scope.labelName = "";
        $scope.isDisable = false;
        $('#labelModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $scope.isEdit = false;
        $('#labelModal').modal('show');
    }
    $scope.showDetail = function (el) {
        $scope.isDisable = true;
        console.log(el);
        $scope.initialS = el.initial;
        $scope.labelCode = el.labelCode;
        $scope.labelName = el.labelName;

        $('#labelModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $scope.isEdit = true;
        $('#labelModal').modal('show');
    }

    $scope.save = function () {
        var data = $.param({
            labelCode : $scope.labelCode,
            labelName: $scope.labelName,
            initial: $scope.initialS
        });

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        $scope.loadingStart();
        $http.post('/label/add', data, config)
            .success(function (data, status, headers, config) {
                alert(data.message);
                $scope.loadingStop();
                $scope.getList();
                $('#labelModal').modal('hide');
                // $scope.getAlbumList();
            })
            .error(function (data, status, header, config) {
                $scope.loadingStop();
                alert(data.message);
                $('#labelModal').modal('hide');
            });
    }
});
