angular.module('MetronicApp').controller('AdminSongController', function($rootScope, $scope, $http, $timeout, $state, ngAudio,$cookies) {
        $scope.$on('$viewContentLoaded', function() {
            App.initAjax(); // initialize core components
            Layout.setAngularJsSidebarMenuActiveLink('set', $('#sidebar_menu_link_profile'), $state); // set profile link active in sidebar menu
        });

        // set sidebar closed and body solid melyx mode

        $scope.isCommercial = 1;

        $rootScope.settings.layout.pageBodySolid = true;
        $rootScope.settings.layout.pageSidebarClosed = true;
        $scope.roleTypes = $cookies.get("roleType");
        $scope.loadingStart = function () {
            $('#loaderModal').modal({
                backdrop: 'static',
                keyboard: false
            })
            $('#loaderModal').modal('show');
        }

        $scope.loadingStop = function () {
            $('#loaderModal').modal('hide');
        }

        $http.get("/genre/list")
            .then(function(response) {
                $scope.dataGenre = response.data;
                console.log(response);
            });

        $http.get("/label/list")
            .then(function(response) {
                $scope.dataLabel = response.data;
                console.log(response);
            });

        $scope.page = 1;
        $scope.isEdit = false;
        $scope.ulrLyric = "";
        $scope.ulrMp3 = "";
        $scope.selectedAll = false;

        $scope.listCategory = [{"id":"song_name","name":"Song Name"},{"id":"song_id","name":"Song ID"},{"id":"label","name":"Label Name"}]
        $scope.categoryList = "song_name";
        $scope.getSongList = function () {
            $scope.selectedAll = false;
            $scope.songIds = [];
            var srch = "";
            var srcCategory = $scope.categoryList;
            if(srcCategory == "label"){
                srch = $scope.mdlLabel;
            }else{
                if($scope.mdlSearch != undefined){
                    srch = $scope.mdlSearch;
                }
            }

            if($scope.page == 1){
                var offsets = 0;
            }else{
                var offsets = ($scope.page - 1) * 10 ;
            }
            //$scope.loadingStart();
            if($scope.isCommercial == 0){
                $scope.isCommercialYn = "Y";
            }else{
                $scope.isCommercialYn = "N";
            }
            $http.get("/adminsong/list",   {
                params: {
                    offset: offsets,
                    search : srch,
                    category : srcCategory,
                    isCommercialYn : $scope.isCommercialYn
                }
            })
                .then(function(response) {
                    if ( response.data.code != "0") {
                        $scope.dataError = 'No Data Found';
                    }else{
                        $scope.data = response.data.data;
                        //console.log($scope.data)
                        $scope.totalPage = Math.ceil(parseInt(response.data.totalSize) / 10)
                    }
                    $scope.loadingStop();
                }, function(x) {
                    $scope.dataError = 'No Data Found';
                    //$scope.loadingStop();
                });
        }

    $scope.getSongList2 = function () {
        $scope.selectedAll = false;
        $scope.songIds = [];
        var srch = "";
        var srcCategory = $scope.categoryList;
        if(srcCategory == "label"){
            srch = $scope.mdlLabel;
        }else{
            if($scope.mdlSearch != undefined){
                srch = $scope.mdlSearch;
            }
        }

        if($scope.page == 1){
            var offsets = 0;
        }else{
            var offsets = ($scope.page - 1) * 10 ;
        }

        if($scope.isCommercial == 0){
            $scope.isCommercialYn = "Y";
        }else{
            $scope.isCommercialYn = "N";
        }
        $scope.loadingStart();
        $http.get("/adminsong/list",   {
            params: {
                offset: offsets,
                search : srch,
                category : srcCategory,
                isCommercialYn : $scope.isCommercialYn
            }
        })
            .then(function(response) {
                if ( response.data.code != "0") {
                    $scope.dataError = 'No Data Found';
                }else{
                    $scope.data = response.data.data;
                    $scope.totalPage = Math.ceil(parseInt(response.data.totalSize) / 10)
                }
                $scope.loadingStop();
            }, function(x) {
                $scope.dataError = 'No Data Found';
                $scope.loadingStop();
            });
    }
        $scope.songIds = [];

        $scope.checkItem = function (el,es) {
            if(es == true){
                $scope.songIds.push(el.songId);
            }else{
                for(els in $scope.songIds){
                    if(el.songId == $scope.songIds[els]){
                        $scope.songIds.splice(els,1);
                    }
                }
            }
            // console.log($scope.songIds);
        }


        $scope.checkAll = function () {
            $scope.songIds = [];
            if ($scope.selectedAll) {
                $scope.selectedAll = true;
                angular.forEach($scope.data, function (item) {
                    if(item.lcStatusCd == 'CS0002' && item.isCommercialYn == 'N'){
                        item.Selected = $scope.selectedAll;
                        console.log(item.songId);
                        if($scope.selectedAll == true){
                            $scope.songIds.push(item.songId);
                        }else{
                            $scope.songIds = [];
                        }
                    }
                });
            } else {
                $scope.selectedAll = false;
                $scope.songIds = [];
            }
            console.log($scope.songIds);
        };

        // $scope.ApproveCheck = function () {
        //
        // }

        $scope.getSongList2();
        $scope.isLabel = 0;
        $scope.changeCategory = function () {
            if($scope.categoryList == 'label'){
                $scope.isLabel = 1;
            }else{
                $scope.isLabel = 0;
            }
        }
        $scope.enterSearch = function(el){
            //console.log(el);
            if(el.keyCode == 13){
                $scope.getSongList2();
            }
        }
        $scope.searchButton = function () {
            $scope.page = 1;
            $scope.getSongList2();
        }

        $scope.getNextPage = function(){
            $scope.page = $scope.page + 1;
            $scope.getSongList2();
        }

        $scope.getPrevPage = function(){
            $scope.page = $scope.page - 1;
            $scope.getSongList2();
        }
        $scope.getAwalPage = function(){
            $scope.page = 1;
            $scope.getSongList2();
        }

        $scope.getAkhirPage = function(){
            $scope.page = $scope.totalPage;
            $scope.getSongList2();
        }

        $scope.clearForm = function (){
            $scope.songId = '';
            $scope.songName = '';
            $scope.songNameOriginal = '';
            $scope.albumName = '';
            $scope.albumId = '';
            $scope.artistName = '';
            $scope.artistId = '';
            $scope.mlb = '';
            $scope.genre = '';
            $scope.issueDate = '';
            $scope.hitSong = 'N';
            $scope.adultSong = 'N';
            $scope.mp3Download = 'N';
            $scope.stream = 'N';
            $scope.originalFileName = "";
            $scope.ulrMp3 = "";
            $scope.ulrLyric = "";
            $scope.songIdx = "";
            $scope.grid = "";
            $scope.hint = "";
            $scope.trackNo = "";
            $scope.nonDrmPrice = "";
            $scope.mp3Download = "";
            $scope.stream = "";
            $scope.labelCdx = "";
        }

        $scope.showModalEdit = function (el) {

            $scope.adultYn1 = el.adultYn;
            $scope.albumId1 = el.albumId;
            $scope.albumName1 = el.albumName;
            $scope.albumRegDate1 = el.albumRegDate;
            $scope.artistId1 = el.artistId;
            $scope.artistName1 = el.artistName;
            $scope.channelGroupCd1 = el.channelGroupCd;
            $scope.cp1Cd1 = el.cp1Cd;
            $scope.cp2Cd1 = el.cp2Cd;
            $scope.cp3Cd1 = el.cp3Cd;
            $scope.cp4Cd1 = el.cp4Cd;
            $scope.cp5Cd1 = el.cp5Cd;
            $scope.diskNo1 = el.diskNo;
            $scope.drmPaymentProdId1 = el.drmPaymentProdId;
            $scope.drmPrice1 = el.drmPrice;
            $scope.fullFilePath1 = el.fullFilePath;
            $scope.genidCd1 = el.genidCd;
            $scope.genreId1 = el.genreId;
            $scope.genreName1 = el.genreName;
            $scope.searchKeywordSong = el.searchKeyword;
            $scope.hint1 = el.hint;
            $scope.hitSong1 = el.hitSong;
            $scope.hitSongYn1 = el.hitSongYn;
            $scope.isCommercialYn1 = el.isCommercialYn;
            $scope.issueDate1 = el.issueDate;
            $scope.issueDateStr1 = el.issueDateStr;
            $scope.labelCd1 = el.labelCd;
            $scope.labelName1 = el.labelName;
            $scope.lcStatusCd1 = el.lcStatusCd;
            $scope.lyricPath1 = el.lyricPath;
            $scope.mlbCd1 = el.mlbCd;
            $scope.mod1 = el.mod;
            $scope.modYn1 = el.modYn;
            $scope.nonDrmPaymentProdId1 = el.nonDrmPaymentProdId;
            $scope.nonDrmPrice1 = el.nonDrmPrice;
            $scope.orderIssueDate1 = el.orderIssueDate;
            $scope.originalFileName1 = el.originalFileName;
            $scope.playTime1 = el.playTime;
            $scope.rbt1 = el.rbt;
            $scope.rbtYn1 = el.rbtYn;
            $scope.regDate1 = el.regDate;
            $scope.regDateStr1 = el.regDateStr;
            $scope.rt1 = el.rt;
            $scope.rtYn1 = el.rtYn;
            $scope.searchKeyword1 = el.searchKeyword;
            $scope.sellAlacarteYn1 = el.sellAlacarteYn;
            $scope.sellDrmYn1 = el.sellDrmYn;
            $scope.sellNonDrmYn1 = el.sellNonDrmYn;
            $scope.sellStreamYn1 = el.sellStreamYn;
            $scope.slfLyric1 = el.slfLyric;
            $scope.slfLyricYn1 = el.slfLyricYn;
            $scope.songId1 = el.songId;
            $scope.songName1 = el.songName;
            $scope.songNameOrigin1 = el.songNameOrigin;
            $scope.textLyric1 = el.textLyric;
            $scope.textLyricYn1 = el.textLyricYn;
            $scope.titleSong1 = el.titleSong;
            $scope.titleSongYn1 = el.titleSongYn;
            $scope.trackNo1 = el.trackNo;
            $scope.updDate1 = el.updDate;
            $scope.vod1 = el.vod;
            $scope.vodYn1 = el.vodYn;
            $scope.workerId1 = el.workerId;


            $('#songsAdminDetailModal').modal({
                backdrop: 'static',
                keyboard: false
            })
            $('#songsAdminDetailModal').modal('show');
        }
        $scope.showFormSearchAlbum = function () {
            $('#albumSearchModal').modal({
                backdrop: 'static',
                keyboard: false
            })
            $('#albumSearchModal').modal('show');
            $scope.getAlbumListSearch();
        }

        $scope.showFormSearchArtist = function () {
            $('#artistSearchModal').modal({
                backdrop: 'static',
                keyboard: false
            })
            $('#artistSearchModal').modal('show');
            $scope.getArtistListSearch();
        }

        $scope.getAlbumListSearch = function () {
            var srch = document.getElementById("mdlAlbumSrcTxt").value;
            console.log(srch)
            var offsets = 0;

            console.log(offsets)
            $http.get("/album/list",   {
                params: {
                    offset: offsets,
                    search : srch,
                    labelCd : $cookies.get("labelCd"),
                    category : 'album_name'
                }
            })
                .then(function(response) {
                    if ( response.data.code != "0") {
                        $scope.dataAlbumError = 'No Data Found';
                    }else{
                        $scope.dataAlbum = response.data.data;
                        //$scope.totalPage = Math.ceil(parseInt(response.data.totalSize) / 10)
                    }
                }, function(x) {
                    $scope.dataAlbumError = 'No Data Found';
                });
        }

        $scope.getArtistListSearch = function () {
            var srch = '';
            if($scope.mdlArtistSrcTxt != undefined){
                srch = $scope.mdlArtistSrcTxt;
            }
            console.log(srch)

            var offsets = 0;

            console.log(offsets)
            $http.get("/artist/list",   {
                params: {
                    offset: offsets,
                    search : srch,
                    category : 'artist_name'
                }
            })
                .then(function(response) {
                    if ( response.data.code != "0") {
                        $scope.dataArtistError = 'No Data Found';
                    }else{
                        $scope.dataArtist = response.data.data;
                        //$scope.totalPage = Math.ceil(parseInt(response.data.totalSize) / 10)
                    }
                }, function(x) {
                    $scope.dataArtistError = 'No Data Found';
                });
        }

        $scope.convertDate = function (el) {
            var d = new Date(el),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return year + month + day;
        }

        $scope.validationForm = function () {

            var rslt = "";

            if($scope.songName == '' ||$scope.songName == undefined) rslt += "Song Name \n";
            // if($scope.songNameOriginal == '' ||$scope.songNameOriginal == undefined) rslt += "Song Name Original \n";
            if($scope.albumId == '' ||$scope.albumId == undefined) rslt += "Album \n";
            if($scope.artistId == '' ||$scope.artistId == undefined) rslt += "Artist \n";
            if($scope.mlb == '' ||$scope.mlb == undefined) rslt += "Mlb \n";
            if($scope.genre == '' ||$scope.genre == undefined) rslt += "Genre \n";
            if($scope.issueDate == '' || $scope.issueDate == undefined) rslt += "Issue Date \n";
            if($scope.grid == '' ||$scope.grid == undefined) rslt += "ISRC \n";
            if($scope.hint == '' ||$scope.hint == undefined) rslt += "Hint \n";
            if($scope.trackNo == '' ||$scope.trackNo == undefined) rslt += "Track No \n";
            if($scope.nonDrmPrice == '' ||$scope.nonDrmPrice == undefined) rslt += "DRM Price \n";

            return rslt;
        }

        $scope.saveSong = function(){
            var IssueDate = $scope.convertDate($scope.issueDate);
            if($scope.isCommercial == 0){
                $scope.isCommercialYn = "Y";
            }else{
                $scope.isCommercialYn = "N";
            }
            var textLyric = 'Y';
            if($scope.ulrLyric == '' || $scope.ulrLyric == undefined){
                textLyric = 'N';
            }

            if($scope.isEdit == false){
                var data = $.param({
                    //songId : $scope.songId,
                    songName : $scope.songName,
                    songNameOrigin : $scope.songNameOriginal,
                    albumId :$scope.albumId,
                    artistId : $scope.artistId,
                    mlbCd : $scope.mlb,
                    genreId : $scope.genre,
                    issueDate : IssueDate,
                    hitSongYn : $scope.hitSong,
                    adultYn : $scope.adultSong,
                    isCommercialYn :  $scope.isCommercialYn,
                    sellNonDrmYn : $scope.mp3Download,
                    sellStreamYn : $scope.stream,
                    originalFileName : $scope.originalFileName,
                    fullFilePath : $scope.ulrMp3,
                    lyricPath : $scope.ulrLyric,
                    genidCd : $scope.grid,
                    hint : $scope.hint,
                    trackNo : $scope.trackNo,
                    nonDrmPrice : $scope.nonDrmPrice,
                    labelCd : $cookies.get("labelCd"),
                    workerId : $cookies.get("username"),
                    textLyricYn : textLyric
                });
            }else{
                var data = $.param({
                    songId : $scope.songId,
                    songName : $scope.songName,
                    songNameOrigin : $scope.songNameOriginal,
                    albumId :$scope.albumId,
                    artistId : $scope.artistId,
                    mlbCd : $scope.mlb,
                    genreId : $scope.genre,
                    issueDate : IssueDate,
                    hitSongYn : $scope.hitSong,
                    adultYn : $scope.adultSong,
                    isCommercialYn :  $scope.isCommercialYn,
                    originalFileName : $scope.originalFileName,
                    fullFilePath : $scope.ulrMp3,
                    lyricPath : $scope.ulrLyric,
                    genidCd : $scope.grid,
                    hint : $scope.hint,
                    trackNo : $scope.trackNo,
                    nonDrmPrice : $scope.nonDrmPrice,
                    sellNonDrmYn : $scope.mp3Download,
                    sellStreamYn : $scope.stream,
                    labelCd : $cookies.get("labelCd"),
                    workerId : $cookies.get("username"),
                    textLyricYn : textLyric
                });
            }

            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }
            var valForm = $scope.validationForm();
            console.log(valForm);
            if(valForm  == '') {
                $scope.loadingStart();
                $http.post('/song/add', data, config)
                    .success(function (data, status, headers, config) {
                        alert(data.message);
                        $('#songsModal').modal('hide');
                        $scope.getSongList();
                        $scope.loadingStop();
                    })
                    .error(function (data, status, header, config) {
                        alert(data.message);
                        $scope.loadingStop();
                    });
            }else{
                alert("Field : \n"+valForm+"Can't be Empty, Please input !");
            }
        }

        $scope.addAlbumId = function (el) {
            console.log(el);
            $scope.albumId = el.albumId;
            $scope.albumName = el.albumName;
            $('#albumSearchModal').modal('hide');
        }

        $scope.addArtistId = function (el) {
            $scope.artistId = el.artistId;
            $scope.artistName = el.artistName;
            $('#artistSearchModal').modal('hide');
        }

        $scope.tabClick = function (el) {
            $scope.isCommercial = el;
            $scope.getSongList2();
            console.log( $scope.isCommercial)
        }

        $scope.getSongModList = function (el) {
            $http.get("/songMod/list?songId="+el)
                .then(function(response) {
                    $scope.songModList = response.data;
                    console.log(response);

                });
        }
        $scope.showEncodeModal = function (el) {
            $('#encoderModal').modal({
                backdrop: 'static',
                keyboard: false
            })
            $scope.getSongModList(el);
            $('#encoderModal').modal('show');
        }

        $scope.uploadFileMp3 = function(){
            var file = $scope.myFile;

            console.log('file is ' );
            console.dir(file);

            var uploadUrl = "/fileUpload";
            var rslt = {};
            if(file != undefined){
                $scope.loadingStart();
                angular.forEach(file, function (files) {
                    console.log(files)
                    var fd = new FormData();
                    fd.append('file', files);
                    $scope.loadingStart();
                    $http.post(uploadUrl, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    })

                        .success(function (data) {
                            console.log(data)
                            //rslt = data;
                            $scope.ulrMp3 = data.message;
                            $scope.originalFileName = data.message;
                            $scope.loadingStop();
                        })

                        .error(function () {
                            $scope.loadingStop();
                        });
                });
            }else{
                alert("Please Browse the MP3 File")
            }

            // console.log(rslt)
            //
            // //var dataUpload = fileUpload.uploadFileToUrl(file, uploadUrl);
            // //console.log(dataUpload);
            // $scope.ulrMp3 = "/upload/music" + rslt.message;
        };


        $scope.uploadFileLyric = function(){
            console.log($scope.myFile)
            var file = $scope.myFile;

            console.log('file is ' );
            console.dir(file);

            var uploadUrl = "/fileUpload";
            var rslt = {};
            if(file != undefined){
                $scope.loadingStart();
                angular.forEach(file, function (files) {
                    console.log(files)
                    var fd = new FormData();
                    fd.append('file', files);

                    $http.post(uploadUrl, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    })

                        .success(function (data) {
                            console.log(data)
                            //rslt = data;
                            $scope.ulrLyric = data.message;
                            $scope.loadingStop();
                        })

                        .error(function () {
                            $scope.loadingStop();
                        });
                });
            }else{
                alert("Please Browse the Lyric File")
            }
        };
        //$scope.lagu="/sjdsd.mp3";
        $scope.getMusicUrl = function (ss) {
            if(ss == ''){
                return '';
            }else{
                // var sp = ss.split("/");
                // var sp = ss.split("/");
                // var ssl = "/music/";
                // if(sp[1] == 'mediaNASX'){
                //     ssl = "/musix/";
                //     return ssl+sp[sp.length - 3]+'/'+sp[sp.length - 2]+'/'+sp[sp.length - 1];
                // }else if(sp[1] == 'mediaNAS1'){
                //     ssl =  "/image1/";
                // }
                // return ssl+sp[sp.length - 1];

                return ss;
            }
        }

        $scope.isPlay = 0;
        $scope.playSong = function (el) {
            var urlMusic = $scope.getMusicUrl(el);
            $scope.audio = ngAudio.load(urlMusic);
            $scope.isPlay = 1;
            if($scope.audio.paused){
                $scope.audio.pause();
            }else{
                $scope.audio.play();
            }

            console.log($scope.audio);
        }

        $scope.stopSong = function () {
            $scope.isPlay = 0;
            $scope.audio.pause();
        }

        $scope.saveEncoder = function (el,filePath) {
            var data = $.param({
                songModId : el.songModId,
                songId : el.songId,
                sampling :el.sampling,
                codecTypeCd : el.codecTypeCd,
                bitRateCd : el.bitRateCd,
                playTime : el.playTime,
                fileSize : el.fileSize,
                originalFileName : filePath,
                fullFilePath : filePath,
                workerId :  el.workerId,
                status :  el.status
            });

            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }
            $scope.loadingStart();
            $http.post('/songMod/add', data, config)
                .success(function (data, status, headers, config) {
                    // alert(data.message);
                    // $('#songsModal').modal('hide');
                    $scope.getSongModList(el.songId);
                    //$scope.getSongList();
                    $scope.loadingStop();
                })
                .error(function (data, status, header, config) {
                    alert(data.message);
                    $scope.loadingStop();
                });
        }
        $scope.encoder = function (el) {
            if($scope.ulrMp3 != ''){
                var type_bit,targetx;
                if(el.codecTypeCd == 'CT0001'){
                    targetx = 'mp3';
                }else if(el.codecTypeCd == 'CT0007'){
                    targetx ='m4a';
                }else if(el.codecTypeCd == 'CT0008'){
                    targetx = 'flac';
                }else if(el.codecTypeCd == 'CT0002'){
                    targetx = 'm4a';
                }


                if(el.bitRateCd == "BR0320"){
                    type_bit = "320";
                }else if(el.bitRateCd == "BR0128"){
                    type_bit = "128";
                }else if(el.bitRateCd == "BR0192"){
                    type_bit = "192";
                }else if(el.bitRateCd == "BR0096"){
                    type_bit = "96";
                }else if(el.bitRateCd == "BR1411"){
                    type_bit = "1411";
                }else if(el.bitRateCd == "BR0256"){
                    type_bit = "256";
                }
                //console.log(type_bit);
                $scope.loadingStart();
                $http.get("http://cmsv2.melon.co.id/encoder/encode_wav",{
                    params: {
                        song_path: $scope.ulrMp3x,
                        bit_rate : type_bit,
                        song_id:el.songId,
                        target : targetx
                    }
                })
                    .success(function (response) {
                        console.log(response);
                        if(response.length <= 70){
                            if(response != "-1"){
                                $scope.saveEncoder(el,response);
                            }
                        }else{
                            alert('Encode Gagal')  ;
                        }
                        $scope.loadingStop();
                    })

                    .error(function (data) {
                        console.log(response);
                        alert('Encode Gagal')  ;
                        $scope.loadingStop();
                    });
            }
        }

        $scope.ApproveSong = function (el) {
            var data = $.param({
                songId : el
            });

            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }
            $scope.loadingStart();

            $http.post('/song/approve', data, config)
                .success(function (data, status, headers, config) {
                    alert(data.message);
                    $scope.getSongList();
                    $scope.loadingStop();

                })
                .error(function (data, status, header, config) {
                    $scope.loadingStop();
                    alert(data.message);
                });
        }


    $scope.DeleteSong = function (el) {
        var data = $.param({

        });

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        $scope.loadingStart();

        $http.post('/song/delete/'+el, data, config)
            .success(function (data, status, headers, config) {
                alert(data.message);
                $scope.getSongList();
                $scope.loadingStop();

            })
            .error(function (data, status, header, config) {
                $scope.loadingStop();
                alert(data.message);
            });
    }

    $scope.approveBulk = function () {
        var songId = $scope.convertSongId();

        var data = $.param({
            songIds : songId
        });

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        $scope.loadingStart();
        $http.post('/song/approvebulk', data, config)
            .success(function (data, status, headers, config) {
                alert(data.message);
                $scope.getSongList();
                $scope.loadingStop();
            })
            .error(function (data, status, header, config) {
                alert(data.message);
                $scope.getSongList();
                $scope.loadingStop();
            });
    }

        $scope.convertSongId = function () {
            var msg = '';
           if($scope.songIds.length > 0) {
               for(els in $scope.songIds){
                   msg += $scope.songIds[els] + "-";
               }
           }
           return msg;
        }
});
