angular.module('MetronicApp').controller('MlbController', function($rootScope, $scope, $http, $timeout, $state, $cookies) {
    $scope.$on('$viewContentLoaded', function() {
        App.initAjax(); // initialize core components
        Layout.setAngularJsSidebarMenuActiveLink('set', $('#sidebar_menu_link_profile'), $state); // set profile link active in sidebar menu
    });

    // set sidebar closed and body solid melyx mode
    $rootScope.settings.layout.pageBodySolid = true;
    $rootScope.settings.layout.pageSidebarClosed = true;
    $scope.page = 1;
    $scope.Mdlsearch = '';
    $scope.listMlbCodes = [];
    $scope.getDataLabel = function () {
        $http.get("/mlb_code/getlabel")
            .then(function(response) {
                $scope.dataMlbLabel = response.data;
                $scope.syncMlbList();
                console.log(response);
            });
    }
    $scope.getDataLabel();

    $scope.getMlbDetail = function (el) {
        $http.get("/mlb_code/detail",{
            params: {
                txId: el
            }
        })
            .then(function(response) {
                $scope.listMlbCodes = response.data;
            });
    }

    $scope.getList = function () {
        if($scope.Mdlsearch == undefined){
            $scope.Mdlsearch = '';
        }
        if($scope.page == 1){
            var offsets = 0;
        }else{
            var offsets = ($scope.page - 1) * 10 ;
        }
        $http.get("/mlb_code/list",   {
            params: {
                offset: offsets,
                search : $scope.Mdlsearch
            }
        })
            .then(function(response) {
                if ( response.data.code != "0") {
                    $scope.dataError = 'No Data Found';
                }else{
                    $scope.data = response.data.data;
                    $scope.totalPage = Math.ceil(parseInt(response.data.totalSize) / 10)
                }
            }, function(x) {
                $scope.dataError = 'No Data Found';
            });
    }

    $scope.getList();

    $scope.loadingStart = function () {
        $('#loaderModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#loaderModal').modal('show');
    }

    $scope.loadingStop = function () {
        $('#loaderModal').modal('hide');
    }
    $scope.searchButton = function () {
        $scope.page = 1;
        $scope.getList();
    }
    $scope.getNextPage = function(){
        $scope.page = $scope.page + 1;
        $scope.getList();
    }
    $scope.getPrevPage = function(){
        $scope.page = $scope.page - 1;
        $scope.getList();
    }
    $scope.getAwalPage = function(){
        $scope.page = 1;
        $scope.getList();
    }

    $scope.getAkhirPage = function(){
        $scope.page = $scope.totalPage;
        $scope.getList();
    }

    $scope.showModalMlb = function () {
        $scope.mdlJumlah = "";
        $scope.mdlMlbCode = "";
        $scope.listMlbCodes = [];

        $('#mlbCodeModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $scope.isEdit = false;
        $('#mlbCodeModal').modal('show');
}
    $scope.showDetail = function (el) {
        $scope.getMlbDetail(el);
        $('#mlbCodeModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $scope.isEdit = true;
        $('#mlbCodeModal').modal('show');
    }

    $scope.pushMlbCode = function () {
        var rr = {
            "labelCode":$scope.mdlMlbCode.labelCode,
            "labelName":$scope.mdlMlbCode.labelName,
            "jumlah":$scope.mdlJumlah
        }
        $scope.listMlbCodes.push(rr);
        $scope.getDataLabel();
        //$scope.syncMlbList();
    }

    $scope.delMlbCode = function (el) {

        $scope.listMlbCodes.splice(el,1);
        $scope.getDataLabel();
        //$scope.syncMlbList();
    }

    $scope.syncMlbList = function () {
        //$scope.getDataLabel();
        // console.log("xxxxxxxxxxxxx")
        angular.forEach($scope.listMlbCodes, function (list) {
            var i = 0;
            angular.forEach($scope.dataMlbLabel, function (lists) {

                if(list.labelCode == lists.labelCode){
                    $scope.dataMlbLabel.splice(i,1);
                    console.log($scope.dataMlbLabel)
                }
                i = i+1;
            })
        })
    }

    $scope.generateParams = function () {
        var xx = "";
        if($scope.listMlbCodes.length <= 0){
            xx = "";
        }else{
            var i = 1;
            angular.forEach($scope.listMlbCodes, function (list) {
                xx += list.labelCode+"#"+list.jumlah+"_";
            })
        }
        return xx.substr(0, xx.length-1);
    }

    $scope.downloadExcel = function (el) {
        window.open(el);
    }
    $scope.save = function () {
        var xx = $scope.generateParams();
        // console.log(xx)
        if(xx == ''){
            alert('Label tidak boleh kosong')
        }else{
            var data = $.param({
                params : xx
            });

            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }
            $scope.loadingStart();
            $http.post('/mlb_code/add', data, config)
                .success(function (data, status, headers, config) {
                    alert(data.message);
                    $scope.loadingStop();
                    $scope.getList();
                    $('#mlbCodeModal').modal('hide');
                    // $scope.getAlbumList();
                })
                .error(function (data, status, header, config) {
                    $scope.loadingStop();
                    alert(data.message);
                    $('#mlbCodeModal').modal('hide');
                });
        }
    }
});
