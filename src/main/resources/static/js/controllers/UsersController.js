angular.module('MetronicApp').controller('UsersController', function($rootScope, $scope, $http, $timeout, $state, $cookies) {
    $scope.$on('$viewContentLoaded', function() {
        App.initAjax(); // initialize core components
        Layout.setAngularJsSidebarMenuActiveLink('set', $('#sidebar_menu_link_profile'), $state); // set profile link active in sidebar menu
    });

    // set sidebar closed and body solid melyx mode
    $rootScope.settings.layout.pageBodySolid = true;
    $rootScope.settings.layout.pageSidebarClosed = true;
    $scope.page = 1;
    $http.get("/label/list")
        .then(function(response) {
            $scope.dataLabel = response.data;
            console.log(response);
        });
    $scope.roleTypeData = [
        {id:"user",name:"User"},
        {id:"admin",name:"Admin"},
        {id:"superuser",name:"Super User"}
    ]
    $scope.getUserList = function () {

        var srcCategory = document.getElementById("srcSelect").value;
        var srch = "";
        if($scope.mdlSearch != undefined){
            srch = $scope.mdlSearch;
        }
        console.log($scope.mdlSearch)
        if($scope.page == 1){
            var offsets = 0;
        }else{
            var offsets = ($scope.page - 1) * 10 ;
        }
        console.log(offsets)
        $http.get("/users/list",   {
            params: {
                offset: offsets,
                search : srch
            }
        })
            .then(function(response) {
                if ( response.data.code != "0") {
                    $scope.dataError = 'No Data Found';
                }else{
                    $scope.data = response.data.data;
                    $scope.totalPage = Math.ceil(parseInt(response.data.totalSize) / 10)
                }
            }, function(x) {
                $scope.dataError = 'No Data Found';
            });
    }
    $scope.getUserList();

    $scope.loadingStart = function () {
        $('#loaderModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#loaderModal').modal('show');
    }

    $scope.loadingStop = function () {
        $('#loaderModal').modal('hide');
    }



    $scope.searchButton = function () {
        $scope.page = 1;
        $scope.getUserList();
    }

    $scope.getNextPage = function(){
        $scope.page = $scope.page + 1;
        $scope.getUserList();
    }

    $scope.getPrevPage = function(){
        $scope.page = $scope.page - 1;
        $scope.getUserList();
    }
    $scope.getAwalPage = function(){
        $scope.page = 1;
        $scope.getUserList();
    }

    $scope.getAkhirPage = function(){
        $scope.page = $scope.totalPage;
        $scope.getUserList();
    }


    $scope.showModalArtist = function (el) {
        $scope.clearForm();
        $scope.isEdit = false;
            $('#UserModal').modal({
                backdrop: 'static',
                keyboard: false
            })
            $scope.isEdit == false;
            $('#UserModal').modal('show');
    }

    $scope.clearForm = function () {
        $scope.userId = "" ;
        $scope.username = "" ;
        $scope.password = "" ;
        $scope.fullname = "" ;
        $scope.label = "" ;
        $scope.roleType = "" ;
    }

    $scope.showModalArtistEdit = function (el) {
        $scope.userId = el.userId ;
        $scope.username = el.username ;
        $scope.password = el.password ;
        $scope.fullname = el.fullname ;
        $scope.label = el.labelCd ;
        $scope.roleType = el.roleType ;
        $scope.isEdit = true;
        $('#UserModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#UserModal').modal('show');
    }

    $scope.validationForm = function () {

        var rslt = "";

        if($scope.username ==  '' || $scope.username  == undefined) rslt += "Username \n";
        if($scope.password ==  '' || $scope.password  == undefined) rslt += "Password \n";
        if($scope.fullname ==  '' || $scope.fullname  == undefined) rslt += "Fullname \n";
        if($scope.label ==  '' || $scope.label  == undefined) rslt += "Label \n";
        if($scope.roleType ==  '' || $scope.roleType  == undefined) rslt += "Role Type \n";
        return rslt;
    }

    $scope.saveUser = function(){

        if($scope.isEdit == false){
            var data = $.param(  {
                "username": $scope.username,
                "password": $scope.password,
                "fullname": $scope.fullname,
                "labelCd": $scope.label,
                "roleType": $scope.roleType
            });
        }else{
            var data = $.param( {
                "userId": $scope.userId,
                "username": $scope.username,
                "password": $scope.password,
                "fullname": $scope.fullname,
                "labelCd": $scope.label,
                "roleType": $scope.roleType
            });
        }

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        var valForm = $scope.validationForm();
        console.log(valForm);
        if(valForm  == '') {
            $scope.loadingStart();
            $http.post('/user/add', data, config)
                .success(function (data, status, headers, config) {
                    alert(data.message);
                    $scope.loadingStop();
                    $('#UserModal').modal('hide');
                    $scope.getUserList();

                })
                .error(function (data, status, header, config) {
                    $scope.loadingStop();
                    alert(data.message);
                });
        }else{
            alert("Field : \n"+valForm+"Can't be Empty, Please input !");
        }
    }

});
