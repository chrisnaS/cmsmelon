angular.module('MetronicApp').controller('ChangePasswordController', function($rootScope, $scope, $http, $timeout, $state, ngAudio,$cookies) {
    $scope.$on('$viewContentLoaded', function() {
        App.initAjax(); // initialize core components
        Layout.setAngularJsSidebarMenuActiveLink('set', $('#sidebar_menu_link_profile'), $state); // set profile link active in sidebar menu
    });

    // set sidebar closed and body solid melyx mode
    $rootScope.settings.layout.pageBodySolid = true;
    $rootScope.settings.layout.pageSidebarClosed = true;
    $scope.loadingStart = function () {
        $('#loaderModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#loaderModal').modal('show');
    }

    $scope.loadingStop = function () {
        $('#loaderModal').modal('hide');
    }
    $http.get("/users/"+$cookies.get("username"))
        .then(function(response) {
            $scope.userlist = response.data;
            console.log($scope.userlist);
            $scope.oldPass = $scope.userlist.userPassword;
        });

    $scope.chkClick = function () {
        var x = document.getElementById("inputPassword3");
        var y = document.getElementById("inputPassword4");
        if (x.type === "password" && y.type === "password") {
            x.type = "text";
            y.type = "text";
        } else {
            x.type = "password";
            y.type = "password";
        }
    }

    $scope.save = function () {
        var data = $.param({
            userId : $scope.userlist.id,
            newPass: $scope.newPass
        });

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        $scope.loadingStart();
        $http.post('/user/changePass', data, config)
            .success(function (data, status, headers, config) {
                alert(data.message);
                $scope.loadingStop();
                $cookies.remove('isLoginYN');
                $cookies.remove('labelCd');
                $cookies.remove('username');
                location.reload();
                // $scope.getAlbumList();
            })
            .error(function (data, status, header, config) {
                $scope.loadingStop();
                alert(data.message);
            });
    }
});
