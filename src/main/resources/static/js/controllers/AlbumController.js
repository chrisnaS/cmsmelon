angular.module('MetronicApp').controller('AlbumController', function($rootScope, $scope, $http,ngAudio, $timeout, $state, $cookies) {
    $scope.$on('$viewContentLoaded', function() {
        App.initAjax(); // initialize core components
        Layout.setAngularJsSidebarMenuActiveLink('set', $('#sidebar_menu_link_profile'), $state); // set profile link active in sidebar menu
    });

    // set sidebar closed and body solid melyx mode
    $rootScope.settings.layout.pageBodySolid = true;
    $rootScope.settings.layout.pageSidebarClosed = true;
    $('[data-toggle="tooltip"]').tooltip();
    $('#releaseDate').datepicker({
        format: "dd-mm-yyyy"
    });
    $scope.lcStatusCdx = '';
    $scope.loadingStart = function () {
        $('#loaderModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#loaderModal').modal('show');
    }
    $scope.ulrMp3x = '';
    $scope.ulrImg = "";
    $http.get("/albumType/list")
        .then(function(response) {
            $scope.dataAlbumType = response.data;
            console.log(response);
        });

    $http.get("/genre/list")
        .then(function(response) {
            $scope.dataGenre = response.data;
            console.log(response);
        });

    $scope.getAlbumArtist = function (el) {
        $scope.AlbumArtistList = [];
        $http.get("/album/artist?albumId="+el)
            .then(function(response) {
                var albumArtiistListx = response.data;
                if(albumArtiistListx.length > 0){
                    angular.forEach(albumArtiistListx, function (el) {
                        $scope.AlbumArtistList.push({artistId: el.artistId,artistName:el.artistName});
                    });
                }
            });
    }
    $scope.domesticYn = "Y";
    $scope.adultYn = "Y";
    $scope.isEdit = false;
    $scope.page = 1;
    $scope.isSongForm = 0;

    $scope.AlbumArtistList = [];
    $scope.AlbumArtist = {artistId:'',artistName:''};
    $scope.getAlbumList = function () {
        var srcCategory = document.getElementById("srcSelect").value;
        var srch = "";
        if($scope.mdlSearch != undefined){
            srch = $scope.mdlSearch;
        }
        console.log($scope.mdlSearch)
        if($scope.page == 1){
            var offsets = 0;
        }else{
            var offsets = ($scope.page - 1) * 10 ;
        }
        //$scope.loadingStart();
        $http.get("/album/list",   {
            params: {
                offset: offsets,
                search : srch,
                labelCd : $cookies.get("labelCd"),
                category : srcCategory,
                lcStatusCd : ''
            }
        })
            .then(function(response) {
                if ( response.data.code != "0") {
                    $scope.dataError = 'No Data Found';
                }else{
                    $scope.data = response.data.data;
                    //console.log($scope.data)
                    $scope.totalPage = Math.ceil(parseInt(response.data.totalSize) / 10)
                }
                //$scope.loadingStop();
            }, function(x) {
                $scope.dataError = 'No Data Found';
                //$scope.loadingStop();
            });
    }

    $scope.getAlbumList2 = function () {
        var srcCategory = document.getElementById("srcSelect").value;
        var srch = "";
        if($scope.mdlSearch != undefined){
            srch = $scope.mdlSearch;
        }
        console.log($scope.mdlSearch)
        if($scope.page == 1){
            var offsets = 0;
        }else{
            var offsets = ($scope.page - 1) * 10 ;
        }
        $scope.loadingStart();
        $http.get("/album/list",   {
            params: {
                offset: offsets,
                search : srch,
                labelCd : $cookies.get("labelCd"),
                category : srcCategory,
                lcStatusCd : ''
            }
        })
            .then(function(response) {
                if ( response.data.code != "0") {
                    $scope.dataError = 'No Data Found';
                }else{
                    $scope.data = response.data.data;
                    $scope.totalPage = Math.ceil(parseInt(response.data.totalSize) / 10)
                }
                $scope.loadingStop();
            }, function(x) {
                $scope.dataError = 'No Data Found';
                $scope.loadingStop();
            });
    }

    $scope.searchButton = function () {
        $scope.page = 1;
        $scope.getAlbumList2();
    }
    $scope.isApprove = 0;
    $scope.showModalAlbum = function (el) {
        $scope.clearForm();
        $scope.isSongForm = 0;
        $scope.isEdit = false;
        $scope.isApprove = 0;
            console.log(el);
            $('#albumModal').modal({
                backdrop: 'static',
                keyboard: false
            })
            $('#albumModal').modal('show');
    }

    $scope.getAlbumList2();
    $scope.getNextPage = function(){
        $scope.page = $scope.page + 1;
        $scope.getAlbumList2();
    }

    $scope.getPrevPage = function(){
        $scope.page = $scope.page - 1;
        $scope.getAlbumList2();
    }
    $scope.getAwalPage = function(){
        $scope.page = 1;
        $scope.getAlbumList2();
    }

    $scope.getAkhirPage = function(){
        $scope.page = $scope.totalPage;
        $scope.getAlbumList2();
    }



    $scope.showModalAlbumEdit = function (el) {
        console.log(el);
        $scope.getAlbumArtist(el.albumId);
        $scope.getAlbumAvailable(el.albumId);
        $scope.isSongForm = 1;
        $scope.albumId = el.albumId;
        $scope.albumName = el.albumName;
        $scope.albumNameOrigin = el.albumNameOrigin;
        $scope.mainSongId = el.mainSongId;
        $scope.artistId = el.mainArtistId;
        $scope.albumType = el.albumTypeCd;
        $scope.seriesNo = parseInt(el.seriesNo);
        $scope.diskNo = parseInt(el.diskCnt);
        $scope.songName = el.songName;
        $scope.artistName = el.artistName
        $scope.releaseDate = new Date(el.issueDateStr);

        $scope.genre = el.genreId;
        // console.log($scope.getImgUrl(el.albumLImgPath));
        //$scope.labelName=el.labelCd;
        $scope.searchKeyword=el.searchKeyword;
        $scope.albumDescription=el.albumReview;
        $scope.sellCompany = el.sellCompany;
        $scope.ulrImg = el.albumLImgPath;
        $scope.lcStatusCdx = el.lcStatusCd;

        $scope.isEdit = true;
        $scope.albumId = el.albumId;
        if(el.lcStatusCd == 'CS0006'){
            $scope.isApprove = 1;
        }else{
            $scope.isApprove = 0;
        }
        $('#albumModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#albumModal').modal('show');
    }
    $scope.isFormAlbum = 1;
    $scope.showFormSearchArtist = function (el) {
        $('#artistSearchModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#artistSearchModal').modal('show');
        $scope.isFormAlbum = el;
        $scope.getArtistListSearch();
    }

    $scope.showFormSearchSong = function () {
        $('#songSearchModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#songSearchModal').modal('show');
        $scope.getSongListSearch();
    }

    $scope.getArtistListSearch = function () {
        var srch = '';
        if($scope.mdlArtistSrcTxt != undefined){
            srch = $scope.mdlArtistSrcTxt;
        }
        console.log(srch)

        var offsets = 0;

        console.log(offsets)
        $http.get("/artist/list",   {
            params: {
                offset: offsets,
                search : srch,
                labelCd : $cookies.get("labelCd"),
                category : 'artist_name',
                lcStatusCd : ''
            }
        })
            .then(function(response) {
                if ( response.data.code != "0") {
                    $scope.dataArtistError = 'No Data Found';
                }else{
                    $scope.dataArtist = response.data.data;
                    //$scope.totalPage = Math.ceil(parseInt(response.data.totalSize) / 10)
                }
            }, function(x) {
                $scope.dataArtistError = 'No Data Found';
            });
    }

    $scope.getSongListSearch = function () {
        var srch = '';
        if($scope.mdlSongSrcTxt != undefined){
            srch = $scope.mdlSongSrcTxt;
        }
        console.log(srch)

        var offsets = 0;

        console.log(offsets)

        $http.get("/song/list",   {
            params: {
                offset: offsets,
                search : srch,
                category : 'song_name',
                labelCd : $cookies.get("labelCd"),
                isCommercialYn : ''
            }
        })
            .then(function(response) {
                if ( response.data.code != "0") {
                    $scope.dataSongError = 'No Data Found';
                }else{
                    $scope.dataSong = response.data.data;
                    //$scope.totalPage = Math.ceil(parseInt(response.data.totalSize) / 10)
                }
            }, function(x) {
                $scope.dataSongError = 'No Data Found';
            });
    }
    $scope.artistVal = function (el) {
        var cc = false;
        console.log(el)
        angular.forEach($scope.AlbumArtistList, function (es) {
            if(es.artistId == el){
                cc = true;
            }
        });
        return cc;
    }
    $scope.addArtistId = function (el) {
        console.log($scope.isFormAlbum)

        if($scope.isFormAlbum == 1){
            $scope.artistId = el.artistId;
            $scope.artistName = el.artistName;
            $scope.AlbumArtistList.push({artistId: el.artistId,artistName:el.artistName});
        }else if($scope.isFormAlbum == 0){
            $scope.artistIdSong = el.artistId;
            $scope.artistNameSong = el.artistName;
        }else if($scope.isFormAlbum == 2){
            console.log($scope.artistVal(el.artistId))
            if($scope.artistVal(el.artistId)){
                alert('Artist sudah tersedia di List ');
            }else{
                $scope.AlbumArtistList.push({artistId: el.artistId,artistName:el.artistName});
            }
        }
        console.log($scope.AlbumArtistList)
        $('#artistSearchModal').modal('hide');
    }

    $scope.addSongId = function (el) {
        $scope.mainSongId = el.songId;
        $scope.songName = el.songName;
        $('#songSearchModal').modal('hide');
    }



    $scope.loadingStop = function () {
        $('#loaderModal').modal('hide');
    }

    $scope.convertDate = function (el) {
        var d = new Date(el),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return year + month + day;
    }

    $scope.validationForm = function () {

        var rslt = "";

        if($scope.albumName == "" || $scope.albumName == undefined) rslt += "Album Name \n";
        // if($scope.albumNameOrigin == '' || $scope.albumNameOrigin == undefined){
        //     rslt += "Album Name Origin \n";
        // }
        // if($scope.mainSongId == '' || $scope.mainSongId == undefined){
        //     rslt += "Song \n";
        // }
        if($scope.artistId == '' || $scope.artistId == undefined){
            rslt += "Artist \n";
        }
        if($scope.albumType == '' || $scope.albumType == undefined){
            rslt += "Album Type \n";
        }
        if($scope.seriesNo == '' || $scope.seriesNo == undefined){
            rslt += "Number of Song \n";
        }
        if($scope.sellCompany == '' || $scope.sellCompany == undefined){
            rslt += "Recording Label \n";
        }
        if($scope.releaseDate == '' || $scope.releaseDate == undefined){
            rslt += "Release Date \n";
        }
        if($scope.searchKeyword == '' || $scope.searchKeyword == undefined){
            rslt += "Search Keyword \n";
        }
        // if($scope.albumDescription == '' || $scope.albumDescription == undefined){
        //     rslt += "Album Description \n";
        //}
        if($scope.ulrImg == '' || $scope.ulrImg == undefined){
            rslt += "Image \n";
        }
        if($scope.genre == '' || $scope.genre == undefined){
            rslt += "genre \n";
        }
        return rslt;
    }

    $scope.addArtistAlbum = function (el) {
        var file = $scope.AlbumArtistList;
        if(file.length > 0) {
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }
            angular.forEach(file, function (files) {
                var data = $.param({
                    albumId: el,
                    artistId: files.artistId
                });
                $http.post('/album/insertAlbumArtist', data, config)
                    .success(function (data, status, headers, config) {
                        console.log(data);
                    })
                    .error(function (data, status, header, config) {

                    });
            });
        }
    }

    $scope.deleteArtistAlbum = function (el) {
        var data = $.param({
            albumId : el
        });

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }

        $http.post('/album/deleteAlbumArtist', data, config)
            .success(function (data, status, headers, config) {
                console.log(data);
            })
            .error(function (data, status, header, config) {

            });

    }
    $scope.saveAlbum = function(){
        var IssueDate = $scope.convertDate($scope.releaseDate);
        console.log($scope.diskNo)
        if($scope.diskNo == NaN){
            $scope.diskNo = '';
        }
        if($scope.isEdit == false){
            var data = $.param({
                albumName : $scope.albumName,
                albumNameOrigin : $scope.albumNameOrigin,
                mainSongId :$scope.mainSongId,
                mainArtistId : $scope.artistId,
                albumTypeCd : $scope.albumType,
                seriesNo : $scope.seriesNo,
                diskCnt : $scope.diskNo,
                issueDate : IssueDate,
                searchKeyword : $scope.searchKeyword,
                albumReview : $scope.albumDescription,
                albumLImgPath : $scope.ulrImg,
                genreId : $scope.genre,
                labelCd : $cookies.get("labelCd"),
                workerId : $cookies.get("username"),
                domesticYn : $scope.domesticYn,
                adultYn : $scope.adultYn,
                sellCompany : $scope.sellCompany
            });
        }else{
            var data = $.param({
                albumId : $scope.albumId,
                albumName : $scope.albumName,
                albumNameOrigin : $scope.albumNameOrigin,
                mainSongId :$scope.mainSongId,
                mainArtistId : $scope.artistId,
                albumTypeCd : $scope.albumType,
                seriesNo : $scope.seriesNo,
                diskCnt : $scope.diskNo,
                issueDate : IssueDate,
                labelCd : $cookies.get("labelCd"),
                searchKeyword : $scope.searchKeyword,
                albumReview : $scope.albumDescription,
                albumLImgPath : $scope.ulrImg,
                genreId : $scope.genre,
                workerId : $cookies.get("username"),
                domesticYn : $scope.domesticYn,
                adultYn : $scope.adultYn,
                sellCompany : $scope.sellCompany
            });
        }

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        var valForm = $scope.validationForm();
        console.log(valForm);
        if(valForm  == '') {
            $scope.loadingStart();
            $http.post('/album/add', data, config)
                .success(function (data, status, headers, config) {
                    var albumIdxx = '';
                    if($scope.isEdit == false){
                        $scope.isSongForm = 1;
                        var str = data.message;
                        var res = str.split("|");
                        alert(res[0]);
                        albumIdxx = res[1];
                        $scope.getAlbumList();
                        $scope.getAlbumAvailable(res[1]);
                        $scope.albumId = res[1];
                        $scope.isEdit = true;
                    }else{
                        var str = data.message;
                        var res = str.split("|");
                        albumIdxx = res[1];
                        $scope.getAlbumList();
                        alert(res[0]);
                        $scope.isSongForm = 1;
                        $('#albumModal').modal('hide');
                    }
                    //$scope.deleteArtistAlbum(albumIdxx);

                    $scope.addArtistAlbum(albumIdxx);

                    $scope.loadingStop();

                })
                .error(function (data, status, header, config) {
                    alert(data.message);
                    $scope.loadingStop();
                });
        }else{
            alert("Field : \n"+valForm+"Can't be Empty, Please input !");
        }
    }
    $scope.showImage = function () {
        $('#imageModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#imageModal').modal('show');
    }
    $scope.clearForm = function (){
        $scope.AlbumArtistList = [];
        $scope.albumId = "";
        $scope.albumName = "";
        $scope.albumNameOrigin = "";
        $scope.mainSongId = "";
        $scope.artistId = "";
        $scope.albumType = "";
        $scope.seriesNo = "";
        $scope.diskNo = "";
        $scope.songName = "";
        $scope.artistName = "";
        $scope.releaseDate = "";
        $scope.labelName="";
        $scope.searchKeyword="";
        $scope.albumDescription="";
        $scope.albumId = "";
        $scope.domesticYn = "Y";
        $scope.adultYn = "N";
        $scope.ulrImg = "";
        $scope.sellCompany = "";
        $scope.genre = "";
        $scope.lcStatusCdx = '';
    }

    $scope.uploadFile = function(){
        var file = $scope.myFile;
        var img;
        var uploadUrl = "/fileUpload";
        var rslt = {};
        var fileUpload = document.getElementById("upload-photo");

        var reader = new FileReader();

        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
        var isValid;
        if (regex.test(fileUpload.value.toLowerCase())) {
            var reader = new FileReader();

            reader.readAsDataURL(fileUpload.files[0]);
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();

                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;

                //Validate the File Height and Width.
                image.onload = function () {
                    var height = this.height;
                    var width = this.width;
                    console.log(height);
                    console.log(width);
                    if (height <= 600 && width <=600) {
                        alert("Height and Width must not exceed 600px.");
                        isValid =  false;
                    }else{
                        //isValid = true;
                        var fd = new FormData();
                        $scope.loadingStart();
                        angular.forEach(file, function (files,index,ls,ll,ss) {
                            fd.append('file', files);
                       });
                        $http.post(uploadUrl, fd, {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined}
                        })

                            .success(function (data) {
                                console.log(data)
                                $scope.ulrImg = data.message;
                                $scope.loadingStop();
                            })

                            .error(function (data) {
                                alert(data.message);
                                $scope.loadingStop();
                            });
                    }
                    //alert("Uploaded image has valid Height and Width.");

                };

            }
        } else {
            alert("Please select a valid Image file.");
            isValid = false;
        }
    };
    
    $scope.getImgUrl = function (ss) {
        if(ss == ''){
            return '';
        }else{
            // var sp = ss.split("/");
            // var ssl = "/image/";
            // if(sp[1] == 'mediaNASX'){
            //     ssl = "/imagex/";
            // }else if(sp[1] == 'mediaNAS1'){
            //     ssl =  "/image1/";
            // }
            // console.log(sp[1]);
            // return ssl+sp[sp.length - 1];
            return ss;
        }
    }

    $scope.deleteImg = function () {
        var data = $.param({
            filename : $scope.ulrImg
        });

        var config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }

        var msg = confirm("Apakah anda ingin menghapus data ini !");
        if(msg == true){
            $scope.loadingStart();
            $http.post('/fileDelete', data, config)
                .success(function (data, status, headers, config) {
                    if(data.code == 1){
                        $scope.ulrImg = "";
                        $scope.getAlbumList();
                    }
                    $scope.loadingStop();
                    alert(data.message);
                })
                .error(function (data, status, header, config) {
                    $scope.loadingStop();
                    alert(data.message);
                });
        }
    }


    /**SONG**/
    $scope.isCommercial = 1;
    // $scope.drmPriceList = [{"id":"1000","price":"1000"},{"id":"3000","price":"3000"},{"id":"5000","price":"5000"},{"id":"10000","price":"10000"}]
    $scope.drmPriceList = [{"id":"5000","price":"5000"},{"id":"7000","price":"7000"}]

    $scope.getAlbumAvailable = function (el) {
        $http.get("/album/songAvailabel",  {
            params: {
                labelCd: '',
                albumId : el
            }
        })
            .then(function (response) {
                $scope.dataAlbumSong = response.data;
                $scope.albumAvailable = $scope.dataAlbumSong[0]
                $scope.getSongNotCompleted();
            });
    }

    $scope.showModalSongs = function () {

        //$scope.getAlbumAvailable();

        $('#songsAddModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        //$scope.albumAvailable[0]
        $('#songsAddModal').modal('show');
    }

    $scope.getSongNotCompleted = function () {
        //console.log($scope.albumAvailable)
        $http.get("/song/listByAlbum?albumId="+$scope.albumAvailable.albumId)
            .then(function(response) {
                $scope.dataSongNotComplete = response.data;
                console.log(response);
            });
    }

    $scope.showModalSongsNew = function () {
        $scope.isSongForm = 1;
        console.log($scope.albumAvailable.seriesNo)
        if(parseInt($scope.albumAvailable.seriesNo) != $scope.dataSongNotComplete.length){
            $scope.clearFormSong();
            //$scope.isEdit = false;
            $('#songsModal').modal({
                backdrop: 'static',
                keyboard: false
            })
            $('#songsModal').modal('show');

            $scope.albumId = $scope.albumAvailable.albumId;
            $scope.albumName = $scope.albumAvailable.albumName;
            $scope.artistName =$scope.albumAvailable.artistName;
            $scope.artistId = $scope.albumAvailable.artistId;
        }else{
            alert("The number of songs matches Field Number of Song in the Album")
        }

    }

    $scope.showModalEdit = function (el) {
        //var sc = moment();
        $scope.isSongForm = 1;
       // console.log(moment(el.issueDateStr,'DD-MM-YYYY')._d);
        console.log(el);
        $scope.isEdit = true;
        $scope.mainSongId = el.songId;
        $scope.songId = el.songId;
        $scope.songName = el.songName;
        $scope.songNameOriginal = el.songNameOrigin;
        $scope.albumName = el.albumName;
        $scope.albumId = el.albumId;
        $scope.artistNameSong = el.artistName;
        $scope.artistId = el.artistId;
        $scope.mlb = el.mlbCd;
        $scope.genre = el.genreId;
        $scope.sellCompany = el.sellCompany;
        $scope.issueDate = moment(el.issueDateStr,'DD-MM-YYYY')._d;
        $scope.hitSong = el.hitSongYn;
        $scope.adultSong = el.adultYn;
        $scope.isCommercialYn =  el.isCommercialYn;
        $scope.originalFileName = el.originalFileName;
        if($scope.ulrLyric !== null || $scope.ulrLyric !== ""){
            $scope.isInstrument = 'N';
        }else{
            $scope.isInstrument = 'Y';
        }
        $scope.ulrMp3 = el.fullFilePath;
        $scope.ulrLyric = el.lyricPath;
        $scope.songIdx = el.songId;
        $scope.grid = el.genidCd;
        $scope.hint = el.hint;
        $scope.trackNo = el.trackNo;
        $scope.nonDrmPrice = el.nonDrmPrice;
        $scope.mp3Download = el.sellNonDrmYn;
        $scope.stream = el.sellStreamYn;
        $scope.labelCdx = el.labelCd;
        $scope.mainSongYn = el.mainSongYn;
        $scope.searchKeywordSong = el.searchKeyword;
        $('#songsModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#songsModal').modal('show');
    }

    $scope.clearFormSong = function (){
        $scope.songId = '';
        $scope.songName = '';
        $scope.songNameOriginal = '';
        $scope.albumName = '';
        $scope.albumId = '';
        $scope.artistName = '';
        $scope.artistId = '';
        $scope.mlb = '';
        $scope.genre = '';
        $scope.issueDate = '';
        $scope.hitSong = 'N';
        $scope.adultSong = 'N';
        $scope.mp3Download = 'Y';
        $scope.stream = 'Y';
        $scope.originalFileName = "";
        $scope.ulrMp3 = "";
        $scope.ulrLyric = "";
        $scope.songIdx = "";
        $scope.grid = "";
        $scope.hint = "";
        $scope.trackNo = "";
        $scope.nonDrmPrice = "5000";
        $scope.labelCdx = "";
        $scope.mainSongYn = 'N';
        $scope.isInstrument = 'N';
        $scope.searchKeywordSong = '';
    }

    $scope.saveSong = function(){
        var IssueDate = $scope.convertDate($scope.issueDate);
        console.log(IssueDate)
        if($scope.isCommercial == 0){
            $scope.isCommercialYn = "Y";
        }else{
            $scope.isCommercialYn = "N";
        }
        var textLyric = 'Y';
        if($scope.ulrLyric == ''){
            textLyric = 'N';
        }else if($scope.ulrLyric == undefined){
            textLyric = 'N';
        }

        if($scope.isEdit == false){
            var data = $.param({
                songName : $scope.songName,
                songNameOrigin : $scope.songNameOriginal,
                albumId :$scope.albumId,
                artistId : $scope.artistId,
                mlbCd : $scope.mlb,
                genreId : $scope.genre,
                issueDate : IssueDate,
                hitSongYn : $scope.hitSong,
                adultYn : $scope.adultSong,
                isCommercialYn :  $scope.isCommercialYn,
                sellNonDrmYn : $scope.mp3Download,
                sellStreamYn : $scope.stream,
                originalFileName : $scope.originalFileName,
                fullFilePath : $scope.ulrMp3,
                lyricPath : $scope.ulrLyric,
                genidCd : $scope.grid,
                hint : $scope.hint,
                trackNo : $scope.trackNo,
                nonDrmPrice : $scope.nonDrmPrice,
                labelCd : $cookies.get("labelCd"),
                workerId : $cookies.get("username"),
                textLyricYn : textLyric,
                lcStatusCd : 'CS0001',
                mainSongYn : $scope.mainSongYn,
                searchKeyword : $scope.searchKeywordSong
            });
        }else{
            var data = $.param({
                songId : $scope.songId,
                songName : $scope.songName,
                songNameOrigin : $scope.songNameOriginal,
                albumId :$scope.albumId,
                artistId : $scope.artistId,
                mlbCd : $scope.mlb,
                genreId : $scope.genre,
                issueDate : IssueDate,
                hitSongYn : $scope.hitSong,
                adultYn : $scope.adultSong,
                isCommercialYn :  $scope.isCommercialYn,
                originalFileName : $scope.originalFileName,
                fullFilePath : $scope.ulrMp3,
                lyricPath : $scope.ulrLyric,
                genidCd : $scope.grid,
                hint : $scope.hint,
                trackNo : $scope.trackNo,
                nonDrmPrice : $scope.nonDrmPrice,
                sellNonDrmYn : $scope.mp3Download,
                sellStreamYn : $scope.stream,
                labelCd : $cookies.get("labelCd"),
                workerId : $cookies.get("username"),
                textLyricYn : textLyric,
                mainSongYn : $scope.mainSongYn,
                lcStatusCd : 'CS0001',
                searchKeyword : $scope.searchKeywordSong
            });
        }

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        var valForm = $scope.validationFormSong();
        console.log(valForm);

        if(valForm  == '') {
            $scope.loadingStart();
            $http.post('/song/add', data, config)
                .success(function (data, status, headers, config) {
                    alert(data.message);
                    $('#songsModal').modal('hide');
                    //$scope.getSongList();
                    $scope.getSongNotCompleted();
                    $scope.loadingStop();
                })
                .error(function (data, status, header, config) {
                    alert(data.message);
                    $scope.loadingStop();
                });
        }else{
            alert("Field : \n"+valForm+"Can't be Empty, Please input !");
        }
    }

    $scope.validationFormSong = function () {

        var rslt = "";
        console.log($scope.isInstrument);
        if($scope.songName == '' ||$scope.songName == undefined) rslt += "Song Name \n";
        // if($scope.songNameOriginal == '' ||$scope.songNameOriginal == undefined) rslt += "Song Name Original \n";
        if($scope.albumId == '' ||$scope.albumId == undefined) rslt += "Album \n";
        if($scope.artistId == '' ||$scope.artistId == undefined) rslt += "Artist \n";
        if($scope.mlb == '' ||$scope.mlb == undefined) rslt += "Mlb \n";
        if($scope.genre == '' ||$scope.genre == undefined) rslt += "Genre \n";
        if($scope.issueDate == '' || $scope.issueDate == undefined) rslt += "Issue Date \n";
        if($scope.grid == '' ||$scope.grid == undefined) rslt += "ISRC \n";
        // if($scope.hint == '' ||$scope.hint == undefined) rslt += "Hint \n";
        if($scope.trackNo == '' ||$scope.trackNo == undefined) rslt += "Track No \n";
        if($scope.nonDrmPrice == '' ||$scope.nonDrmPrice == undefined) rslt += "DRM Price \n";
        if($scope.ulrMp3 == '' ||$scope.ulrMp3 == undefined) rslt += "File Song \n";
        console.log($scope.isInstrument);
        if($scope.isInstrument == 'N'){
            if($scope.ulrLyric == '' ||$scope.ulrLyric == undefined) rslt += "Lyric \n";
        }
        return rslt;
    }

    $scope.saveSongAdd = function () {
        console.log($scope.dataSongNotComplete.length)
        console.log($scope.dataSongNotComplete.length)
        if( $scope.albumAvailable.seriesNo == $scope.dataSongNotComplete.length){
            alert("Data berhasil disimpan");
            $('#songsAddModal').modal('hide');
            $('#albumModal').modal('hide');
            $scope.getAlbumList();
        }else{
            alert("Data harus sesuai dengan jumlah song dalam album")
        }

    }

    $scope.uploadFileMp3 = function(){
        var file = $scope.myFile;
        // if(file.size >= 100000000){
        //     alert("File tidak boleh lebih besar dari 100MB");
        //     return;
        // }
        console.log('file is ' );
        //console.dir(file);
        //console.dir(file.name);
        var ssh = file.name;
        var sp = ssh.split(".");
        var fileExt = sp[sp.length-1];
        console.log(fileExt);
        var uploadUrl = "/fileUpload";
        var rslt = {};
        if(file != undefined){
            if(fileExt == 'wav'){
                $scope.loadingStart();
                var fd = new FormData();
                angular.forEach(file, function (files) {
                    fd.append('file', files);
                });
                $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                })

                    .success(function (data) {
                        console.log(data)
                        //rslt = data;
                        $scope.ulrMp3 = data.message;
                        $scope.originalFileName = data.message;
                        $scope.loadingStop();
                    })

                    .error(function (data) {
                        alert(data.message);
                        $scope.loadingStop();
                    });
            }else{
                alert("Not WAV file ! Please Browse Again with wav Extention")
            }

        }else{
            alert("Please Browse the File")
        }
        //$scope.loadingStop();
        // console.log(rslt)
        //
        // //var dataUpload = fileUpload.uploadFileToUrl(file, uploadUrl);
        // //console.log(dataUpload);
        // $scope.ulrMp3 = "/upload/music" + rslt.message;
    };


    $scope.uploadFileLyric = function(){
        console.log($scope.myFile)
        var file = $scope.myFile1;

        console.log('file is ' );
        console.dir(file);
        var ssh = file.name;
        var sp = ssh.split(".");
        var fileExt = sp[sp.length-1];
        console.log(fileExt);
        var uploadUrl = "/fileUpload";
        var rslt = {};
        if(file != undefined){
            if(fileExt == 'txt'){
                $scope.loadingStart();
                var fd = new FormData();
                angular.forEach(file, function (files) {
                    fd.append('file', files);
                });

                $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                })

                    .success(function (data) {
                        console.log(data)
                        //rslt = data;
                        $scope.ulrLyric = data.message;
                        $scope.loadingStop();
                    })

                    .error(function (data) {
                        alert(data.message);
                        $scope.loadingStop();
                    });
            }else{
                alert("Not TXT file ! Please Browse Again with txt Extention")
            }

        }else{
            alert("Please Browse the File")
        }

    };

    //$scope.lagu="/sjdsd.mp3";
    $scope.getMusicUrl = function (ss) {
        if(ss == ''){
            return '';
        }else{
            // var sp = ss.split("/");
            // var sp = ss.split("/");
            // var ssl = "/music/";
            // if(sp[1] == 'mediaNASX'){
            //     ssl = "/musix/";
            //     return ssl+sp[sp.length - 3]+'/'+sp[sp.length - 2]+'/'+sp[sp.length - 1];
            // }else if(sp[1] == 'mediaNAS1'){
            //     ssl =  "/image1/";
            // }
            // return ssl+sp[sp.length - 1];

            return ss;
        }
    }
    $scope.songModAll = "";
    $scope.showEncodeModal = function (el) {
        $('#encoderModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $scope.songModAll = el;
        $scope.getSongModList(el);
        $('#encoderModal').modal('show');
    }
    $scope.saveEncoder = function (el,filePath) {
        var data = $.param({
            songModId : el.songModId,
            songId : el.songId,
            sampling :el.sampling,
            codecTypeCd : el.codecTypeCd,
            bitRateCd : el.bitRateCd,
            playTime : el.playTime,
            fileSize : el.fileSize,
            originalFileName : filePath,
            fullFilePath : filePath,
            workerId :  el.workerId,
            status :  el.status
        });

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        $scope.loadingStart();
        $http.post('/songMod/add', data, config)
            .success(function (data, status, headers, config) {
                // alert(data.message);
                // $('#songsModal').modal('hide');
                $scope.getSongModList(el.songId);
                //$scope.getSongList();
                $scope.loadingStop();
            })
            .error(function (data, status, header, config) {
                alert(data.message);
                $scope.loadingStop();
            });
    }
    $scope.encoder = function (el) {
        if($scope.ulrMp3x != ''){
            var type_bit,targetx;
            if(el.codecTypeCd == 'CT0001'){
                targetx = 'mp3';
            }else if(el.codecTypeCd == 'CT0007'){
                targetx ='m4a';
            }else if(el.codecTypeCd == 'CT0008'){
                targetx = 'flac';
            }else if(el.codecTypeCd == 'CT0002'){
                targetx = 'm4a';
            }


            if(el.bitRateCd == "BR0320"){
                type_bit = "320";
            }else if(el.bitRateCd == "BR0128"){
                type_bit = "128";
            }else if(el.bitRateCd == "BR0192"){
                type_bit = "192";
            }else if(el.bitRateCd == "BR0096"){
                type_bit = "96";
            }else if(el.bitRateCd == "BR1411"){
                type_bit = "1411";
            }else if(el.bitRateCd == "BR0256"){
                type_bit = "256";
            }
            //console.log(type_bit);
            $scope.loadingStart();
            $http.get("http://cmsv2.melon.co.id/encoder/encode_wav",{
                params: {
                    song_path: $scope.ulrMp3x,
                    bit_rate : type_bit,
                    song_id:el.songId,
                    target : targetx
                }
            })
                .success(function (response) {
                    console.log(response);
                    if(response.length <= 70){
                        if(response != "-1"){
                            $scope.saveEncoder(el,response);
                        }
                    }else{
                        alert('Encode Gagal')  ;
                    }
                    $scope.loadingStop();
                })

                .error(function (data) {
                    console.log(response);
                    alert('Encode Gagal')  ;
                    $scope.loadingStop();
                });

        }
    }
    $scope.encodeAll = function () {
        var data = $.param({
            songId : $scope.songModAll
        });

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        $scope.loadingStart();
        $http.post('/songMod/job', data, config)
            .success(function (data, status, headers, config) {
                alert(data.message);
                $scope.loadingStop();
                $('#encoderModal').modal('hide');
            })
            .error(function (data, status, header, config) {
                alert(data.message);
                $scope.loadingStop();
            });
    }
    $scope.isPlay = 0;
    $scope.playSong = function (el) {
        var urlMusic = $scope.getMusicUrl(el);
        $scope.audio = ngAudio.load(urlMusic);
        $scope.isPlay = 1;
        if($scope.audio.paused){
            $scope.audio.pause();
        }else{
            $scope.audio.play();
        }

        console.log($scope.audio);
    }
    $scope.showButtonAllSongMod = true;
    $scope.getSongModList = function (el) {
        $http.get("/songMod/list?songId="+el)
            .then(function(response) {
                $scope.songModList = response.data;
                $scope.ulrMp3x = $scope.songModList[0].fullFilePath;
                console.log(response);
                var i = 0;
                angular.forEach($scope.songModList, function (el){
                    console.log(el.fullFilePath);
                    if(el.fullFilePath == ''){
                        i = 1;
                    }
                });
                if(i > 0){
                    $scope.showButtonAllSongMod = true;
                }else{
                    $scope.showButtonAllSongMod = false;
                }
            });
    }
    
    $scope.removeAlbumArtistList = function (el) {
        console.log(el)
        $scope.AlbumArtistList.splice(el);
        console.log($scope.AlbumArtistList);
    }

    $scope.showModalMlb = function () {

        $('#mlbSearchModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#mlbSearchModal').modal('show');
    }
    $scope.pageMlb = 1;
    $scope.offsetMlb = 0;
    $scope.totalPageMlb = 0;
    $scope.getMlbCode = function () {
        var srcMlb = document.getElementById("srcMlb").value;
        var artistMlb = '';
        var songMlb = '';
        if(srcMlb == 'artistMlb'){
            artistMlb = $scope.searchValue;
        }else{
            songMlb = $scope.searchValue;
        }

        if($scope.pageMlb <= 1){
            $scope.offsetMlb = 0;
        }else{
            $scope.offsetMlb = ($scope.pageMlb - 1 ) * 10;
        }
        $scope.loadingStart();
        $http.get("/song/getMlbCode",   {
            params: {
                offset: $scope.offsetMlb,
                searchArtist : artistMlb,
                searchSong : songMlb,
                limit : 10
            }
        })
            .then(function(response) {
                if ( response.data.code != "0") {
                    $scope.dataError = 'No Data Found';
                }else{
                    $scope.dataMlb = response.data.data;
                    //$scope.pageMlb = 1;
                    $scope.totalPageMlb = Math.ceil(parseInt(response.data.totalSize) / 10);
                }
                $scope.loadingStop();
            }, function() {
                $scope.dataError = 'No Data Found';
                $scope.loadingStop();
            });
    }

    $scope.getAwalPageMlb = function () {
        $scope.pageMlb = 1;
        $scope.getMlbCode();
    }
    $scope.getPrevPageMlb = function () {
        $scope.pageMlb = $scope.pageMlb - 1;
        $scope.getMlbCode();
    }
    $scope.getNextPageMlb = function () {
        $scope.pageMlb = $scope.pageMlb + 1;
        $scope.getMlbCode();
    }
    $scope.getAkhirPageMlb = function () {
        $scope.pageMlb = $scope.totalPageMlb;
        $scope.getMlbCode();
    }

    $scope.addMlbCode = function (el) {
        $scope.mlb = el.musicCode;
        $scope.grid  = el.isrc;
        $('#mlbSearchModal').modal('hide');
    }
 });
