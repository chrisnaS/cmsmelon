angular.module('MetronicApp').controller('SongController', function($rootScope, $scope, $http, $timeout, $state, ngAudio,$cookies) {
    $scope.$on('$viewContentLoaded', function() {
        App.initAjax(); // initialize core components
        Layout.setAngularJsSidebarMenuActiveLink('set', $('#sidebar_menu_link_profile'), $state); // set profile link active in sidebar menu
    });

    // set sidebar closed and body solid melyx mode
    $('[data-toggle="tooltip"]').tooltip();
    $scope.isCommercial = 1;
    $scope.ulrMp3x = '';
    $rootScope.settings.layout.pageBodySolid = true;
    $rootScope.settings.layout.pageSidebarClosed = true;
    $scope.loadingStart = function () {
        $('#loaderModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#loaderModal').modal('show');
    }

    $scope.loadingStop = function () {
        $('#loaderModal').modal('hide');
    }
    $http.get("/genre/list")
        .then(function(response) {
            $scope.dataGenre = response.data;
            console.log(response);
        });

    $scope.getAlbumAvailable = function () {
        $http.get("/album/songAvailabel?labelCd="+$cookies.get("labelCd"))
            .then(function(response) {
                $scope.dataAlbumSong = response.data;
                console.log(response);
            });
    }

    // $scope.getSongNotCompleted = function () {
    //     //console.log($scope.albumAvailable)
    //     $http.get("/song/listByAlbum?albumId="+$scope.albumAvailable.albumId)
    //         .then(function(response) {
    //             $scope.dataSongNotComplete = response.data;
    //             console.log(response);
    //         });
    // }

    $scope.page = 1;
    $scope.isEdit = false;
    $scope.ulrLyric = "";
    $scope.ulrMp3 = "";

    $scope.drmPriceList = [{"id":"5000","price":"5000"},{"id":"7000","price":"7000"}]
    $scope.getSongList = function () {
        var srcCategory = document.getElementById("srcSelect").value;
        var srch = "";
        var lcStatusCdx = '';
        if($scope.mdlSearch != undefined){
            srch = $scope.mdlSearch;
        }
        console.log($scope.mdlSearch)
        if($scope.page == 1){
            var offsets = 0;
        }else{
            var offsets = ($scope.page - 1) * 10 ;
        }
        console.log(offsets)
        if($scope.isCommercial == 0){
            $scope.isCommercialYn = "Y";
        }else{
            $scope.isCommercialYn = "N";
            lcStatusCdx = 'CS0001'
        }
        //$scope.loadingStart();
        $http.get("/song/list",   {
            params: {
                offset: offsets,
                search : srch,
                category : srcCategory,
                labelCd : $cookies.get("labelCd"),
                isCommercialYn : $scope.isCommercialYn,
                lcStatusCd : lcStatusCdx
            }
        })
            .then(function(response) {
                if ( response.data.code != "0") {
                    $scope.dataError = 'No Data Found';
                }else{
                    $scope.data = response.data.data;
                    //console.log($scope.data)
                    $scope.totalPage = Math.ceil(parseInt(response.data.totalSize) / 10)
                }
                //$scope.loadingStop();
            }, function(x) {
                $scope.dataError = 'No Data Found';
                //$scope.loadingStop();
            });
    }

    $scope.getSongList2 = function () {
        var srcCategory = document.getElementById("srcSelect").value;
        var srch = "";
        var lcStatusCdx = '';
        if($scope.mdlSearch != undefined){
            srch = $scope.mdlSearch;
        }
        console.log($scope.mdlSearch)
        if($scope.page == 1){
            var offsets = 0;
        }else{
            var offsets = ($scope.page - 1) * 10 ;
        }
        console.log(offsets)
        if($scope.isCommercial == 0){
            $scope.isCommercialYn = "Y";
        }else{
            $scope.isCommercialYn = "N";
            lcStatusCdx = 'CS0001'
        }
        $scope.loadingStart();
        $http.get("/song/list",   {
            params: {
                offset: offsets,
                search : srch,
                category : srcCategory,
                labelCd : $cookies.get("labelCd"),
                isCommercialYn : $scope.isCommercialYn,
                lcStatusCd : lcStatusCdx
            }
        })
            .then(function(response) {
                if ( response.data.code != "0") {
                    $scope.dataError = 'No Data Found';
                }else{
                    $scope.data = response.data.data;
                    //console.log($scope.data)
                    $scope.totalPage = Math.ceil(parseInt(response.data.totalSize) / 10)
                }
                $scope.loadingStop();
            }, function(x) {
                $scope.dataError = 'No Data Found';
                $scope.loadingStop();
            });
    }

    $scope.getSongList2();

    $scope.enterSearch = function(el){
        if(el.keyCode == 13){
            $scope.getSongList2();
        }
    }
    $scope.searchButton = function () {
        $scope.page = 1;
        $scope.getSongList2();
    }

    $scope.getNextPage = function(){
        $scope.page = $scope.page + 1;
        $scope.getSongList2();
    }

    $scope.getPrevPage = function(){
        $scope.page = $scope.page - 1;
        $scope.getSongList2();
    }
    $scope.getAwalPage = function(){
        $scope.page = 1;
        $scope.getSongList2();
    }

    $scope.getAkhirPage = function(){
        $scope.page = $scope.totalPage;
        $scope.getSongList2();
    }

    $scope.clearForm = function (){
        $scope.songId = '';
        $scope.songName = '';
        $scope.songNameOriginal = '';
        $scope.albumName = '';
        $scope.albumId = '';
        $scope.artistNameSong = '';
        $scope.artistIdSong = '';
        $scope.mlb = '';
        $scope.genre = '';
        $scope.issueDate = '';
        $scope.hitSong = 'N';
        $scope.adultSong = 'N';
        $scope.mp3Download = 'Y';
        $scope.stream = 'Y';
        $scope.isInstrument = 'N';
        $scope.originalFileName = "";
        $scope.ulrMp3 = "";
        $scope.ulrLyric = "";
        $scope.songIdx = "";
        $scope.grid = "";
        $scope.hint = "";
        $scope.trackNo = "";
        $scope.nonDrmPrice = "5000";
        $scope.labelCdx = "";
        $scope.mainSongYn = 'N';
        $scope.searchKeywordSong = '';
    }

    $scope.showModalSongs = function () {
        $scope.clearForm();
        $scope.isEdit = false;
        $scope.getAlbumAvailable();

        $('#songsAddModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#songsAddModal').modal('show');
    }

    $scope.showModalSongsNew = function () {
        $scope.clearForm();
        $scope.isEdit = false;
        console.log($scope.albumAvailable)
        $('#songsModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#songsModal').modal('show');

        // $scope.albumId = $scope.albumAvailable.albumId;
        // $scope.albumName = $scope.albumAvailable.albumName;
        // $scope.artistNameSong =$scope.albumAvailable.artistName;
        // $scope.artistIdSong = $scope.albumAvailable.artistId;
        // $scope.getAlbumAvailable();
        //
        // $('#songsAddModal').modal({
        //     backdrop: 'static',
        //     keyboard: false
        // })
        // $('#songsAddModal').modal('show');
    }

    $scope.showModalEdit = function (el) {
        console.log(el);
        $scope.isEdit = true;
        $scope.songId = el.songId;
        $scope.songName = el.songName;
        $scope.songNameOriginal = el.songNameOrigin;
        $scope.albumName = el.albumName;
        $scope.albumId = el.albumId;
        $scope.artistNameSong = el.artistName;
        $scope.artistIdSong = el.artistId;
        $scope.mlb = el.mlbCd;
        $scope.genre = el.genreId;
        $scope.issueDate = new Date(el.issueDateStr);
        $scope.hitSong = el.hitSongYn;
        $scope.adultSong = el.adultYn;
        $scope.isCommercialYn =  el.isCommercialYn;
        $scope.originalFileName = el.originalFileName;
        $scope.ulrMp3 = el.fullFilePath;
        $scope.ulrLyric = el.lyricPath;
        $scope.mainSongYn = el.mainSongYn;

        if($scope.ulrLyric !== null || $scope.ulrLyric !== ""){
            $scope.isInstrument = 'N';
        }else{
            $scope.isInstrument = 'Y';
        }
        $scope.songIdx = el.songId;
        $scope.grid = el.genidCd;
        $scope.hint = el.hint;
        $scope.trackNo = el.trackNo;
        $scope.nonDrmPrice = el.nonDrmPrice;
        $scope.mp3Download = el.sellNonDrmYn;
        $scope.stream = el.sellStreamYn;
        $scope.labelCdx = el.labelCd;
        $scope.searchKeywordSong = el.searchKeyword;
        $('#songsModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#songsModal').modal('show');
    }
    $scope.showFormSearchAlbum = function () {
        $('#albumSearchModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#albumSearchModal').modal('show');
        $scope.getAlbumListSearch();
    }
    $scope.isFormAlbum = 0;
    $scope.showFormSearchArtist = function () {
        $('#artistSearchModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#artistSearchModal').modal('show');
        $scope.isFormAlbum = 0;
        $scope.getArtistListSearch();
    }

    $scope.getAlbumListSearch = function () {
        var srch = document.getElementById("mdlAlbumSrcTxt").value;
        console.log(srch)
        var offsets = 0;

        console.log(offsets)
        $http.get("/album/list",   {
            params: {
                offset: offsets,
                search : srch,
                labelCd : $cookies.get("labelCd"),
                category : 'album_name',
                lcStatusCd : ''
            }
        })
            .then(function(response) {
                if ( response.data.code != "0") {
                    $scope.dataAlbumError = 'No Data Found';
                }else{
                    $scope.dataAlbum = response.data.data;
                    //$scope.totalPage = Math.ceil(parseInt(response.data.totalSize) / 10)
                }
            }, function(x) {
                $scope.dataAlbumError = 'No Data Found';
            });
    }

    $scope.getArtistListSearch = function () {
        var srch = '';
        if($scope.mdlArtistSrcTxt != undefined){
            srch = $scope.mdlArtistSrcTxt;
        }
        console.log(srch)

        var offsets = 0;

        console.log(offsets)
        $http.get("/artist/list",   {
            params: {
                offset: offsets,
                search : srch,
                labelCd : $cookies.get("labelCd"),
                category : 'artist_name',
                lcStatusCd : ''
            }
        })
            .then(function(response) {
                if ( response.data.code != "0") {
                    $scope.dataArtistError = 'No Data Found';
                }else{
                    $scope.dataArtist = response.data.data;
                    //$scope.totalPage = Math.ceil(parseInt(response.data.totalSize) / 10)
                }
            }, function(x) {
                $scope.dataArtistError = 'No Data Found';
            });
    }

    $scope.convertDate = function (el) {
        var d = new Date(el),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return year + month + day;
    }

    $scope.validationForm = function () {

        var rslt = "";

        if($scope.songName == '' ||$scope.songName == undefined) rslt += "Song Name \n";
        // if($scope.songNameOriginal == '' ||$scope.songNameOriginal == undefined) rslt += "Song Name Original \n";
        if($scope.albumId == '' ||$scope.albumId == undefined) rslt += "Album \n";
        if($scope.artistIdSong == '' ||$scope.artistIdSong == undefined) rslt += "Artist \n";
        if($scope.mlb == '' ||$scope.mlb == undefined) rslt += "Mlb \n";
        if($scope.genre == '' ||$scope.genre == undefined) rslt += "Genre \n";
        if($scope.issueDate == '' || $scope.issueDate == undefined) rslt += "Issue Date \n";
        if($scope.grid == '' ||$scope.grid == undefined) rslt += "ISRC \n";
        // if($scope.hint == '' ||$scope.hint == undefined) rslt += "Hint \n";
        if($scope.trackNo == '' ||$scope.trackNo == undefined) rslt += "Track No \n";
        if($scope.nonDrmPrice == '' ||$scope.nonDrmPrice == undefined) rslt += "DRM Price \n";
        if($scope.ulrMp3 == '' ||$scope.ulrMp3 == undefined) rslt += "File Song \n";
        if($scope.isInstrument == 'N'){
            if($scope.ulrLyric == '' ||$scope.ulrLyric == undefined) rslt += "Lyric \n";
        }

        return rslt;
    }

    $scope.saveSong = function(){
        var IssueDate = $scope.convertDate($scope.issueDate);
        console.log(IssueDate);
        if($scope.isCommercial == 0){
            $scope.isCommercialYn = "Y";
        }else{
            $scope.isCommercialYn = "N";
        }
        var textLyric = 'Y';
        if($scope.ulrLyric == '' || $scope.ulrLyric == undefined){
            textLyric = 'N';
        }

        if($scope.isEdit == false){
            var data = $.param({
                //songId : $scope.songId,
                songName : $scope.songName,
                songNameOrigin : $scope.songNameOriginal,
                albumId :$scope.albumId,
                artistId : $scope.artistIdSong,
                mlbCd : $scope.mlb,
                genreId : $scope.genre,
                issueDate : IssueDate,
                hitSongYn : $scope.hitSong,
                adultYn : $scope.adultSong,
                isCommercialYn :  $scope.isCommercialYn,
                sellNonDrmYn : $scope.mp3Download,
                sellStreamYn : $scope.stream,
                originalFileName : $scope.originalFileName,
                fullFilePath : $scope.ulrMp3,
                lyricPath : $scope.ulrLyric,
                genidCd : $scope.grid,
                hint : $scope.hint,
                trackNo : $scope.trackNo,
                nonDrmPrice : $scope.nonDrmPrice,
                labelCd : $cookies.get("labelCd"),
                workerId : $cookies.get("username"),
                textLyricYn : textLyric,
                mainSongYn : $scope.mainSongYn,
                lcStatusCd : 'CS0001',
                searchKeyword : $scope.searchKeywordSong
            });
        }else{
            var data = $.param({
                songId : $scope.songId,
                songName : $scope.songName,
                songNameOrigin : $scope.songNameOriginal,
                albumId :$scope.albumId,
                artistId : $scope.artistIdSong,
                mlbCd : $scope.mlb,
                genreId : $scope.genre,
                issueDate : IssueDate,
                hitSongYn : $scope.hitSong,
                adultYn : $scope.adultSong,
                isCommercialYn :  $scope.isCommercialYn,
                originalFileName : $scope.originalFileName,
                fullFilePath : $scope.ulrMp3,
                lyricPath : $scope.ulrLyric,
                genidCd : $scope.grid,
                hint : $scope.hint,
                trackNo : $scope.trackNo,
                nonDrmPrice : $scope.nonDrmPrice,
                sellNonDrmYn : $scope.mp3Download,
                sellStreamYn : $scope.stream,
                labelCd : $cookies.get("labelCd"),
                workerId : $cookies.get("username"),
                textLyricYn : textLyric,
                mainSongYn : $scope.mainSongYn,
                lcStatusCd : 'CS0001',
                searchKeyword : $scope.searchKeywordSong
            });
        }

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        var valForm = $scope.validationForm();
        console.log(valForm);
        if(valForm  == '') {
            $scope.loadingStart();
            $http.post('/song/add', data, config)
                .success(function (data, status, headers, config) {
                    alert(data.message);
                    $('#songsModal').modal('hide');
                    $scope.getSongList();
                    //$scope.getSongNotCompleted();
                    $scope.loadingStop();
                })
                .error(function (data, status, header, config) {
                    alert(data.message);
                    $scope.loadingStop();
                });
        }else{
            alert("Field : \n"+valForm+"Can't be Empty, Please input !");
        }
    }

    $scope.addAlbumId = function (el) {
        console.log(el);
        $scope.albumId = el.albumId;
        $scope.albumName = el.albumName;
        $('#albumSearchModal').modal('hide');
    }

    $scope.addArtistId = function (el) {
        if($scope.isFormAlbum == 1){
            $scope.artistId = el.artistId;
            $scope.artistName = el.artistName;
        }else{
            $scope.artistIdSong = el.artistId;
            $scope.artistNameSong = el.artistName;
        }

        $('#artistSearchModal').modal('hide');
    }

    $scope.tabClick = function (el) {
        $scope.isCommercial = el;
        $scope.getSongList2();
        console.log( $scope.isCommercial)
    }
    $scope.showButtonAllSongMod = true;
    $scope.getSongModList = function (el) {
        $http.get("/songMod/list?songId="+el)
            .then(function(response) {
                $scope.songModList = response.data;
                $scope.ulrMp3x = $scope.songModList[0].fullFilePath;
                console.log(response);
                var i = 0;
                angular.forEach($scope.songModList, function (el){
                    console.log(el.fullFilePath);
                    if(el.fullFilePath == ''){
                        i = 1;
                    }
                });
                console.log(i);
                if(i > 0){
                    $scope.showButtonAllSongMod = true;
                }else{
                    $scope.showButtonAllSongMod = false;
                }
            });
    }
    $scope.songModAll = "";
    $scope.showEncodeModal = function (el) {
        $('#encoderModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $scope.songModAll = el;
        $scope.getSongModList(el);
        $('#encoderModal').modal('show');
    }

    $scope.encodeAll = function () {
        var data = $.param({
            songId : $scope.songModAll
        });

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        $scope.loadingStart();
        $http.post('/songMod/job', data, config)
            .success(function (data, status, headers, config) {
                alert(data.message);
                $scope.loadingStop();
                $('#encoderModal').modal('hide');
            })
            .error(function (data, status, header, config) {
                alert(data.message);
                $scope.loadingStop();
            });
    }

    $scope.uploadFileMp3 = function(){
        var file = $scope.myFile;
        // if(file.size >= 100000000){
        //     alert("File tidak boleh lebih besar dari 100MB");
        //     return;
        // }
        console.log('file is ' );
        console.dir(file);

        var uploadUrl = "/fileUpload";
        var rslt = {};
        if(file != undefined){
            $scope.loadingStart();
            var fd = new FormData();
            angular.forEach(file, function (files) {
                fd.append('file', files);
            });
                $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                })

                    .success(function (data) {
                        console.log(data)
                        //rslt = data;
                        $scope.ulrMp3 = data.message;
                        $scope.originalFileName = data.message;
                        $scope.loadingStop();
                    })

                    .error(function (data) {
                        alert(data.message)
                        $scope.loadingStop();
                    });
        }else{
            alert("Please Browse the MP3 File")
        }
        //$scope.loadingStop();
        // console.log(rslt)
        //
        // //var dataUpload = fileUpload.uploadFileToUrl(file, uploadUrl);
        // //console.log(dataUpload);
        // $scope.ulrMp3 = "/upload/music" + rslt.message;
    };


    $scope.uploadFileLyric = function(){
        //console.log($scope.myFile1)
        var file = $scope.myFile1;

        console.log('file is ' );
        console.dir(file);

        var uploadUrl = "/fileUpload";
        var rslt = {};
        if(file != undefined){
            $scope.loadingStart();
            var fd = new FormData();
            angular.forEach(file, function (files) {
                fd.append('file', files);
            });
                $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                })

                    .success(function (data) {
                        console.log(data)
                        //rslt = data;
                        $scope.ulrLyric = data.message;
                        $scope.loadingStop();
                    })

                    .error(function (data) {
                        alert(data.message)
                        $scope.loadingStop();
                    });
        }else{
            alert("Please Browse the Lyric File")
        }

    };
    //$scope.lagu="/sjdsd.mp3";
    $scope.getMusicUrl = function (ss) {
        if(ss == ''){
            return '';
        }else{
            // var sp = ss.split("/");
            // var sp = ss.split("/");
            // var ssl = "/music/";
            // if(sp[1] == 'mediaNASX'){
            //     ssl = "/musix/";
            //     return ssl+sp[sp.length - 3]+'/'+sp[sp.length - 2]+'/'+sp[sp.length - 1];
            // }else if(sp[1] == 'mediaNAS1'){
            //     ssl =  "/image1/";
            // }
            // // console.log(sp[sp.length - 2]);
            // // console.log(sp[sp.length - 3]);
            // return ssl+sp[sp.length - 1];
            return ss;
        }
    }

    $scope.isPlay = 0;
    $scope.playSong = function (el) {
        var urlMusic = $scope.getMusicUrl(el);
        $scope.audio = ngAudio.load(urlMusic);
        $scope.isPlay = 1;
        if($scope.audio.paused){
            $scope.audio.pause();
        }else{
            $scope.audio.play();
        }

        console.log($scope.audio);
    }

    $scope.stopSong = function () {
        $scope.isPlay = 0;
        $scope.audio.pause();
    }

    $scope.saveEncoder = function (el,filePath) {
        var data = $.param({
            songModId : el.songModId,
            songId : el.songId,
            sampling :el.sampling,
            codecTypeCd : el.codecTypeCd,
            bitRateCd : el.bitRateCd,
            playTime : el.playTime,
            fileSize : el.fileSize,
            originalFileName : filePath,
            fullFilePath : filePath,
            workerId :  el.workerId,
            status :  el.status
        });

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        $scope.loadingStart();
        $http.post('/songMod/add', data, config)
            .success(function (data, status, headers, config) {
                // alert(data.message);
                // $('#songsModal').modal('hide');
                $scope.getSongModList(el.songId);
                //$scope.getSongList();
                $scope.loadingStop();
            })
            .error(function (data, status, header, config) {
                alert(data.message);
                $scope.loadingStop();
            });
    }

    $scope.encoder = function (el) {
        if($scope.ulrMp3x != ''){
            var type_bit,targetx;
            if(el.codecTypeCd == 'CT0001'){
                targetx = 'mp3';
            }else if(el.codecTypeCd == 'CT0007' ){
                targetx ='m4a';
            }else if(el.codecTypeCd == 'CT0008'){
                targetx = 'flac';
            }else if(el.codecTypeCd == 'CT0002'){
                targetx = 'm4a';
            }


            if(el.bitRateCd == "BR0320"){
                type_bit = "320";
            }else if(el.bitRateCd == "BR0128"){
                type_bit = "128";
            }else if(el.bitRateCd == "BR0192"){
                type_bit = "192";
            }else if(el.bitRateCd == "BR0096"){
                type_bit = "96";
            }else if(el.bitRateCd == "BR1411"){
                type_bit = "1411";
            }else if(el.bitRateCd == "BR0256"){
                type_bit = "256";
            }
            //console.log(type_bit);
            $scope.loadingStart();
            $http.get("http://cmsv2.melon.co.id/encoder/encode_wav",{
                params: {
                    song_path: $scope.ulrMp3x,
                    bit_rate : type_bit,
                    song_id:el.songId,
                    target : targetx
                }
            })
                .success(function (response) {
                    console.log(response);
                    if(response.length <= 70){
                        if(response != "-1"){
                            $scope.saveEncoder(el,response);
                        }
                    }else{
                        alert('Encode Gagal')  ;
                    }
                    $scope.loadingStop();
                })

                .error(function (data) {
                    console.log(response);
                    alert('Encode Gagal')  ;
                    $scope.loadingStop();
                });
        }
    }

    $scope.showModalMlb = function () {

        $('#mlbSearchModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#mlbSearchModal').modal('show');
    }
    $scope.pageMlb = 1;
    $scope.offsetMlb = 0;
    $scope.totalPageMlb = 0;
    $scope.getMlbCode = function () {
        var srcMlb = document.getElementById("srcMlb").value;
        var artistMlb = '';
        var songMlb = '';
        if(srcMlb == 'artistMlb'){
            artistMlb = $scope.searchValue;
        }else{
            songMlb = $scope.searchValue;
        }

        if($scope.pageMlb <= 1){
            $scope.offsetMlb = 0;
        }else{
            $scope.offsetMlb = ($scope.pageMlb - 1 ) * 10;
        }
        $scope.loadingStart();
        $http.get("/song/getMlbCode",   {
            params: {
                offset: $scope.offsetMlb,
                searchArtist : artistMlb,
                searchSong : songMlb,
                limit : 10
            }
        })
            .then(function(response) {
                if ( response.data.code != "0") {
                    $scope.dataError = 'No Data Found';
                }else{
                    $scope.dataMlb = response.data.data;
                    //$scope.pageMlb = 1;
                    $scope.totalPageMlb = Math.ceil(parseInt(response.data.totalSize) / 10);
                }
                $scope.loadingStop();
            }, function() {
                $scope.dataError = 'No Data Found';
                $scope.loadingStop();
            });
    }

    $scope.getAwalPageMlb = function () {
        $scope.pageMlb = 1;
        $scope.getMlbCode();
    }
    $scope.getPrevPageMlb = function () {
        $scope.pageMlb = $scope.pageMlb - 1;
        $scope.getMlbCode();
    }
    $scope.getNextPageMlb = function () {
        $scope.pageMlb = $scope.pageMlb + 1;
        $scope.getMlbCode();
    }
    $scope.getAkhirPageMlb = function () {
        $scope.pageMlb = $scope.totalPageMlb;
        $scope.getMlbCode();
    }

    $scope.addMlbCode = function (el) {
        $scope.mlb = el.musicCode;
        $scope.grid  = el.isrc;
        $('#mlbSearchModal').modal('hide');
    }
});
