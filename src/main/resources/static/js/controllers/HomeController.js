/* Setup blank page controller */
angular.module('MetronicApp').controller('HomeController', ['$rootScope', '$scope', 'settings','$cookies','$http',
    function($rootScope, $scope, settings,$cookies,$http) {
    $scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        App.initAjax();
        Dashboard.init($cookies.get("labelCd"));
        // set default melyx mode

        $http.get("/dashboard/list",   {
            params: {
                labelCd : $cookies.get("labelCd")
            }
        })
            .then(function(response) {
                // if ( response.data.code != "0") {
                //     $scope.dataError = 'No Data Found';
                // }else{
                //     console.log("CCCC");
                //     $scope.data = response.data;
                // }
               // console.log(response);
                $scope.data = response.data;
            }, function(x) {
                $scope.dataError = 'No Data Found';
            });

    });
}]);
