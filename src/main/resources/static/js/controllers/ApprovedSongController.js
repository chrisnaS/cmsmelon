angular.module('MetronicApp').controller('ApprovedSongController', function($rootScope, $scope, $http, $timeout, $state, ngAudio,$cookies) {
    $scope.$on('$viewContentLoaded', function() {
        App.initAjax(); // initialize core components
        Layout.setAngularJsSidebarMenuActiveLink('set', $('#sidebar_menu_link_profile'), $state); // set profile link active in sidebar menu
    });
    $scope.ulrMp3x = '';
    $scope.page = 1;
    $scope.lcSatatus = 'CS0001';
    $scope.loadingStart = function () {
        $('#loaderModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#loaderModal').modal('show');
    }
    $scope.loadingStop = function () {
        $('#loaderModal').modal('hide');
    }
    $scope.getSongList = function () {
        var srcCategory = document.getElementById("srcSelect").value;
        var srch = "";
        if($scope.mdlSearch != undefined){
            srch = $scope.mdlSearch;
        }

        if($scope.page == 1){
            var offsets = 0;
        }else{
            var offsets = ($scope.page - 1) * 10 ;
        }
        //$scope.loadingStart();
        $http.get("/song/list",   {
            params: {
                offset: offsets,
                search : srch,
                category : srcCategory,
                labelCd : $cookies.get("labelCd"),
                isCommercialYn : 'N',
                lcStatusCd : $scope.lcSatatus
            }
        })
            .then(function(response) {
                if ( response.data.code != "0") {
                    $scope.dataError = 'No Data Found';
                }else{
                    $scope.data = response.data.data;
                    //console.log($scope.data)
                    $scope.totalPage = Math.ceil(parseInt(response.data.totalSize) / 10)
                }
                //$scope.loadingStop();
            }, function(x) {
                $scope.dataError = 'No Data Found';
                //$scope.loadingStop();
            });
    }

    $scope.getSongList2 = function () {
        var srcCategory = document.getElementById("srcSelect").value;
        var srch = "";
        if($scope.mdlSearch != undefined){
            srch = $scope.mdlSearch;
        }

        if($scope.page == 1){
            var offsets = 0;
        }else{
            var offsets = ($scope.page - 1) * 10 ;
        }
        $scope.loadingStart();
        $http.get("/song/list",   {
            params: {
                offset: offsets,
                search : srch,
                category : srcCategory,
                labelCd : $cookies.get("labelCd"),
                isCommercialYn : 'N',
                lcStatusCd : $scope.lcSatatus
            }
        })
            .then(function(response) {
                if ( response.data.code != "0") {
                    $scope.dataError = 'No Data Found';
                }else{
                    $scope.data = response.data.data;
                    //console.log($scope.data)
                    $scope.totalPage = Math.ceil(parseInt(response.data.totalSize) / 10)
                }
                $scope.loadingStop();
            }, function(x) {
                $scope.dataError = 'No Data Found';
                $scope.loadingStop();
            });
    }

    $scope.tabClick = function (el) {
        $scope.isApproved = el;
        if(el == 0){
            $scope.lcSatatus = 'CS0001';
        }else if(el == 1){
            $scope.lcSatatus = 'CS0002';
        }else{
            $scope.lcSatatus = 'CS0006';
        }
        $scope.getSongList2();
    }
    $scope.tabClick(0);

    $scope.getNextPage = function(){
        $scope.page = $scope.page + 1;
        $scope.getSongList2();
    }

    $scope.getPrevPage = function(){
        $scope.page = $scope.page - 1;
        $scope.getSongList2();
    }
    $scope.getAwalPage = function(){
        $scope.page = 1;
        $scope.getSongList2();
    }

    $scope.getAkhirPage = function(){
        $scope.page = $scope.totalPage;
        $scope.getSongList2();
    }
    $scope.songModAll = "";
    $scope.showEncodeModal = function (el) {
        $('#encoderModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $scope.songModAll = el;
        $scope.getSongModList(el);
        $('#encoderModal').modal('show');
    }

    $scope.getSongModList = function (el) {
        $http.get("/songMod/list?songId="+el)
            .then(function(response) {
                $scope.songModList = response.data;
                console.log(response);
                //$scope.ulrMp3x = '';
                $scope.ulrMp3x = $scope.songModList[0].fullFilePath;
                console.log($scope.ulrMp3)
            });
    }

    $scope.encodeAll = function () {
        var data = $.param({
            songId : $scope.songModAll
        });

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        $scope.loadingStart();
        $http.post('/songMod/job', data, config)
            .success(function (data, status, headers, config) {
                alert(data.message);
                $scope.loadingStop();
            })
            .error(function (data, status, header, config) {
                alert(data.message);
                $scope.loadingStop();
            });
    }

    $scope.ApproveSong = function (el) {
        var data = $.param({
            songId : el.songId,
            lcStatusCd : 'CS0002'
        });

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        $scope.loadingStart();
        $http.post('/song/approvedSong', data, config)
            .success(function (data, status, headers, config) {
                console.log(data);
                if(data.msg == 1){
                        alert('Request Approve Song berhasil');
                    $scope.getSongList();
                }else{
                    alert('Encode Song not Complate ! Please Encode this Song ');
                }
                $scope.loadingStop();
            })
            .error(function (data, status, header, config) {
                alert(data.message);
                $scope.loadingStop();
            });
    }

    $scope.saveEncoder = function (el,filePath) {
        var data = $.param({
            songModId : el.songModId,
            songId : el.songId,
            sampling :el.sampling,
            codecTypeCd : el.codecTypeCd,
            bitRateCd : el.bitRateCd,
            playTime : el.playTime,
            fileSize : el.fileSize,
            originalFileName : filePath,
            fullFilePath : filePath,
            workerId :  el.workerId,
            status :  el.status
        });

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        $scope.loadingStart();
        $http.post('/songMod/add', data, config)
            .success(function (data, status, headers, config) {
                // alert(data.message);
                // $('#songsModal').modal('hide');
                $scope.getSongModList(el.songId);
                //$scope.getSongList();
                $scope.loadingStop();
            })
            .error(function (data, status, header, config) {
                alert(data.message);
                $scope.loadingStop();
            });
    }

    $scope.encoder = function (el) {
        if($scope.ulrMp3x != ''){
            var type_bit,targetx;
            if(el.codecTypeCd == 'CT0001'){
                targetx = 'mp3';
            }else{
                targetx ='m4a';
            }


            if(el.bitRateCd == "BR0320"){
                type_bit = "320";
            }else if(el.bitRateCd == "BR0128"){
                type_bit = "128";
            }else if(el.bitRateCd == "BR0192"){
                type_bit = "192";
            }else if(el.bitRateCd == "BR0096"){
                type_bit = "96";
            }
            //console.log(type_bit);
            $scope.loadingStart();
            $http.get("http://cmsv2.melon.co.id/encoder/encode_wav",{
                params: {
                    song_path: $scope.ulrMp3x,
                    bit_rate : type_bit,
                    song_id:el.songId,
                    target : targetx
                }
            })
                .success(function (response) {
                    console.log(response);
                    if(response.length <= 70){
                        if(response != "-1"){
                            $scope.saveEncoder(el,response);
                        }
                    }else{
                        alert('Encode Gagal')  ;
                    }
                    $scope.loadingStop();
                })

                .error(function (data) {
                    console.log(response);
                    alert('Encode Gagal')  ;
                    $scope.loadingStop();
                });
        }
    }

    // $scope.loadingStart = function () {
    //     $('#loaderModal').modal({
    //         backdrop: 'static',
    //         keyboard: false
    //     })
    //     $('#loaderModal').modal('show');
    // }

    $scope.loadingStop = function () {
        $('#loaderModal').modal('hide');
    }

    $scope.getMusicUrl = function (ss) {
        if(ss == ''){
            return '';
        }else{
            // var sp = ss.split("/");
            // var sp = ss.split("/");
            // var ssl = "/music/";
            // if(sp[1] == 'mediaNASX'){
            //     ssl = "/musix/";
            // }else if(sp[1] == 'mediaNAS1'){
            //     ssl =  "/image1/";
            // }
            // return ssl+sp[sp.length - 1];
            return ss;
        }
    }

    $scope.playSong = function (el) {
        var urlMusic = $scope.getMusicUrl(el);
        $scope.audio = ngAudio.load(urlMusic);
        $scope.isPlay = 1;
        if($scope.audio.paused){
            $scope.audio.pause();
        }else{
            $scope.audio.play();
        }

        console.log($scope.audio);
    }
});