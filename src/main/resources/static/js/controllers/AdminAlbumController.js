angular.module('MetronicApp').controller('AdminAlbumController', function($rootScope, $scope, $http, $timeout, $state, $cookies) {
    $scope.$on('$viewContentLoaded', function() {
        App.initAjax(); // initialize core components
        Layout.setAngularJsSidebarMenuActiveLink('set', $('#sidebar_menu_link_profile'), $state); // set profile link active in sidebar menu
    });

    $rootScope.settings.layout.pageBodySolid = true;
    $rootScope.settings.layout.pageSidebarClosed = true;
    $scope.roleTypes = $cookies.get("roleType");
    // console.log($scope.roleTypes)
    $scope.page = 1;
    $scope.lcStatusCd = "CS0001";
    $scope.domesticYn = "Y";
    $scope.adultYn = "Y";
    $scope.isEdit = false;
    $scope.page = 1;
    $scope.AlbumArtistList = [];
    $scope.AlbumArtist = {artistId:'',artistName:''};
    $scope.ulrMp3x = '';
    $scope.ulrImg = ""

    $scope.loadingStart = function () {
        $('#loaderModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#loaderModal').modal('show');
    }

    $scope.loadingStop = function () {
        $('#loaderModal').modal('hide');
    }

    $scope.addArtistAlbum = function (el) {
        var file = $scope.AlbumArtistList;
        if(file.length > 0) {
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }
            angular.forEach(file, function (files) {
                var data = $.param({
                    albumId: el,
                    artistId: files.artistId
                });
                $http.post('/album/insertAlbumArtist', data, config)
                    .success(function (data, status, headers, config) {
                        console.log(data);
                    })
                    .error(function (data, status, header, config) {

                    });
            });
        }
    }

    $scope.tabClick = function (el) {
        console.log(el)
        $scope.lcStatusCd = el;
        $scope.getAlbumList2();
        if($scope.lcStatusCd == 'CS0001'){
            $scope.isEdit = false;
        }else{
            $scope.isEdit = true;
        }
    };


    $http.get("/albumType/list")
        .then(function(response) {
            $scope.dataAlbumType = response.data;
            console.log(response);
        });
    $http.get("/genre/list")
        .then(function(response) {
            $scope.dataGenre = response.data;
            console.log(response);
        });
    $scope.getAlbumList = function () {
        var srcCategory = document.getElementById("srcSelect").value;
        var srch = "";
        $scope.selectedAll = false;
        $scope.albumIds = [];
        if($scope.mdlSearch != undefined){
            srch = $scope.mdlSearch;
        }
        console.log($scope.mdlSearch)
        if($scope.page == 1){
            var offsets = 0;
        }else{
            var offsets = ($scope.page - 1) * 10 ;
        }
        //$scope.loadingStart();
        $http.get("/album/list",   {
            params: {
                offset: offsets,
                search : srch,
                category : srcCategory,
                labelCd : '',
                lcStatusCd : $scope.lcStatusCd
            }
        })
            .then(function(response) {
                if ( response.data.code != "0") {
                    $scope.dataError = 'No Data Found';
                }else{
                    $scope.data = response.data.data;
                    //console.log($scope.data)
                    $scope.totalPage = Math.ceil(parseInt(response.data.totalSize) / 10)
                }
                //$scope.loadingStop();
            }, function(x) {
                $scope.dataError = 'No Data Found';
               // $scope.loadingStop();
            });
    }

    $scope.getAlbumList2 = function () {
        var srcCategory = document.getElementById("srcSelect").value;
        var srch = "";
        $scope.selectedAll = false;
        $scope.albumIds = [];
        if($scope.mdlSearch != undefined){
            srch = $scope.mdlSearch;
        }
        console.log($scope.mdlSearch)
        if($scope.page == 1){
            var offsets = 0;
        }else{
            var offsets = ($scope.page - 1) * 10 ;
        }
        $scope.loadingStart();
        $http.get("/album/list",   {
            params: {
                offset: offsets,
                search : srch,
                category : srcCategory,
                labelCd : '',
                lcStatusCd : $scope.lcStatusCd
            }
        })
            .then(function(response) {
                if ( response.data.code != "0") {
                    $scope.dataError = 'No Data Found';
                }else{
                    $scope.data = response.data.data;
                    //console.log($scope.data)
                    $scope.totalPage = Math.ceil(parseInt(response.data.totalSize) / 10)
                }
                $scope.loadingStop();
            }, function(x) {
                $scope.dataError = 'No Data Found';
                $scope.loadingStop();
            });
    }

    $scope.getAlbumList2();
    $scope.searchButton = function () {
        $scope.page = 1;
        $scope.getAlbumList2();
    }

    $scope.getNextPage = function(){
        $scope.page = $scope.page + 1;
        $scope.getAlbumList2();
    }

    $scope.getPrevPage = function(){
        $scope.page = $scope.page - 1;
        $scope.getAlbumList2();
    }
    $scope.getAwalPage = function(){
        $scope.page = 1;
        $scope.getAlbumList2();
    }

    $scope.getAkhirPage = function(){
        $scope.page = $scope.totalPage;
        $scope.getAlbumList2();
    }
    $scope.getAlbumArtist = function (el) {
        $scope.AlbumArtistList = [];
        $http.get("/album/artist?albumId="+el)
            .then(function(response) {
                var albumArtiistListx = response.data;
                if(albumArtiistListx.length > 0){
                    angular.forEach(albumArtiistListx, function (el) {
                        $scope.AlbumArtistList.push({artistId: el.artistId,artistName:el.artistName});
                    });
                }
            });
    }
    $scope.isApprove = 0;
    $scope.showModalAlbumEdit = function (el) {
        console.log(el);
        $scope.getAlbumArtist(el.albumId);
        //$scope.getAlbumAvailable(el.albumId);
        $scope.isSongForm = 1;
        $scope.albumId = el.albumId;
        $scope.albumName = el.albumName;
        $scope.albumNameOrigin = el.albumNameOrigin;
        $scope.mainSongId = el.mainSongId;
        $scope.artistId = el.mainArtistId;
        $scope.albumType = el.albumTypeCd;
        $scope.seriesNo = parseInt(el.seriesNo);
        $scope.diskNo = parseInt(el.diskCnt);
        $scope.songName = el.songName;
        $scope.artistName = el.artistName
        $scope.releaseDate = new Date(el.issueDateStr);
        $scope.adultYn = el.adultYn;

        $scope.genre = el.genreId;

        //$scope.labelName=el.labelCd;
        $scope.searchKeyword=el.searchKeyword;
        $scope.albumDescription=el.albumReview;

        $scope.ulrImg = el.albumLImgPath;

        $scope.isEdit = true;
        $scope.albumId = el.albumId;
        $scope.sellCompany = el.sellCompany;

        if(el.lcStatusCd == 'CS0001'){
            $scope.isApprove = 0;
        }else {
            $scope.isApprove = 1;
        }
        $('#albumModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#albumModal').modal('show');
    }

    $scope.saveAlbum = function(){
        var IssueDate = $scope.convertDate($scope.releaseDate);
        console.log($scope.diskNo)
        if($scope.diskNo == NaN){
            $scope.diskNo = '';
        }

        if($scope.isEdit == false){
            var data = $.param({
                albumName : $scope.albumName,
                albumNameOrigin : $scope.albumNameOrigin,
                mainSongId :$scope.mainSongId,
                mainArtistId : $scope.artistId,
                albumTypeCd : $scope.albumType,
                seriesNo : $scope.seriesNo,
                diskCnt : $scope.diskNo,
                issueDate : IssueDate,
                searchKeyword : $scope.searchKeyword,
                albumReview : $scope.albumDescription,
                albumLImgPath : $scope.ulrImg,
                genreId : $scope.genre,
                labelCd : $cookies.get("labelCd"),
                workerId : $cookies.get("username"),
                domesticYn : $scope.domesticYn,
                adultYn : $scope.adultYn,
                sellCompany : $scope.sellCompany
            });
        }else{
            var data = $.param({
                albumId : $scope.albumId,
                albumName : $scope.albumName,
                albumNameOrigin : $scope.albumNameOrigin,
                mainSongId :$scope.mainSongId,
                mainArtistId : $scope.artistId,
                albumTypeCd : $scope.albumType,
                seriesNo : $scope.seriesNo,
                diskCnt : $scope.diskNo,
                issueDate : IssueDate,
                labelCd : $cookies.get("labelCd"),
                searchKeyword : $scope.searchKeyword,
                albumReview : $scope.albumDescription,
                albumLImgPath : $scope.ulrImg,
                genreId : $scope.genre,
                workerId : $cookies.get("username"),
                domesticYn : $scope.domesticYn,
                adultYn : $scope.adultYn,
                sellCompany : $scope.sellCompany
            });
        }

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        var valForm = $scope.validationForm();
        console.log(valForm);
        if(valForm  == '') {
            $scope.loadingStart();
            $http.post('/album/add', data, config)
                .success(function (data, status, headers, config) {
                    var albumIdxx = '';
                    if($scope.isEdit == false){
                        $scope.isSongForm = 1;
                        var str = data.message;
                        var res = str.split("|");
                        alert(res[0]);
                        albumIdxx = res[1];
                        $scope.getAlbumAvailable(res[1]);
                        $scope.albumId = res[1];
                        $scope.isEdit = true;
                    }else{
                        var str = data.message;
                        var res = str.split("|");
                        albumIdxx = res[1];
                        alert(res[0]);
                        $scope.isSongForm = 1;
                        $('#albumModal').modal('hide');
                    }
                    //$scope.deleteArtistAlbum(albumIdxx);

                    $scope.addArtistAlbum(albumIdxx);

                    $scope.loadingStop();

                })
                .error(function (data, status, header, config) {
                    alert(data.message);
                    $scope.loadingStop();
                });
        }else{
            alert("Field : \n"+valForm+"Can't be Empty, Please input !");
        }
    }
    $scope.convertDate = function (el) {
        var d = new Date(el),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return year + month + day;
    }
    $scope.showImage = function () {
        $('#imageModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#imageModal').modal('show');
    }

    $scope.validationForm = function () {

        var rslt = "";

        if($scope.albumName == "" || $scope.albumName == undefined) rslt += "Album Name \n";
        // if($scope.albumNameOrigin == '' || $scope.albumNameOrigin == undefined){
        //     rslt += "Album Name Origin \n";
        // }
        if($scope.artistId == '' || $scope.artistId == undefined){
            rslt += "Artist \n";
        }
        if($scope.albumType == '' || $scope.albumType == undefined){
            rslt += "Album Type \n";
        }
        if($scope.seriesNo == '' || $scope.seriesNo == undefined){
            rslt += "Number of Song \n";
        }
        if($scope.releaseDate == '' || $scope.releaseDate == undefined){
            rslt += "Release Date \n";
        }
        if($scope.searchKeyword == '' || $scope.searchKeyword == undefined){
            rslt += "Search Keyword \n";
        }
        if($scope.ulrImg == '' || $scope.ulrImg == undefined){
            rslt += "Image \n";
        }
        if($scope.genre == '' || $scope.genre == undefined){
            rslt += "genre \n";
        }
        return rslt;
    }

    $scope.ApproveAlbum = function (el) {
        var data = $.param({
            albumId : el
        });

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        $scope.loadingStart();
        $http.post('/album/approve', data, config)
            .success(function (data, status, headers, config) {
                alert(data.message);
                $scope.loadingStop();
                $scope.getAlbumList();
            })
            .error(function (data, status, header, config) {
                $scope.loadingStop();
                alert(data.message);
            });
    }

    $scope.DeleteAlbum = function (el) {
        var data = $.param({

        });

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        $scope.loadingStart();
        $http.post('/album/delete/'+el, data, config)
            .success(function (data, status, headers, config) {
                alert(data.message);
                $scope.loadingStop();
                $scope.getAlbumList();
            })
            .error(function (data, status, header, config) {
                $scope.loadingStop();
                alert(data.message);
            });
    }

    $scope.showImage = function () {
        $('#imageModal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#imageModal').modal('show');
    }
    $scope.clearForm = function (){
        $scope.AlbumArtistList = [];
        $scope.albumId = "";
        $scope.albumName = "";
        $scope.albumNameOrigin = "";
        $scope.mainSongId = "";
        $scope.artistId = "";
        $scope.albumType = "";
        $scope.seriesNo = "";
        $scope.diskNo = "";
        $scope.songName = "";
        $scope.artistName = "";
        $scope.releaseDate = "";
        $scope.labelName="";
        $scope.searchKeyword="";
        $scope.albumDescription="";
        $scope.albumId = "";
        $scope.domesticYn = "Y";
        $scope.adultYn = "N";
        $scope.ulrImg = "";
        $scope.genre = "";
    }

    $scope.uploadFile = function(){
        var file = $scope.myFile;
        var img;
        var uploadUrl = "/fileUpload";
        var rslt = {};
        var fileUpload = document.getElementById("upload-photo");

        var reader = new FileReader();

        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
        var isValid;
        if (regex.test(fileUpload.value.toLowerCase())) {
            var reader = new FileReader();

            reader.readAsDataURL(fileUpload.files[0]);
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();

                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;

                //Validate the File Height and Width.
                image.onload = function () {
                    var height = this.height;
                    var width = this.width;
                    console.log(height);
                    console.log(width);
                    if (height < 100 ) {
                        alert("Height and Width must not exceed 100px.");
                        isValid =  false;
                    }else{
                        //isValid = true;
                        var fd = new FormData();
                        $scope.loadingStart();
                        angular.forEach(file, function (files,index,ls,ll,ss) {
                            fd.append('file', files);
                        });
                        $http.post(uploadUrl, fd, {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined}
                        })

                            .success(function (data) {
                                console.log(data)
                                $scope.ulrImg = data.message;
                                $scope.loadingStop();
                            })

                            .error(function (data) {
                                alert(data.message);
                                $scope.loadingStop();
                            });
                    }
                    //alert("Uploaded image has valid Height and Width.");

                };

            }
        } else {
            alert("Please select a valid Image file.");
            isValid = false;
        }
    };

    $scope.getImgUrl = function (ss) {
        if(ss == ''){
            return '';
        }else{
            // var sp = ss.split("/");
            // var ssl = "/image/";
            // if(sp[1] == 'mediaNASX'){
            //     ssl = "/imagex/";
            // }else if(sp[1] == 'mediaNAS1'){
            //     ssl =  "/image1/";
            // }
            // console.log(sp[1]);
            // return ssl+sp[sp.length - 1];
            return ss;
        }
    }

    $scope.deleteImg = function () {
        var data = $.param({
            filename : $scope.ulrImg
        });

        var config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }

        var msg = confirm("Apakah anda ingin menghapus data ini !");
        if(msg == true){
            $scope.loadingStart();
            $http.post('/fileDelete', data, config)
                .success(function (data, status, headers, config) {
                    if(data.code == 1){
                        $scope.ulrImg = "";
                        $scope.getAlbumList();
                    }
                    $scope.loadingStop();
                    alert(data.message);
                })
                .error(function (data, status, header, config) {
                    $scope.loadingStop();
                    alert(data.message);
                });
        }
    }

    $scope.checkItem = function (el,es) {
        if(es == true){
            $scope.albumIds.push(el.albumId);
        }else{
            for(els in $scope.albumIds){
                if(el.albumId == $scope.albumIds[els]){
                    $scope.albumIds.splice(els,1);
                }
            }
        }
        console.log($scope.albumIds)
    }

    $scope.selectedAll = false;
    $scope.albumIds = [];
    $scope.checkAll = function () {
        $scope.albumIds = [];
        // console.log($scope.selectedAll);
        // console.log($scope.data);
        if ($scope.selectedAll) {
            $scope.selectedAll = true;
            angular.forEach($scope.data, function (item) {
                // console.log(item);
                if(item.lcStatusCd == 'CS0001'){
                    item.Selected = $scope.selectedAll;
                    if($scope.selectedAll == true){
                        $scope.albumIds.push(item.albumId);
                    }else{
                        $scope.albumIds = [];
                    }
                }
            });
        } else {
            $scope.selectedAll = false;
            $scope.albumIds = [];
            angular.forEach($scope.data, function (item) {
                // console.log(item);
                if(item.Selected == true){
                    item.Selected = $scope.selectedAll;
                }
            });
        }
        console.log($scope.albumIds);
    };

    $scope.approveBulk = function () {
        var albumId = $scope.convertAlbumId();

        var data = $.param({
            albumIds : albumId
        });

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
        $scope.loadingStart();
        $http.post('/album/approvebulk', data, config)
            .success(function (data, status, headers, config) {
                alert(data.message);
                $scope.getAlbumList();
                $scope.loadingStop();
            })
            .error(function (data, status, header, config) {
                alert(data.message);
                $scope.getAlbumList();
                $scope.loadingStop();
            });
    }

    $scope.convertAlbumId = function () {
        var msg = '';
        if($scope.albumIds.length > 0) {
            for(els in $scope.albumIds){
                msg += $scope.albumIds[els] + "-";
            }
        }
        return msg;
    }
});
