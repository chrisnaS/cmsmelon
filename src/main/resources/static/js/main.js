/***

Metronic AngularJS App Main Script
***/

/* Metronic App */
var MetronicApp = angular.module("MetronicApp", [
    "ui.router", 
    "ui.bootstrap", 
    "oc.lazyLoad",  
    "ngSanitize",
    'ngCookies',
    'ngAudio'
]);

/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
MetronicApp.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);

//AngularJS v1.3.x workaround for old style controller declarition in HTML
MetronicApp.config(['$controllerProvider', function($controllerProvider) {
  // this option might be handy for migrating old apps, but please don't use it
  // in new ones!
  $controllerProvider.allowGlobals();
}]);

/********************************************
 END: BREAKING CHANGE in AngularJS v1.3.x:
*********************************************/

/* Setup global settings */
MetronicApp.factory('settings', ['$rootScope', function($rootScope) {
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageContentWhite: true, // set page content melyx
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        assetsPath: '../assets',
        globalPath: '../assets/global',
        layoutPath: '../assets/layouts/layout4',
    };

    $rootScope.settings = settings;

    return settings;
}]);

/* Setup App Main Controller */
MetronicApp.controller('AppController', ['$scope', '$rootScope','$uibModal', '$cookies','$http', function($scope, $rootScope,$uibModal,$cookies,$http) {
    // console.log($cookies.get("username"));
    //console.log($cookies.get("isLoginYN"));
    // var user = $cookies.user;
    // if(!user){
    //     alert('not logged!');
    //     //$state.go('access.signin');
    //     location.href="/#/home.html";
    // }else {
    //     console.log(user);
    //     console.log($cookies.userName);
    //     $rootScope.user = user;
    //     $rootScope.userName = $cookies.userName;
    // }
    var urls = window.location.pathname;
    console.log(urls)
    if($cookies.get("isLoginYN") == undefined || $cookies.get("isLoginYN") == 'N'){

        location.href="/#/home.html";
        //alert("Not Loged")
    }else if($cookies.get("isLoginYN") == "Y"){
        if(urls == "/"){
            location.href="/homes#/home.html";
        }
        $scope.roleType = $cookies.get("roleType");
//         if(angular.isDefined($cookies.get("user"))){
//             $scope.loggedUser = angular.fromJson($cookies.get("user"));
//             console.log($scope.loggedUser);
// // alert(angular.toJson($scope.loggedUser));
//         }

    }


    $scope.$on('$viewContentLoaded', function() {
        //App.initComponents(); // init core components
        //Layout.init(); //  Init entire melyx(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive
    });

    $scope.enterLogin = function(el){
        if(el.keyCode == 13){
            $scope.login();
        }
    }

    $scope.login = function () {

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }

        var data = $.param({
            userName : $scope.userName,
            password : $scope.password
        });

        $http.post('/auth/user', data, config)
            .success(function (data, status, headers, config) {
                //alert(data);
                    if(data.length > 0){
                        console.log(data)
                        // $rootScope.user = data;
                        // $rootScope.userName = data.userName;
                        // $cookies.user = $rootScope.user;
                        // $cookies.userName = $rootScope.userName;isLoginYN
                        $cookies.put('username', data[0].userName);
                        $cookies.put('isLoginYN', 'Y');
                        $cookies.put('labelCd', data[0].labelCd);
                        $cookies.put('roleType', data[0].roleType);
                        $scope.roleType = $cookies.get("roleType");
                        location.href="/homes#/home.html";
                    }else{
                        alert("Username atau Password Salah !")
                    }
                    var x = $cookies.get("username")
                    console.log(x);
            })
            .error(function (data, status, header, config) {
                alert(data.message);
            });
    }


    $scope.logout = function () {
        location.href="/";
    }

    // modal MLB Code
    var ModalMLBCodeCtrl = function($rootScope,$scope,$uibModalInstance){

        $scope.closeSongView = function () {
            $uibModalInstance.close();

        }
        $scope.selectSongView = function () {
            $uibModalInstance.close();

        }
    }
    $scope.openSongView = function () {
        var modalInstance = $uibModal.open({
            templateUrl: 'songViewModalContent.html',
            controller: ModalMLBCodeCtrl,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            // $log.info('Modal dismissed at: ' + new Date());
        });
    };

    // modal Album View
    var ModalAlbumViewCtrl = function($rootScope,$scope,$uibModalInstance){


        $scope.closeAlbumView = function () {
            $uibModalInstance.close();

        }
        $scope.selectAlbumView = function () {
            $uibModalInstance.close();

        }
    }
    $scope.openAlbumView = function () {
        var modalInstance = $uibModal.open({
            templateUrl: 'albumViewModalContent.html',
            controller: ModalAlbumViewCtrl,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            // $log.info('Modal dismissed at: ' + new Date());
        });
    };

    // modal Artist View
    var ModalArtistViewCtrl = function($rootScope,$scope,$uibModalInstance){


        $scope.closeArtistView = function () {
            $uibModalInstance.close();

        }
        $scope.selectArtistView = function () {
            $uibModalInstance.close();

        }
    }
    $scope.openArtistView = function () {

        $('#birthDate').datepicker({
            format: "dd/mm/yyyy"
        });
        var modalInstance = $uibModal.open({
            templateUrl: 'artistViewModalContent.html',
            controller: ModalArtistViewCtrl,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            // $log.info('Modal dismissed at: ' + new Date());
        });
    };

}]);

/***
Layout Partials.
By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial 
initialization can be disabled and Layout.init() should be called on page load complete as explained above.
***/

/* Setup Layout Part - Header */
MetronicApp.controller('HeaderController', ['$scope','$cookies', function($scope,$cookies) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initHeader(); // init header
    });
    $scope.username = $cookies.get('username');
    $scope.logOutClick = function () {
        $cookies.remove('isLoginYN');
        $cookies.remove('labelCd');
        $cookies.remove('username');
        location.reload();
    }
}]);

/* Setup Layout Part - Sidebar */
MetronicApp.controller('SidebarController', ['$state', '$scope', function($state, $scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initSidebar($state); // init sidebar
    });
}]);

/* Setup Layout Part - Sidebar */
MetronicApp.controller('PageHeadController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {        
        Demo.init(); // init theme panel
    });
}]);

/* Setup Layout Part - Quick Sidebar */
MetronicApp.controller('QuickSidebarController', ['$scope', function($scope) {    
    $scope.$on('$includeContentLoaded', function() {
       setTimeout(function(){
            QuickSidebar.init(); // init quick sidebar        
        }, 2000)
    });
}]);

/* Setup Layout Part - Theme Panel */
MetronicApp.controller('ThemePanelController', ['$scope', function($scope) {    
    $scope.$on('$includeContentLoaded', function() {
        Demo.init(); // init theme panel
    });
}]);

/* Setup Layout Part - Footer */
MetronicApp.controller('FooterController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initFooter(); // init footer
    });
}]);

/* Setup Rounting For All Pages */
MetronicApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    // Redirect any unmatched url
    $urlRouterProvider.otherwise("/home.html");

    $stateProvider

        // MeLyX PAGE
        // HOME
        .state('home', {
            url: "/home.html",
            templateUrl: "views/home.html",
            data: {pageTitle: 'Home'},
            controller: "HomeController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '/extlib/morris/morris.css',
                            '/extlib/morris/morris.min.js',
                            '/extlib/morris/raphael-min.js',
                            '/js/plugin/jquery.sparkline.min.js',

                            '/js/melyx/dashboards.js',
                            '/js/controllers/HomeController.js',
                        ]
                    });
                }]
            }
        })

        // ARTIST
        .state("artist", {
            url: "/artist",
            templateUrl: "views/cms/artist.html",
            data: {pageTitle: 'ARTIST'},
            controller: "ArtistController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            //
                            // '../assets/global/plugins/jquery.sparkline.min.js',
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            //
                            // '../assets/pages/scripts/profile.min.js',
                            'css/components.css',
                            'css/melyx/css/profile.css',
                            'js/controllers/ArtistController.js'
                        ]
                    });
                }]
            }
        })

        // ALBUM
        .state("album", {
            url: "/album",
            templateUrl: "views/cms/album.html",
            data: {pageTitle: 'ALBUM'},
            controller: "AlbumController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            //
                            // '../assets/global/plugins/jquery.sparkline.min.js',
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            //
                            // '../assets/pages/scripts/profile.min.js',
                            'css/components.css',
                            'css/melyx/css/profile.css',
                            'js/controllers/AlbumController.js'
                        ]
                    });
                }]
            }
        })

        // Song
        .state("song", {
            url: "/song",
            templateUrl: "views/cms/song.html",
            data: {pageTitle: 'SONG'},
            controller: "SongController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            //
                            // '../assets/global/plugins/jquery.sparkline.min.js',
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            //
                            // '../assets/pages/scripts/profile.min.js',
                            'css/components.css',
                            'css/melyx/css/profile.css',
                            'js/controllers/SongController.js'
                        ]
                    });
                }]
            }
        })

        .state("changePassword", {
            url: "/changePassword",
            templateUrl: "views/cms/changePassword.html",
            data: {pageTitle: 'Change Password'},
            controller: "ChangePasswordController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            //
                            // '../assets/global/plugins/jquery.sparkline.min.js',
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            //
                            // '../assets/pages/scripts/profile.min.js',
                            'css/components.css',
                            'css/melyx/css/profile.css',
                            'js/controllers/ChangePasswordController.js'
                        ]
                    });
                }]
            }
        })

        //Admin Artist
        .state("adminartist", {
            url: "/adminArtist",
            templateUrl: "views/cms/admin_artist.html",
            data: {pageTitle: 'ADMIN ARTIST'},
            controller: "AdminArtistController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'css/components.css',
                            'css/melyx/css/profile.css',
                            'js/controllers/AdminArtistController.js'
                        ]
                    });
                }]
            }
        })

        // Admin Album
        .state("adminalbum", {
            url: "/adminAlbum",
            templateUrl: "views/cms/admin_album.html",
            data: {pageTitle: 'ADMIN ALBUM'},
            controller: "AdminAlbumController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'css/components.css',
                            'css/melyx/css/profile.css',
                            'js/controllers/AdminAlbumController.js'
                        ]
                    });
                }]
            }
        })

        // Admin Song
        .state("adminsong", {
        url: "/adminSong",
        templateUrl: "views/cms/admin_song.html",
        data: {pageTitle: 'ADMIN SONG'},
        controller: "AdminSongController",
        resolve: {
            deps: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'MetronicApp',
                    insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                    files: [
                        // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                        //
                        // '../assets/global/plugins/jquery.sparkline.min.js',
                        // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                        //
                        // '../assets/pages/scripts/profile.min.js',
                        'css/components.css',
                        'css/melyx/css/profile.css',
                        'js/controllers/AdminSongController.js'
                    ]
                });
            }]
        }
    })
        //User
        .state("users", {
            url: "/users",
            templateUrl: "views/cms/users.html",
            data: {pageTitle: 'USER MANAGEMENT'},
            controller: "UsersController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            //
                            // '../assets/global/plugins/jquery.sparkline.min.js',
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            //
                            // '../assets/pages/scripts/profile.min.js',
                            'css/components.css',
                            'css/melyx/css/profile.css',
                            'js/controllers/UsersController.js'
                        ]
                    });
                }]
            }
        })

        // Add Song
        .state("add_song", {
            url: "/add_song",
            templateUrl: "views/cms/add_new_song.html",
            data: {pageTitle: 'ADD SONG'},
            controller: "SongController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            //
                            // '../assets/global/plugins/jquery.sparkline.min.js',
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            //
                            // '../assets/pages/scripts/profile.min.js',
                            'css/components.css',
                            'css/melyx/css/profile.css',
                            // '/extlib/datepicker/bootstrap-datepicker3.min.css',
                            // '/extlib/datepicker/bootstrap-datepicker.min.js',
                            'js/controllers/SongController.js'
                        ]
                    });
                }]
            }
        })

        // Category
        .state("category", {
            url: "/category",
            templateUrl: "views/cms/classngenre.html",
            data: {pageTitle: 'CATEGORY'},
            controller: "CategoryController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            //
                            // '../assets/global/plugins/jquery.sparkline.min.js',
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            //
                            // '../assets/pages/scripts/profile.min.js',
                            'css/components.css',
                            'css/melyx/css/profile.css',
                            'js/controllers/CategoryController.js'
                        ]
                    });
                }]
            }
        })
        .state("approve_song", {
            url: "/approve_song",
            templateUrl: "views/cms/approved_song.html",
            data: {pageTitle: 'APPROVED SONG'},
            controller: "ApprovedSongController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            //
                            // '../assets/global/plugins/jquery.sparkline.min.js',
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            //
                            // '../assets/pages/scripts/profile.min.js',
                            'css/components.css',
                            'css/melyx/css/profile.css',
                            'js/controllers/ApprovedSongController.js'
                        ]
                    });
                }]
            }
        })

        // Music Video
        .state("mv", {
            url: "/mv",
            templateUrl: "views/cms/mv.html",
            data: {pageTitle: 'MUSIC VIDEO'},
            controller: "MusicVideoController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            //
                            // '../assets/global/plugins/jquery.sparkline.min.js',
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            //
                            // '../assets/pages/scripts/profile.min.js',
                            'css/components.css',
                            'css/melyx/css/profile.css',
                            'js/controllers/MusicVideoController.js'
                        ]
                    });
                }]
            }
        })

        // CHART
        .state("chart", {
            url: "/chart",
            templateUrl: "views/cms/chart.html",
            data: {pageTitle: 'CHART'},
            controller: "ChartController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            //
                            // '../assets/global/plugins/jquery.sparkline.min.js',
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            //
                            // '../assets/pages/scripts/profile.min.js',
                            'css/components.css',
                            'css/melyx/css/profile.css',
                            'js/controllers/ChartController.js'
                        ]
                    });
                }]
            }
        })

        .state("mlb", {
            url: "/mlb",
            templateUrl: "views/cms/mlb.html",
            data: {pageTitle: 'MLB GENERATE CODE'},
            controller: "MlbController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            //
                            // '../assets/global/plugins/jquery.sparkline.min.js',
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            //
                            // '../assets/pages/scripts/profile.min.js',
                            'css/components.css',
                            'css/melyx/css/profile.css',
                            'js/controllers/MlbController.js'
                        ]
                    });
                }]
            }
        })

        .state("label", {
            url: "/label",
            templateUrl: "views/cms/label.html",
            data: {pageTitle: 'LABEL'},
            controller: "LabelController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            //
                            // '../assets/global/plugins/jquery.sparkline.min.js',
                            // '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            //
                            // '../assets/pages/scripts/profile.min.js',
                            'css/components.css',
                            'css/melyx/css/profile.css',
                            'js/controllers/LabelController.js'
                        ]
                    });
                }]
            }
        })

        // Dashboard
        .state('dashboard', {
            url: "/dashboard.html",
            templateUrl: "views/dashboard.html",            
            data: {pageTitle: 'Admin Dashboard Template'},
            controller: "DashboardController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '/extlib/morris/morris.css',
                            '/extlib/morris/morris.min.js',
                            '/extlib/morris/raphael-min.js',
                            '/js/plugin/jquery.sparkline.min.js',

                            '/js/melyx/dashboard.min.js',
                            '/js/controllers/DashboardController.js',
                        ] 
                    });
                }]
            }
        })

        // Blank Page
        .state('blank', {
            url: "/blank",
            templateUrl: "views/blank.html",            
            data: {pageTitle: 'Blank Page Template'},
            controller: "BlankController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            'js/controllers/BlankController.js'
                        ] 
                    });
                }]
            }
        })

        // AngularJS plugins
        .state('fileupload', {
            url: "/file_upload.html",
            templateUrl: "views/file_upload.html",
            data: {pageTitle: 'AngularJS File Upload'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'angularFileUpload',
                        files: [
                            '/extlib/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                        ] 
                    }, {
                        name: 'MetronicApp',
                        files: [
                            'js/controllers/GeneralPageController.js'
                        ]
                    }]);
                }]
            }
        })

        // UI Select
        .state('uiselect', {
            url: "/ui_select.html",
            templateUrl: "views/ui_select.html",
            data: {pageTitle: 'AngularJS Ui Select'},
            controller: "UISelectController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'ui.select',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '/extlib/angularjs/plugins/ui-select/select.min.css',
                            '/extlib/angularjs/plugins/ui-select/select.min.js'
                        ] 
                    }, {
                        name: 'MetronicApp',
                        files: [
                            'js/controllers/UISelectController.js'
                        ] 
                    }]);
                }]
            }
        })

        // UI Bootstrap
        .state('uibootstrap', {
            url: "/ui_bootstrap.html",
            templateUrl: "views/ui_bootstrap.html",
            data: {pageTitle: 'AngularJS UI Bootstrap'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'MetronicApp',
                        files: [
                            'js/controllers/GeneralPageController.js'
                        ] 
                    }]);
                }] 
            }
        })

        // Tree View
        .state('tree', {
            url: "/tree",
            templateUrl: "views/tree.html",
            data: {pageTitle: 'jQuery Tree View'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '/extlib/jstree/dist/themes/default/style.min.css',

                            '/extlib/jstree/dist/jstree.min.js',
                            '/js/melyx/ui-tree.min.js',
                            'js/controllers/GeneralPageController.js'
                        ] 
                    }]);
                }] 
            }
        })     

        // Form Tools
        .state('formtools', {
            url: "/form-tools",
            templateUrl: "views/form_tools.html",
            data: {pageTitle: 'Form Tools'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '/extlib/bootstrap-fileinput/bootstrap-fileinput.css',
                            '/extlib/bootstrap-switch/css/bootstrap-switch.min.css',
                            '/extlib/bootstrap-markdown/css/bootstrap-markdown.min.css',
                            '/extlib/typeahead/typeahead.css',

                            '/extlib/fuelux/js/spinner.min.js',
                            '/extlib/bootstrap-fileinput/bootstrap-fileinput.js',
                            '/extlib/jquery-inputmask/jquery.inputmask.bundle.min.js',
                            '/js/plugin/jquery.input-ip-address-control-1.0.min.js',
                            '/extlib/bootstrap-pwstrength/pwstrength-bootstrap.min.js',
                            '/extlib/bootstrap-switch/js/bootstrap-switch.min.js',
                            '/extlib/bootstrap-maxlength/bootstrap-maxlength.min.js',
                            '/extlib/bootstrap-touchspin/bootstrap.touchspin.js',
                            '/extlib/typeahead/handlebars.min.js',
                            '/extlib/typeahead/typeahead.bundle.min.js',
                            '/js/melyx/components-form-tools-2.min.js',

                            'js/controllers/GeneralPageController.js'
                        ] 
                    }]);
                }] 
            }
        })        

        // Date & Time Pickers
        .state('pickers', {
            url: "/pickers",
            templateUrl: "views/pickers.html",
            data: {pageTitle: 'Date & Time Pickers'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/clockface/css/clockface.css',
                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            '../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                            '../assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css',
                            '../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            '../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                            '../assets/global/plugins/clockface/js/clockface.js',
                            '../assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js',
                            '../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                            '../assets/pages/scripts/components-date-time-pickers.min.js',

                            'js/controllers/GeneralPageController.js'
                        ] 
                    }]);
                }] 
            }
        })

        // Custom Dropdowns
        .state('dropdowns', {
            url: "/dropdowns",
            templateUrl: "views/dropdowns.html",
            data: {pageTitle: 'Custom Dropdowns'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css',
                            '../assets/global/plugins/select2/css/select2.min.css',
                            '../assets/global/plugins/select2/css/select2-bootstrap.min.css',

                            '../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js',
                            '../assets/global/plugins/select2/js/select2.full.min.js',

                            '../assets/pages/scripts/components-bootstrap-select.min.js',
                            '../assets/pages/scripts/components-select2.min.js',

                            'js/controllers/GeneralPageController.js'
                        ] 
                    }]);
                }] 
            }
        }) 

        // Advanced Datatables
        .state('datatablesmanaged', {
            url: "/datatables/managed.html",
            templateUrl: "views/datatables/managed.html",
            data: {pageTitle: 'Advanced Datatables'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [                             
                            '../assets/global/plugins/datatables/datatables.min.css', 
                            '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',

                            '../assets/global/plugins/datatables/datatables.all.min.js',

                            '../assets/pages/scripts/table-datatables-managed.min.js',

                            'js/controllers/GeneralPageController.js'
                        ]
                    });
                }]
            }
        })

        // Ajax Datetables
        .state('datatablesajax', {
            url: "/datatables/ajax.html",
            templateUrl: "views/datatables/ajax.html",
            data: {pageTitle: 'Ajax Datatables'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/datatables/datatables.min.css', 
                            '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',

                            '../assets/global/plugins/datatables/datatables.all.min.js',
                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            '../assets/global/scripts/datatable.js',

                            'js/scripts/table-ajax.js',
                            'js/controllers/GeneralPageController.js'
                        ]
                    });
                }]
            }
        })

        // User Profile
        .state("profile", {
            url: "/profile",
            templateUrl: "views/profile/main.html",
            data: {pageTitle: 'User Profile'},
            controller: "UserProfileController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',  
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            '../assets/pages/css/profile.css',
                            
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',

                            '../assets/pages/scripts/profile.min.js',

                            'js/controllers/UserProfileController.js'
                        ]                    
                    });
                }]
            }
        })

        // User Profile Dashboard
        .state("profile.dashboard", {
            url: "/dashboard",
            templateUrl: "views/profile/dashboard.html",
            data: {pageTitle: 'User Profile'}
        })

        // User Profile Account
        .state("profile.account", {
            url: "/account",
            templateUrl: "views/profile/account.html",
            data: {pageTitle: 'User Account'}
        })

        // User Profile Help
        .state("profile.help", {
            url: "/help",
            templateUrl: "views/profile/help.html",
            data: {pageTitle: 'User Help'}      
        })

        // Todo
        .state('todo', {
            url: "/todo",
            templateUrl: "views/todo.html",
            data: {pageTitle: 'Todo'},
            controller: "TodoController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({ 
                        name: 'MetronicApp',  
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            '../assets/apps/css/todo-2.css',
                            '../assets/global/plugins/select2/css/select2.min.css',
                            '../assets/global/plugins/select2/css/select2-bootstrap.min.css',

                            '../assets/global/plugins/select2/js/select2.full.min.js',
                            
                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',

                            '../assets/apps/scripts/todo-2.min.js',

                            'js/controllers/TodoController.js'  
                        ]                    
                    });
                }]
            }
        })

}]);

/* Init global settings and run the app */
MetronicApp.run(["$rootScope", "settings", "$state", function($rootScope, settings, $state) {
    $rootScope.$state = $state; // state to be accessed from view
    $rootScope.$settings = settings; // state to be accessed from view
}]);