package com.cmsmelon.helper;

import com.cmsmelon.BO.SongBo;
import com.cmsmelon.Model.AlbumAdd;
import com.cmsmelon.Model.ArtistCrud;
import com.cmsmelon.Model.SongAdd;
import com.cmsmelon.Model.SongModAdd;
import com.cmsmelon.Repository.AlbumAddRepository;
import com.cmsmelon.Repository.ArtistAddRepository;
import com.cmsmelon.Repository.SongAddRepository;
import com.cmsmelon.Repository.SongModAddRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by IT19 on 02/11/2017.
 */
@Component
public class CopyFile {
    @Autowired
    private SongModAddRepository songModAddRepository;
    @Autowired
    private AlbumAddRepository albumAddRepository;
    @Autowired
    private ArtistAddRepository artistAddRepository;
    @Autowired
    private SongAddRepository songAddRepository;

    @Value( "${home.todir}" )
    private String pathTo;

    private String imgFilePath = "content/img/";
    private String songFilePath = "content/mod/";
    private String lyricFilePath = "content/lyric/";
    private static final Logger log = LoggerFactory.getLogger(SongBo.class);
    private String getFileName (String fileName){
        String [] ss = fileName.split("/");
        String y = ss[ss.length-1];
        return y;
    }

    public Integer checkFile(Integer songId){
        List<SongModAdd> songMods = songModAddRepository.songModById(songId);
        Integer resp = 1;
        try{
            for(SongModAdd songMod : songMods){
                File f = new File(songMod.getFullFilePath());
                if(!f.exists()) {
                   resp = 0;
                }
            }
        }catch (Exception Ex){
            System.out.println(Ex.getMessage());
        }

        return resp;
    }

    public void CopyFileMp3 (Integer songId, Integer oraSongId, Integer albumProdId) throws IOException {
        String Command = "cp -r ";
        List<SongModAdd> songMods = songModAddRepository.songModById(songId);
        SongAdd songAdd = songAddRepository.songById(songId);
        
        String toPathLyric = "/"+pathTo+"/"+ lyricFilePath+getFileName(songAdd.getLyricPath());
        String commandLyric = Command+songAdd.getLyricPath()+" "+toPathLyric;
        Runtime.getRuntime().exec(commandLyric);
        songAdd.setLyricPath(toPathLyric);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
        Date today = Calendar.getInstance().getTime();
        String monYear = sdf.format(today.getTime());

        String monYearStr = "/"+pathTo+"/"+ songFilePath + monYear+"/";

        File monDir = new File(monYearStr);

        if (! monDir.exists()){
            monDir.mkdir();
        }

        String toDir = monYearStr+albumProdId+"/";
        File directory = new File(toDir);

        if (! directory.exists()){
            directory.mkdir();
        }

        //songFilePath = toDir;

        try{
            for(SongModAdd songMod : songMods){

                String fileExt = ".mp3";
                if(songMod.getCodecTypeCd().equals("CT0000")){
                    fileExt = ".wav";
                }else if(songMod.getCodecTypeCd().equals("CT0001")){
                    fileExt = ".mp3";
                }else if(songMod.getCodecTypeCd().equals("CT0002")){
                    fileExt = ".m4a";
                }else if(songMod.getCodecTypeCd().equals("CT0007")){
                    fileExt = ".m4a";
                }else if(songMod.getCodecTypeCd().equals("CT0008")){
                    fileExt = ".flac";
                }
                String bitStr = songMod.getBitRateCd().replace("BR","");

                Integer bitNum = Integer.parseInt(bitStr);

                String fileNameStr = oraSongId + "_" + bitNum + fileExt;

//                String toPath = songFilePath+getFileName(songMod.getFullFilePath());
                String toPath = toDir + fileNameStr;
                String commands = Command+songMod.getFullFilePath()+" "+toPath;
                Runtime.getRuntime().exec(commands);
                songMod.setFullFilePath(toPath);
                songMod.setOriginalFileName(songMod.getFullFilePath());
                songModAddRepository.save(songMod);
                if(songMod.getCodecTypeCd().equals("CT0000")){
                    songAdd.setFullFilePath(toPath);
                    songAdd.setOriginalFileName(songMod.getFullFilePath());
                }
            }
            songAddRepository.save(songAdd);
        }catch (Exception Ex){

        }

    }

    public void CopyFileAlbumImg (AlbumAdd albumAdd) throws IOException {
        String Command = "cp ";
        //AlbumAdd albumAdd = albumAddRepository.albumById(albumId);

        String toPathL = "/"+pathTo+"/"+imgFilePath+getFileName(albumAdd.getAlbumLImgPath());
        String commandL = Command+albumAdd.getAlbumLImgPath()+" "+toPathL;
        Runtime.getRuntime().exec(commandL);
        albumAdd.setAlbumLImgPath(toPathL);

        String toPathM = "/"+pathTo+"/"+imgFilePath+getFileName(albumAdd.getAlbumMImgPath());
        String commandM = Command+albumAdd.getAlbumMImgPath()+" "+toPathM;
        Runtime.getRuntime().exec(commandM);
        albumAdd.setAlbumMImgPath(toPathM);

        String toPathS = "/"+pathTo+"/"+imgFilePath+getFileName(albumAdd.getAlbumSImgPath());
        String commandS = Command+albumAdd.getAlbumSImgPath()+" "+toPathS;
        Runtime.getRuntime().exec(commandS);
        albumAdd.setAlbumSImgPath(toPathS);

        albumAddRepository.save(albumAdd);
    }

    public void CopyFileArtistImg (ArtistCrud artistCrud) throws IOException {
        String Command = "cp ";
        //ArtistCrud artistCrud= artistAddRepository.artistById(artistId);

        String toPathL = "/"+pathTo+"/"+imgFilePath+getFileName(artistCrud.getArtistLImgPath());
        String commandL = Command+artistCrud.getArtistLImgPath()+" "+toPathL;
        Runtime.getRuntime().exec(commandL);
        artistCrud.setArtistLImgPath(toPathL);

        String toPathM = "/"+pathTo+"/"+imgFilePath+getFileName(artistCrud.getArtistMImgPath());
        String commandM = Command+artistCrud.getArtistMImgPath()+" "+toPathM;
        Runtime.getRuntime().exec(commandM);
        artistCrud.setArtistMImgPath(toPathM);

        String toPathS = "/"+pathTo+"/"+imgFilePath+getFileName(artistCrud.getArtistSImgPath());
        String commandS = Command+artistCrud.getArtistSImgPath()+" "+toPathS;
        Runtime.getRuntime().exec(commandS);
        artistCrud.setArtistSImgPath(toPathS);
        //return "x";
        artistAddRepository.save(artistCrud);
    }

    public void DeleteFile(String fileName){
        String command = "rm -f "+fileName;
        try{
            Runtime.getRuntime().exec(command);
        }catch (Exception ex){
            log.info(ex.getMessage());
        }

    }
}
