package com.cmsmelon.helper;

import com.cmsmelon.Model.AlbumAdd;
import com.cmsmelon.Model.ArtistCrud;
import com.cmsmelon.Repository.AlbumAddRepository;
import com.cmsmelon.Repository.ArtistAddRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by IT19 on 01/11/2017.
 */
@Component
public class ImageResize {
    @Autowired
    ArtistAddRepository artistAddRepository;
    @Value( "${home.dir}" )
    private String pathHome;
    @Autowired
    AlbumAddRepository albumAddRepository;

    public void ResizeAllArtistImage(String filePath,ArtistCrud artistCrud ) throws IOException {
        //ArtistCrud artistCrud = artistAddRepository.artistById(artistId);
        artistCrud.setArtistLImgPath(ResizeImage(filePath,"L"));
        artistCrud.setArtistMImgPath(ResizeImage(filePath,"M"));
        artistCrud.setArtistSImgPath(ResizeImage(filePath,"S"));

        //artistAddRepository.save(artistCrud);
    }

    public void ResizeAllAlbumImage(String filePath, AlbumAdd albumAdd) throws IOException {
        albumAdd.setAlbumLImgPath(ResizeImage(filePath,"L"));
        albumAdd.setAlbumMImgPath(ResizeImage(filePath,"M"));
        albumAdd.setAlbumSImgPath(ResizeImage(filePath,"S"));
    }

    private String ResizeImage(String filePath, String imgType) throws IOException {
        BufferedImage originalImage = ImageIO.read(new File(filePath));
        String rlstFile = "";
        int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
        int height = 0;
        int width = 0;

        String [] ss = filePath.split("/");
        String y = ss[ss.length-1];
        String [] yy = y.split("\\.");
        String z = yy[yy.length-1];
        String d = yy[yy.length-2];

        if(imgType.equals("L")){
            height = 600;
            width = 600;
        }else if(imgType.equals("M")){
            height = 400;
            width = 400;
        }else if(imgType.equals("S")){
            height = 200;
            width = 200;
        }
        rlstFile = pathHome+"image/"+d+"-"+imgType+"."+z;
        BufferedImage resizeImageJpg = resizeImage(originalImage, type, width,height);
        ImageIO.write(resizeImageJpg, "jpg", new File(rlstFile));

        return rlstFile;
    }
    private BufferedImage resizeImage(BufferedImage originalImage, int type, Integer width, Integer height){
        BufferedImage resizedImage = new BufferedImage(width, height, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, width, height, null);
        g.dispose();

        return resizedImage;
    }
}
