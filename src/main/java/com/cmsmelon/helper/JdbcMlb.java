package com.cmsmelon.helper;

import com.cmsmelon.Model.Album;
import com.cmsmelon.Model.MlbModel;
import com.cmsmelon.Model.PagingResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class JdbcMlb {
    private Connection con ;
    private Statement stmt;
    private ResultSet rs;
    private ResultSet rsCount;

    private static final Logger log = LoggerFactory.getLogger(JdbcMlb.class);

    public JdbcMlb(){
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        con = null;
        try {
            con = DriverManager.getConnection(
                    "jdbc:oracle:thin:@(description=(address_list=(address=(host=192.168.10.41)" +
                            "(protocol=tcp)(port=1521))" +
                            "(address=(host=192.168.10.44)(protocol=tcp)(port=1521))" +
                            "(load_balance=yes)(failover=yes))(connect_data=(service_name=MLBDB)))", "mlb", "mlb");
//            System.out.println("cOnnect");
        }catch (Exception ex){
//            System.out.println("Not cOnnect");
//            SendMail sendMail = new SendMail(ex.getMessage(),"SMS OTP Error on", "[Notification] SMS OTP Monitoring");
//            Thread t = new Thread(sendMail);
            //t.start();
            log.info(ex.getMessage());
        }
    }

    public void ConClose() throws SQLException {
        con.close();
    }

    public PagingResponse<List<MlbModel>> getSearchMlb(
            String searchSong,
            String searchArtist,
            Integer offset,
            Integer limit
    ) throws SQLException {
        PagingResponse<List<MlbModel>> response = new PagingResponse<>();
        response.setCode("0");
        response.setStatusCode(200);
        response.setMsg("Report generated successfully");
        response.setData(null);

        List<MlbModel> mlbModels = new ArrayList<MlbModel>();
        Integer cint = 0;

        String sql = "select * from ( " +
                "SELECT " +
                " ROWNUM rowno, " +
                " a.* " +
                " FROM " +
                " ( " +
                "select b.*, FN_MUSIC_CODE_LH_MUSIC_CODE(b.music_code) as ISRC from TABLE(FN_SEARCH_MUSIC('000001', '001', '"+searchSong+"', '"+searchArtist+"')) b" +
                " where service_date is not null order by service_date desc " +
                ") a ) b " +
                "where rowno BETWEEN "+(offset+1)+" and "+(offset+limit);
        System.out.println(sql);
        String sqlCount = "select count(0) as CINT " +
                "from TABLE(FN_SEARCH_MUSIC('000001', '001', '"+searchSong+"', '"+searchArtist+"')) ";

            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()){
                MlbModel mlb = new MlbModel();
                mlb.setMusicCode(rs.getString("MUSIC_CODE"));
                mlb.setAlbumTitle(rs.getString("ALBUM_TITLE"));
                mlb.setMusicGenre(rs.getString("MUSIC_GENRE"));
                mlb.setMusicTitle(rs.getString("MUSIC_TITLE"));
                mlb.setSingerName(rs.getString("SINGER_NAME"));
                mlb.setIsrc(rs.getString("ISRC"));
                mlbModels.add(mlb);
            }
            rsCount = stmt.executeQuery(sqlCount);
            if(rsCount.next()){
                cint = rsCount.getInt("CINT");
            }

            if(mlbModels != null){
                response.setOffset(offset);
                response.setSize(mlbModels.size());
                response.setData(mlbModels);
                response.setTotalSize(cint);
            }else{
                response.setCode("1");
                response.setMsg("No data found in repository");
            }
            this.ConClose();
        return response;
    }
}
