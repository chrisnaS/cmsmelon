package com.cmsmelon.helper;

import com.cmsmelon.Model.*;
import com.cmsmelon.Repository.LabelRepository;
import com.cmsmelon.Repository.SongModAddRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.List;

/**
 * Created by IT19 on 23/08/2017.
 */
@Component
public class JdbcOracle {
    private Connection con ;
    private Statement stmt;
    private ResultSet rs;

    @Autowired
    private LabelRepository labelRepository;

    @Autowired
    private SongModAddRepository songModAddRepository;

    private static final Logger log = LoggerFactory.getLogger(JdbcOracle.class);

    public JdbcOracle() throws ClassNotFoundException, SQLException {
        //con = DataSource.getConnection();
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        con = null;
        try {
 //            con = DriverManager.getConnection(
//                    "jdbc:oracle:thin:@(description=(address_list=" +
//                            "(address=(host=192.168.10.40)(protocol=tcp)(port=1521)) " +
//                            "(address=(host=192.168.10.37)(protocol=tcp)(port=1521)) " +
//                            "(load_balance=yes)(failover=yes))(connect_data=(service_name=MelOndb)))", "MBP001", "51tiB4dr14H111191");


            con = DriverManager.getConnection(
                    "jdbc:oracle:thin:@(description=(address_list=(address=(host=192.168.10.98)(protocol=tcp)" +
                            "(port=1521))(address=(host=192.168.10.99)(protocol=tcp)(port=1521))(load_balance=yes)" +
                            "(failover=yes))(connect_data=(service_name=melondbs)))", "MBP001", "51tiB4dr14H111191");
            //System.out.println("cOnnect");
        }catch (Exception ex){
            //System.out.println("Not cOnnect");

//            SendMail sendMail = new SendMail(ex.getMessage(),"SMS OTP Error on", "[Notification] SMS OTP Monitoring");
//            Thread t = new Thread(sendMail);
            //t.start();
            log.info(ex.getMessage());
        }

    }

    public void ConClose() throws SQLException {
        con.close();
    }

    public Integer getSeqAlbum() throws SQLException {
        stmt = null;
        ResultSet rs = null;
        stmt = con.createStatement();
        Integer rslt = null;
        String sql = "select album_seq.nextval as album_id from dual ";
        rs =stmt.executeQuery(sql);
        if(rs.next()){
            rslt = rs.getInt(1);
        }
        return rslt;
    }

    public Integer getSeqArtist() throws SQLException {
        stmt = null;
        ResultSet rs = null;
        stmt = con.createStatement();
        Integer rslt = null;
        String sql = "select artist_seq.nextval as artist_id from dual ";
        rs =stmt.executeQuery(sql);
        if(rs.next()){
            rslt = rs.getInt(1);
        }
        return rslt;
    }

    public Integer getSeqSong() throws SQLException {
        stmt = null;
        ResultSet rs = null;
        stmt = con.createStatement();
        Integer rslt = null;
        String sql = "select song_seq.nextval as mtTxId from dual ";
        rs =stmt.executeQuery(sql);
        if(rs.next()){
            rslt = rs.getInt(1);
        }
        return rslt;
    }

    public Integer getSeqSongMod() throws SQLException {
        stmt = null;
        ResultSet rs = null;
        stmt = con.createStatement();
        Integer rslt = null;
        String sql = "select SONG_MOD_SEQ.nextval as mtTxId from dual ";
        rs =stmt.executeQuery(sql);
        if(rs.next()){
            rslt = rs.getInt(1);
        }
        return rslt;
    }

    public void InsertArtist(ArtistCrud artist, Integer artistId,String labelName){
        stmt = null;
        rs = null;

        try{
            stmt = con.createStatement();
            String sql = "INSERT INTO ARTIST ( " +
                    "  ARTIST_ID, " +
                    "  ARTIST_NAME, " +
                    "  ARTIST_NAME_ORIGIN, " +
                    "  NICKNAME, " +
                    "  HINT, " +
                    "  GROUP_YN, " +
                    "  ARTIST_ROLE_TYPE_CD, " +
                    "  DOMESTIC_YN, " +
                    "  NATIONALITY_CD, " +
                    "  GENRE_ID, " +
                    "  BIRTHDAY, " +
                    "  DEATHDAY, " +
                    "  DEBUT_DAY, " +
                    "  DEBUT_SONG, " +
                    "  PERFORM_YEARS, " +
                    "  GENDER, " +
                    "  BLOOD_TYPE, " +
                    "  RELIGION, " +
                    "  HOBBY, " +
                    "  ARTIST_PRIORITY, " +
                    "  HOMEPAGE, " +
                    "  TWITTER_URL, " +
                    "  FACEBOOK_URL, " +
                    "  SEARCH_YN, " +
                    "  SEARCH_KEYWORD, " +
                    "  PROFILE, " +
                    "  ARTIST_L_IMG_PATH, " +
                    "  ARTIST_M_IMG_PATH, " +
                    "  ARTIST_S_IMG_PATH, " +
                    "  STATUS, " +
                    "  WORKER_ID, " +
                    "  REG_DATE " +
                    ") values ( " +
                    artistId +", " +
                    ValNullString(artist.getArtistName()) +", " +
                    ValNullString(artist.getArtistNameOrigin()) +", " +
                    ValNullString(artist.getNickname()) +", " +
                    ValNullString(artist.getHint()) +", " +
                    ValNullString(artist.getGroupYn()) +", " +
                    ValNullString(artist.getArtistRoleTypeCd()) +", " +
                    ValNullString(artist.getDomesticYn()) +", " +
                    ValNullString(artist.getNationalityCd()) +", " +
                    ValNullString(artist.getGenreId()) +", " +
                    ValNullString(artist.getBirthday()) +", " +
                    ValNullString(artist.getDeathday()) +", " +
                    ValNullString(artist.getDebutDay()) +", " +
                    ValNullString(artist.getDebutSong()) +", " +
                    ValNullString(artist.getPerformYears()) +", " +
                    ValNullString(artist.getGender()) +", " +
                    ValNullString(artist.getBloodType()) +", " +
                    ValNullString(artist.getReligion()) +", " +
                    ValNullString(artist.getHobby()) +", " +
                    artist.getArtistPriority() +", " +
                    ValNullString(artist.getHomepage()) +", " +
                    ValNullString(artist.getTwitterUrl()) +", " +
                    ValNullString(artist.getFacebookUrl()) +", " +
                    ValNullString(artist.getSearchYn()) +", " +
                    ValNullString(artist.getSearchKeyword()) +", " +
                    ValNullString(artist.getProfile()) +", " +
                    ValNullString(artist.getArtistLImgPath()) +", " +
                    ValNullString(artist.getArtistMImgPath()) +", " +
                    ValNullString(artist.getArtistSImgPath()) +", " +
                    "'A', " +
                    "'cmsv2', " +
                    "SYSDATE " +
                    " )";

            String sql1 = "insert into if_artist_label( " +
                    "artist_id,label_cd,label_name,reg_date " +
                    ")values( " +
                    artistId+","+ValNullString(artist.getLabelCd()) +","+ValNullString(labelName) +",SYSDATE " +
                    ") ";

            System.out.println(sql);
            stmt.executeQuery(sql);
            //stmt.execute(sql1);
            stmt.close();
        }catch(Exception ex){
            log.info(ex.getMessage());
            //System.out.println(ex.getMessage());
        }
    }

    public Integer InsertAlbum(AlbumAdd albumAdd,Integer albumId,Integer MainArtistId,String labelName){
        Integer xx = 0;
        stmt = null;
        rs = null;
//        Label label = labelRepository.listLabelbyCode(albumAdd.getLabelCd());
        try{
            stmt = con.createStatement();

            String sql = "insert into album ( " +
                    "ALBUM_ID, " +
                    "ALBUM_NAME, " +
                    "ALBUM_NAME_ORIGIN, " +
                    "HINT, " +
                    "ALBUM_TYPE_CD, " +
                    "SERIES_NO, " +
                    "DISK_CNT, " +
                    "ISSUE_DATE, " +
                    "ORDER_ISSUE_DATE, " +
                    "ADULT_YN, " +
                    "DOMESTIC_YN, " +
                    "GENRE_ID, " +
                    "SELL_COMPANY, " +
                    "MAIN_SONG_ID, " +
                    "MAIN_ARTIST_ID, " +
                    "SEARCH_KEYWORD, " +
                    "ALBUM_L_IMG_PATH, " +
                    "ALBUM_M_IMG_PATH, " +
                    "ALBUM_S_IMG_PATH, " +
                    "ALBUM_REVIEW, " +
                    "STATUS, " +
                    "WORKER_ID, " +
                    "TOTAL_SONG_CNT, " +
                    "REG_DATE " +
                    ") values ( " +
                    albumId+", " +
                    ValNullString(albumAdd.getAlbumName())+", " +
                    ValNullString(albumAdd.getAlbumNameOrigin())+", " +
                    ValNullString(albumAdd.getHint())+", " +
                    ValNullString(albumAdd.getAlbumTypeCd())+", " +
                    albumAdd.getSeriesNo()+", " +
                    albumAdd.getDiskCnt()+", " +
                    ValNullString(albumAdd.getIssueDate())+", " +
                    ValNullString((albumAdd.getIssueDate()+"00"))+", " +
                    ValNullString(albumAdd.getAdultYn())+", " +
                    ValNullString(albumAdd.getDomesticYn())+", " +
                    ValNullString(albumAdd.getGenreId())+", " +
                    ValNullString(albumAdd.getSellCompany())+", " +
                    "'', " +
                    MainArtistId+", " +
                    ValNullString(albumAdd.getSearchKeyword())+", " +
                    ValNullString(albumAdd.getAlbumLImgPath())+", " +
                    ValNullString(albumAdd.getAlbumMImgPath())+", " +
                    ValNullString(albumAdd.getAlbumSImgPath())+", " +
                    ValNullString(albumAdd.getAlbumReview())+", " +
                    "'A', " +
                    "'cmsv2', " +
                    "0 ,"+
                    "SYSDATE " +
                    ")";

            String sql1 = "insert into if_album_label( " +
                    "album_id,label_cd,label_name,reg_date " +
                    ")values( " +
                    albumId+","+ValNullString(albumAdd.getLabelCd()) +","+ValNullString(labelName) +",SYSDATE " +
                    ") ";

            System.out.println(sql);
            stmt.executeQuery(sql);
//            stmt.executeQuery(sql1);
            stmt.close();
            xx = 1;
        }catch(Exception ex){
            log.info(ex.getMessage());
            //System.out.println(ex.getMessage());
            xx = 0;
        }
        return xx;
    }

    public void EditAlbum(AlbumAdd albumAdd){
        stmt = null;
        rs = null;
//        Label label = labelRepository.listLabelbyCode(albumAdd.getLabelCd());
        try{
            stmt = con.createStatement();
            String sql = "UPDATE ALBUM SET " +
                    "ALBUM_NAME =  "+ValNullString(albumAdd.getAlbumName())+"," +
                    "ALBUM_NAME_ORIGIN =  "+ValNullString(albumAdd.getAlbumNameOrigin())+"," +
                    "HINT =  "+ValNullString(albumAdd.getHint())+"," +
                    "ALBUM_TYPE_CD =  "+ValNullString(albumAdd.getAlbumTypeCd())+"," +
                    "SERIES_NO = "+albumAdd.getSeriesNo()+"," +
                    "DISK_CNT =  "+albumAdd.getDiskCnt()+"," +
                    "TOTAL_SONG_CNT =  0," +
                    "ISSUE_DATE =  "+ValNullString(albumAdd.getIssueDate())+"," +
                    //"ORDER_ISSUE_DATE =  "+ValNullString(albumAdd.getOrderIssueDate())+"," +
                    "ADULT_YN =  "+ValNullString(albumAdd.getAdultYn())+"," +
                    "DOMESTIC_YN = "+ValNullString(albumAdd.getDomesticYn())+" ," +
                    "ISSUE_COUNTRY_CD = "+ValNullString(albumAdd.getIssueCountryCd())+" ," +
                    "GENRE_ID = "+ValNullString(albumAdd.getGenreId())+" ," +
                    "AGENCY = "+ValNullString(albumAdd.getAgency())+" ," +
                    "SELL_COMPANY = "+ValNullString(albumAdd.getSellCompany())+" ," +
                    //"MAIN_SONG_ID = "+ValNullString(albumAdd.getMainSongId())+" ," +
                    "MAIN_ARTIST_ID = "+albumAdd.getMainArtistId()+" ," +
                    "SEARCH_KEYWORD =  "+ValNullString(albumAdd.getSearchKeyword())+"," +
                    "ALBUM_L_IMG_PATH = "+ValNullString(albumAdd.getAlbumLImgPath())+" ," +
                    "ALBUM_M_IMG_PATH = "+ValNullString(albumAdd.getAlbumMImgPath())+" ," +
                    "ALBUM_S_IMG_PATH = "+ValNullString(albumAdd.getAlbumSImgPath())+" ," +
                    "ALBUM_REVIEW = "+ValNullString(albumAdd.getAlbumReview())+" ," +
                    //"RECOMMEND_CNT = "+albumAdd.getRecommendCnt()+" ," +
                    //"OPPOSE_CNT = "+albumAdd.getOpposeCnt()+" ," +
                    "STATUS = "+ValNullString(albumAdd.getStatus())+" ," +
                    "UPD_DATE = SYSDATE " +
                    "WHERE ALBUM_ID = "+albumAdd.getProdAlbumId() ;

            System.out.println(sql);
            stmt.executeQuery(sql);
            stmt.close();
        }catch(Exception ex){
            log.info(ex.getMessage());
            //System.out.println(ex.getMessage());
        }
    }

    public void InsertArtistAlbum(Integer artistId,
                                   Integer albumId,
                                   String artistName,
                                   String AlbumName,String albumIssueDate){
        stmt = null;
        rs = null;

        try{
            stmt = con.createStatement();
            String sql = "insert into ARTIST_ALBUM ( " +
                    "ARTIST_ID, " +
                    "ALBUM_ID, " +
                    "ARTIST_NAME, " +
                    "ALBUM_NAME, " +
                    "ALBUM_ISSUE_DATE, " +
                    "REG_DATE) " +
                    "VALUES ( " +
                    artistId+", " +
                    albumId+", " +
                    ValNullString(artistName)+", " +
                    ValNullString(AlbumName)+", " +
                    ValNullString(albumIssueDate)+", " +
                    "SYSDATE " +
                    ")";
            System.out.println(sql);
            stmt.executeQuery(sql);
            stmt.close();
        }catch(Exception ex){
            log.info(ex.getMessage());
        }
    }

    public void DeleteArtistAlbum(Integer albumId){
        stmt = null;
        rs = null;

        try{
            stmt = con.createStatement();
            String sql = "DELETE FROM  ARTIST_ALBUM  WHERE ALBUM_ID = "+albumId;
            System.out.println(sql);
            stmt.executeQuery(sql);
            stmt.close();
        }catch(Exception ex){
            log.info(ex.getMessage());
        }
    }

    public void InsertSong(Integer songId, SongAdd songAdd, Integer albumOraId, Integer artistOraId,String labelName, String searckKeyword){
        stmt = null;
        rs = null;

        try{
            stmt = con.createStatement();

            String sql2 = "update album " +
                    "set total_song_cnt = total_song_cnt + 1 " +
                    "where album_id = " + albumOraId;

            String sql = "INSERT INTO SONG ( " +
                    "SONG_ID, " +
                    "SONG_NAME, " +
                    "SONG_NAME_ORIGIN, " +
                    "MLB_CD, " +
                    "GENID_CD, " +
                    "CP1_CD, " +
                    "CP2_CD, " +
                    "CP3_CD, " +
                    "CP4_CD, " +
                    "CP5_CD, " +
                    "HINT, " +
                    "DISK_NO, " +
                    "TRACK_NO, " +
                    "TITLE_SONG_YN, " +
                    "HIT_SONG_YN, " +
                    "MOD_YN, " +
                    "VOD_YN, " +
                    "RT_YN, " +
                    "RBT_YN, " +
                    "TEXT_LYRIC_YN, " +
                    "SLF_LYRIC_YN, " +
                    "GENRE_ID, " +
                    "ALBUM_ID, " +
                    "ARTIST_ID, " +
                    "ISSUE_DATE, " +
                    "ORDER_ISSUE_DATE, " +
                    "SELL_STREAM_YN, " +
                    "SELL_DRM_YN, " +
                    "SELL_NON_DRM_YN, " +
                    "SELL_ALACARTE_YN, " +
                    "DRM_PAYMENT_PROD_ID, " +
                    "DRM_PRICE, " +
                    "NON_DRM_PAYMENT_PROD_ID, " +
                    "NON_DRM_PRICE, " +
                    "SEARCH_KEYWORD, " +
                    "ADULT_YN, " +
                    "LC_STATUS_CD, " +
                    "WORKER_ID, " +
                    "REG_DATE," +
                    "PLAY_TIME " +
                    ")VALUES( " +
                    songId+", " +
                    ValNullString(songAdd.getSongName())+", " +
                    ValNullString(songAdd.getSongNameOrigin())+", " +
                    ValNullString(songAdd.getMlbCd())+", " +
                    ValNullString(songAdd.getGenidCd())+", " +
                    ValNullString(songAdd.getCp1Cd())+", " +
                    ValNullString(songAdd.getCp2Cd())+", " +
                    ValNullString(songAdd.getCp3Cd())+", " +
                    ValNullString(songAdd.getCp3Cd())+", " +
                    ValNullString(songAdd.getCp5Cd())+", " +
                    ValNullString(songAdd.getHint())+", " +
                    songAdd.getDiskNo()+", " +
                    songAdd.getTrackNo()+", " +
                    "'N', " +
                    ValNullString(songAdd.getHitSongYn())+", " +
                    "'N', " +
                    "'N', " +
                    "'N', " +
                    "'N', " +
                    ValNullString(songAdd.getTextLyricYn())+", " +
                    "'N', " +
                    ValNullString(songAdd.getGenreId())+", " +
                    albumOraId+", " +
                    artistOraId+", " +
                    ValNullString(songAdd.getIssueDate())+", " +
                    ValNullString(songAdd.getIssueDate()+"00")+", " +
                    "'Y', " +
                    "'Y', " +
                    ValNullString(songAdd.getSellNonDrmYn())+", " +
                    ValNullString(songAdd.getSellNonDrmYn())+", " +
                    ValNullString(songAdd.getDrmPaymentProdId())+", " +
                    songAdd.getDrmPrice()+", " +
                    songAdd.getNonDrmPaymentProdId()+", " +
                    songAdd.getNonDrmPrice()+", " +
                    ValNullString(songAdd.getSearchKeyword())+", " +
                    ValNullString(songAdd.getAdultYn())+", " +
                    "'CS0006', " +
                    "'cmsv2', " +
                    "Sysdate, " +
                    songAdd.getPlayTime()+
                    ")";

            String sql1 = "insert into if_song_label( " +
                    "song_id,mlb_cd,label_cd,label_name,reg_date " +
                    ")values( " +
                    songId+","+ValNullString(songAdd.getMlbCd())+","+ValNullString(songAdd.getLabelCd()) +","+ValNullString(labelName) +",SYSDATE " +
                    ") ";

            System.out.println(sql);
            stmt.executeQuery(sql);
//            stmt.executeQuery(sql1);
            stmt.executeQuery(sql2);
            stmt.close();
        }catch(Exception ex){
            log.info(ex.getMessage());
        }
    }

    public void EditSong(Integer songId, SongAdd songAdd, Integer albumOraId, Integer artistOraId){
        stmt = null;
        rs = null;

        try {
            stmt = con.createStatement();
            String sql = "UPDATE SONG SET " +
                    "SONG_NAME = " + ValNullString(songAdd.getSongName()) + "," +
                    "SONG_NAME_ORIGIN = " + ValNullString(songAdd.getSongNameOrigin()) + "," +
                    "MLB_CD = " + ValNullString(songAdd.getMlbCd()) + "," +
                    "GENID_CD = " + ValNullString(songAdd.getGenidCd()) + "," +
                    "CP1_CD = " + ValNullString(songAdd.getCp1Cd()) + "," +
                    "CP2_CD = " + ValNullString(songAdd.getCp2Cd()) + "," +
                    "CP3_CD = " + ValNullString(songAdd.getCp3Cd()) + "," +
                    "CP4_CD = " + ValNullString(songAdd.getCp3Cd()) + "," +
                    "CP5_CD = " + ValNullString(songAdd.getCp5Cd()) + "," +
                    "HINT = " + ValNullString(songAdd.getHint()) + "," +
                    "DISK_NO = " + songAdd.getDiskNo() + "," +
                    "TRACK_NO = " + songAdd.getTrackNo() + "," +
                    "HIT_SONG_YN = " + ValNullString(songAdd.getHitSongYn()) + "," +
                    "TEXT_LYRIC_YN = " + ValNullString(songAdd.getTextLyricYn()) + "," +
                    "GENRE_ID = " + ValNullString(songAdd.getGenreId()) + "," +
                    "ALBUM_ID = " + albumOraId + "," +
                    "ARTIST_ID = " + artistOraId + "," +
                    "ISSUE_DATE = " + ValNullString(songAdd.getIssueDate()) + "," +
                    "DRM_PAYMENT_PROD_ID = " + ValNullString(songAdd.getDrmPaymentProdId()) + "," +
                    "DRM_PRICE = " + songAdd.getDrmPrice() + "," +
                    "NON_DRM_PAYMENT_PROD_ID = " + songAdd.getNonDrmPaymentProdId() + "," +
                    "NON_DRM_PRICE = " + songAdd.getNonDrmPrice() + "," +
                    "SEARCH_KEYWORD = " + ValNullString(songAdd.getSearchKeyword()) + "," +
                    "ADULT_YN = " + ValNullString(songAdd.getAdultYn()) + "," +
                    "UPD_DATE = Sysdate " +
                    " WHERE SONG_ID = '"+songId+"'";

            System.out.println(sql);
            stmt.executeQuery(sql);
            stmt.close();
        }catch(Exception ex){
            log.info(ex.getMessage());
        }
    }

    public void InsertSongMod(Integer songId, Integer songOraId, List<SongModAdd> songMods){
        stmt = null;
        rs = null;

        try{
            stmt = con.createStatement();
            for(SongModAdd songMod : songMods) {
                Integer songModId = this.getSeqSongMod();
                String sql = "insert into SONG_MOD ( " +
                        "SONG_MOD_ID, " +
                        "SONG_ID, " +
                        "SAMPLING, " +
                        "CODEC_TYPE_CD, " +
                        "BIT_RATE_CD, " +
                        "PLAY_TIME, " +
                        "FILE_SIZE, " +
                        "ORIGINAL_FILE_NAME, " +
                        "FULL_FILE_PATH, " +
                        "STATUS, " +
                        "WORKER_ID, " +
                        "REG_DATE " +
                        ")VALUES( " +
                        songModId+", " +
                        songOraId+", " +
                        songMod.getSampling()+", " +
                        ValNullString(songMod.getCodecTypeCd())+", " +
                        ValNullString(songMod.getBitRateCd())+", " +
                        songMod.getPlayTime()+", " +
                        songMod.getFileSize()+", " +
                        ValNullString(songMod.getOriginalFileName())+", " +
                        ValNullString(songMod.getFullFilePath())+", " +
                        ValNullString(songMod.getStatus())+", " +
                        "'cmsv2', " +
                        "SYSDATE " +
                        ") ";
                stmt.executeQuery(sql);
            }

            stmt.close();
        }catch(Exception ex){
            log.info(ex.getMessage());
        }
    }

    public void UpdateSongMod(Integer songId, Integer songOraId, List<SongModAdd> songMods){
        stmt = null;
        rs = null;

        try{
            stmt = con.createStatement();
            for(SongModAdd songMod : songMods) {
                Integer songModId = this.getSeqSongMod();
                String sql = "update song_mod set " +
                        "song_id = '1' " +
                        "where song_id = '"+songOraId+"' " +
                        "and codec_type_cd = '"+songMod.getCodecTypeCd()+"' " +
                        "and bit_rate_cd = '"+songMod.getBitRateCd()+"'  ";
                stmt.executeQuery(sql);
            }

            stmt.close();
        }catch(Exception ex){
            log.info(ex.getMessage());
        }
    }

    public void setSongAlbum(Integer albumOraId, Integer songOraId){
        stmt = null;
        rs = null;

        try{
            stmt = con.createStatement();
            String sql = "update album set main_song_id =  " +songOraId+
                    " where album_id = "+albumOraId;
            System.out.println(sql);
            stmt.executeQuery(sql);
            stmt.close();
        }catch(Exception ex){
            log.info(ex.getMessage());
        }
    }

    public Integer ValidationArtist(String artistName,String nationalityCd) throws SQLException{
        stmt = null;
        ResultSet rs = null;
        stmt = con.createStatement();
        Integer rslt = null;
        String sql = "select check_artist_name("+ValNullString(artistName)+","+ValNullString(nationalityCd)+") as xx from dual ";
        rs =stmt.executeQuery(sql);
        if(rs.next()){
            rslt = rs.getInt(1);
        }
        return rslt;
    }

    public Integer ValidationAlbum(String albumName,String artistId) throws SQLException{
        stmt = null;
        ResultSet rs = null;
        stmt = con.createStatement();
        Integer rslt = null;
        String sql = "select CHECK_ALBUM_NAME("+ValNullString(albumName)+","+ValNullString(artistId)+") as xx from dual ";
        rs =stmt.executeQuery(sql);
        if(rs.next()){
            rslt = rs.getInt(1);
        }
        return rslt;
    }

    private Integer ValidationSong(){
        return 0;
    }

    private String ValNullString (String el){
        String rslt;

        if(el == null){
            rslt = "null";
        }else{
            rslt = "'"+el.replace("'","''")+"'";
        }
        return rslt;
    }

    public void addArtistLabel(

    ){

    }

}
