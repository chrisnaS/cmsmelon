package com.cmsmelon.helper;

import com.cmsmelon.Model.TmpMlbCode;
import jxl.Workbook;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.write.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

 import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class GenerateTxt {
    private static final Logger log = LoggerFactory.getLogger(GenerateTxt.class);
//    private static final String EXCEL_FILE_LOCATION = "D:/";
    private static final String EXCEL_FILE_LOCATION = "/mediaNAS15/exel/";

    private static final String COMMA_DELIMITER = ";";
    private static final String NEW_LINE_SEPARATOR = "\n";
    private static final String FILE_HEADER = "MLB Code;Label Code;Label Name";


    public String Generate(List<TmpMlbCode> tmpMlbCodes){
        DateFormat df = new SimpleDateFormat("yyyyMMddhhmmss");
        Date today = Calendar.getInstance().getTime();
        String filename = df.format(today) + ".csv";
        String resp = EXCEL_FILE_LOCATION + filename;
        FileWriter fileWriter = null;

        try{
            fileWriter = new FileWriter(resp);
            fileWriter.append(FILE_HEADER.toString());
            fileWriter.append(NEW_LINE_SEPARATOR);

            for(TmpMlbCode tmpMlbCode : tmpMlbCodes){
                fileWriter.append(String.valueOf(tmpMlbCode.getMlbCode()));
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(String.valueOf(tmpMlbCode.getLabelCode()));
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(String.valueOf(tmpMlbCode.getLabelName()));
                fileWriter.append(NEW_LINE_SEPARATOR);
            }
            System.out.println("CSV file was created successfully !!!");
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }finally {
            try{
                fileWriter.flush();
                fileWriter.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return resp;

    }
}
