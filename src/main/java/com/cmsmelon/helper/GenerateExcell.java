package com.cmsmelon.helper;

import com.cmsmelon.Model.TmpMlbCode;
import jxl.Workbook;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.write.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class GenerateExcell {
    private static final String EXCEL_FILE_LOCATION = "/mediaNAS15/exel/";
    private static final Logger log = LoggerFactory.getLogger(GenerateExcell.class);

//    private static final String EXCEL_FILE_LOCATION = "F:/";
    public String Generate(List<TmpMlbCode> tmpMlbCodes){
        DateFormat df = new SimpleDateFormat("yyyyMMddhhmmss");
        Date today = Calendar.getInstance().getTime();
        String filename = df.format(today) + ".xls";
        String resp = EXCEL_FILE_LOCATION + filename;
        WritableWorkbook myFirstWbook = null;
        Border border = Border.ALL;
        BorderLineStyle borderLineStyle = BorderLineStyle.THIN;
        try {
            myFirstWbook = Workbook.createWorkbook(new File(resp));

            // create an Excel sheet
            WritableSheet excelSheet = myFirstWbook.createSheet("Sheet 1", 0);
            WritableCellFormat cellFormat = new WritableCellFormat();

            cellFormat.setBorder(border, borderLineStyle);
            // add something into the Excel sheet
            Label label;
            label = new Label(0, 0, "LABEL CODE",cellFormat);
            excelSheet.addCell(label);
            label = new Label(1, 0, "LABEL NAME",cellFormat);
            excelSheet.addCell(label);
            label = new Label(2, 0, "MLB CODE",cellFormat);
            excelSheet.addCell(label);
            Integer i = 1;
            for(TmpMlbCode tmpMlbCode : tmpMlbCodes){
                label = new Label(0, i, tmpMlbCode.getLabelCode(),cellFormat);
                excelSheet.addCell(label);
                label = new Label(1, i, tmpMlbCode.getLabelName(),cellFormat);
                excelSheet.addCell(label);
                label = new Label(2, i, tmpMlbCode.getMlbCode(),cellFormat);
                excelSheet.addCell(label);
                i++;
                log.info("data ke -" + String.valueOf(i));
            }
            myFirstWbook.write();
            return resp;
        }catch (IOException e){
            e.printStackTrace();
        }catch (WriteException e) {
            e.printStackTrace();
        } finally {
            if (myFirstWbook != null) {
                try {
                    myFirstWbook.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (WriteException e) {
                    e.printStackTrace();
                }
            }

        }
        return null;
    }

}
