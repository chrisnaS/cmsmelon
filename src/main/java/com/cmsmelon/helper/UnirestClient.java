package com.cmsmelon.helper;

import com.cmsmelon.ModelScheduler.JobSchedule;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class UnirestClient {
    private static final Logger log = LoggerFactory.getLogger(UnirestClient.class);
    public String GetEncoder(String song_path, String bit_rate,
                                           String song_id, String target) throws UnirestException {
        Unirest.setTimeouts(600000, 600000);
        String filename = null;
        try {
            HttpResponse<String> response = Unirest.get("http://192.168.10.235:8888/encoder/encode_wav?song_path="+song_path+"&bit_rate="+bit_rate+"&song_id="+song_id+"&target="+target)
                    .header("cache-control", "no-cache")
                    .asString();
//            System.out.println("http://localhost:8800/encoder/encode_wav?song_path="+song_path+"&bit_rate="+bit_rate+"&song_id="+song_id+"&target="+target+"&from=live");
//            HttpResponse<String> response = Unirest.get("http://localhost:8800/encoder/encode_wav?song_path="+song_path+"&bit_rate="+bit_rate+"&song_id="+song_id+"&target="+target+"&from=live")
//                    .header("cache-control", "no-cache")
//                    .asString();
            filename = response.getBody().toString();
        }catch (Exception ex){
            System.out.println();
        }


        return filename;
    }
}
