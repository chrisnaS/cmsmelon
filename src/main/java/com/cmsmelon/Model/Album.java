package com.cmsmelon.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by IT19 on 18/08/2017.
 */
@Entity
@NamedNativeQueries({
        @NamedNativeQuery(name = "Album.listAlbumByName",
                query = "select a.*, \n" +
                        "         b.ARTIST_NAME,c.SONG_NAME,   \n" +
                        "         str_to_date(a.ISSUE_DATE,'%Y%m%d') as ISSUE_DATE_STR, d.LABEL_NAME, e.COMMON_CD_NAME   \n" +
                        "         from ALBUM_MELON a  \n" +
                        "         left outer join ARTIST b  \n" +
                        "         on a.MAIN_ARTIST_ID = b.ARTIST_ID \n" +
                        "         left outer join SONG c  \n" +
                        "         on a.MAIN_SONG_ID = c.SONG_ID \n" +
                        "         left outer join LABEL d  \n" +
                        "         on a.LABEL_CD = d.LABEL_CD  \n" +
                        "         left outer join  MD_COMMON_CD e on a.LC_STATUS_CD = e.COMMON_CD "+
                        "where a.ALBUM_NAME like concat(:search ,'%')  " +
                        " and a.LC_STATUS_CD like concat('%', :lcStatusCd ,'%') \n" +
                        " and a.LABEL_CD like concat('%', :labelCd ,'%') \n" +
//                        "ORDER BY a.REG_DATE DESC " +
                        "limit :offset,10  ",
                resultClass = Album.class
        ),
        @NamedNativeQuery(name = "Album.listAlbumById",
                query = "select a.*, \n" +
                        "         b.ARTIST_NAME,c.SONG_NAME,   \n" +
                        "         str_to_date(a.ISSUE_DATE,'%Y%m%d') as ISSUE_DATE_STR, d.LABEL_NAME, e.COMMON_CD_NAME   \n" +
                        "         from ALBUM_MELON a  \n" +
                        "         left outer join ARTIST b  \n" +
                        "         on a.MAIN_ARTIST_ID = b.ARTIST_ID \n" +
                        "         left outer join SONG c  \n" +
                        "         on a.MAIN_SONG_ID = c.SONG_ID \n" +
                        "         left outer join LABEL d  \n" +
                        "         on a.LABEL_CD = d.LABEL_CD  \n" +
                        "         left outer join  MD_COMMON_CD e on a.LC_STATUS_CD = e.COMMON_CD " +
                        "where a.ALBUM_ID like concat(:search ,'%')  " +
                        " and a.LC_STATUS_CD like concat('%', :lcStatusCd ,'%') \n" +
                        " and a.LABEL_CD like concat('%', :labelCd ,'%') \n" +
//                        "ORDER BY a.REG_DATE DESC " +
                        "limit :offset,10  ",
                resultClass = Album.class
        )
})
public class Album {
    @Id
    @Column(name = "ALBUM_ID")
    private Integer albumId;

    @Column(name = "REG_DATE")
    private String regDate;

    @Column(name="MAIN_SONG_ID")
    private String mainSongId;

    @Column(name="MAIN_ARTIST_ID")
    private float mainArtistId;

    @Column(name="ALBUM_NAME")
    private String albumName;

    @Column(name="ALBUM_NAME_ORIGIN")
    private String albumNameOrigin;

    @Column(name="HINT")
    private String hint;

    @Column(name="ALBUM_TYPE_CD")
    private String albumTypeCd;

    @Column(name="SERIES_NO")
    private String seriesNo;

    @Column(name="DISK_CNT")
    private String diskCnt;

    @Column(name="TOTAL_SONG_CNT")
    private String totalSongCnt;

    @Column(name="ISSUE_DATE")
    private String issueDate;

    @Column(name="ORDER_ISSUE_DATE")
    private String orderIssueDate;

    @Column(name="ADULT_YN")
    private String adultYn;

    @Column(name="DOMESTIC_YN")
    private String domesticYn;

    @Column(name="ISSUE_COUNTRY_CD")
    private String issueCountryCd;

    @Column(name="GENRE_ID")
    private String genreId;

    @Column(name="AGENCY")
    private String agency;

    @Column(name="SELL_COMPANY")
    private String sellCompany;

    @Column(name="SEARCH_KEYWORD")
    private String searchKeyword;

    @Column(name="ALBUM_L_IMG_PATH")
    private String albumLImgPath;

    @Column(name="ALBUM_M_IMG_PATH")
    private String albumMImgPath;

    @Column(name="ALBUM_S_IMG_PATH")
    private String albumSImgPath;

    @Column(name="ALBUM_REVIEW")
    private String albumReview;

    @Column(name="RECOMMEND_CNT")
    private float RecommendCnt;

    @Column(name="OPPOSE_CNT")
    private float opposeCnt;

    @Column(name="STATUS")
    private String status;

    @Column(name="LABEL_CD")
    private String labelCd;

    @Column(name="CHANNEL_GROUP_CD")
    private String channelGroupCd;

    @Column(name="WORKER_ID")
    private String workerId;

    @Column(name="UPD_DATE")
    private Date updDate;

    @Column(name="ISSUE_DATE_STR")
    private String issueDateStr;

    @Column(name="ARTIST_NAME")
    private String artistName;

    @Column(name="SONG_NAME")
    private String songName;

    @Column(name="LABEL_NAME")
    private String labelName;

    @Column(name="COMMON_CD_NAME")
    private String statusName;

    @Column(name="LC_STATUS_CD")
    private String lcStatusCd;


    public String getLcStatusCd() {
        return lcStatusCd;
    }

    public void setLcStatusCd(String lcStatusCd) {
        this.lcStatusCd = lcStatusCd;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getIssueDateStr() {
        return issueDateStr;
    }

    public void setIssueDateStr(String issueDateStr) {
        this.issueDateStr = issueDateStr;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public Integer getAlbumId() {
        return albumId;
    }

    public void setAlbumId(Integer albumId) {
        this.albumId = albumId;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getMainSongId() {
        return mainSongId;
    }

    public void setMainSongId(String mainSongId) {
        this.mainSongId = mainSongId;
    }

    public float getMainArtistId() {
        return mainArtistId;
    }

    public void setMainArtistId(float mainArtistId) {
        this.mainArtistId = mainArtistId;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public String getAlbumNameOrigin() {
        return albumNameOrigin;
    }

    public void setAlbumNameOrigin(String albumNameOrigin) {
        this.albumNameOrigin = albumNameOrigin;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getAlbumTypeCd() {
        return albumTypeCd;
    }

    public void setAlbumTypeCd(String albumTypeCd) {
        this.albumTypeCd = albumTypeCd;
    }

    public String getSeriesNo() {
        return seriesNo;
    }

    public void setSeriesNo(String seriesNo) {
        this.seriesNo = seriesNo;
    }

    public String getDiskCnt() {
        return diskCnt;
    }

    public void setDiskCnt(String diskCnt) {
        this.diskCnt = diskCnt;
    }

    public String getTotalSongCnt() {
        return totalSongCnt;
    }

    public void setTotalSongCnt(String totalSongCnt) {
        this.totalSongCnt = totalSongCnt;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getOrderIssueDate() {
        return orderIssueDate;
    }

    public void setOrderIssueDate(String orderIssueDate) {
        this.orderIssueDate = orderIssueDate;
    }

    public String getAdultYn() {
        return adultYn;
    }

    public void setAdultYn(String adultYn) {
        this.adultYn = adultYn;
    }

    public String getDomesticYn() {
        return domesticYn;
    }

    public void setDomesticYn(String domesticYn) {
        this.domesticYn = domesticYn;
    }

    public String getIssueCountryCd() {
        return issueCountryCd;
    }

    public void setIssueCountryCd(String issueCountryCd) {
        this.issueCountryCd = issueCountryCd;
    }

    public String getGenreId() {
        return genreId;
    }

    public void setGenreId(String genreId) {
        this.genreId = genreId;
    }

    public String getAgency() {
        return agency;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public String getSellCompany() {
        return sellCompany;
    }

    public void setSellCompany(String sellCompany) {
        this.sellCompany = sellCompany;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getAlbumLImgPath() {
        return albumLImgPath;
    }

    public void setAlbumLImgPath(String albumLImgPath) {
        this.albumLImgPath = albumLImgPath;
    }

    public String getAlbumMImgPath() {
        return albumMImgPath;
    }

    public void setAlbumMImgPath(String albumMImgPath) {
        this.albumMImgPath = albumMImgPath;
    }

    public String getAlbumSImgPath() {
        return albumSImgPath;
    }

    public void setAlbumSImgPath(String albumSImgPath) {
        this.albumSImgPath = albumSImgPath;
    }

    public String getAlbumReview() {
        return albumReview;
    }

    public void setAlbumReview(String albumReview) {
        this.albumReview = albumReview;
    }

    public float getRecommendCnt() {
        return RecommendCnt;
    }

    public void setRecommendCnt(float recommendCnt) {
        RecommendCnt = recommendCnt;
    }

    public float getOpposeCnt() {
        return opposeCnt;
    }

    public void setOpposeCnt(float opposeCnt) {
        this.opposeCnt = opposeCnt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLabelCd() {
        return labelCd;
    }

    public void setLabelCd(String labelCd) {
        this.labelCd = labelCd;
    }

    public String getChannelGroupCd() {
        return channelGroupCd;
    }

    public void setChannelGroupCd(String channelGroupCd) {
        this.channelGroupCd = channelGroupCd;
    }

    public String getWorkerId() {
        return workerId;
    }

    public void setWorkerId(String workerId) {
        this.workerId = workerId;
    }

    public Date getUpdDate() {
        return updDate;
    }

    public void setUpdDate(Date updDate) {
        this.updDate = updDate;
    }
}
