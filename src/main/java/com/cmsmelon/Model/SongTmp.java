package com.cmsmelon.Model;

import javax.persistence.*;

@Entity
@NamedNativeQueries({
        @NamedNativeQuery(name = "SongTmp.listSongModQue",
                query = "select SONG_ID from TMP_SONG_ENCODER order by REG_DATE asc limit 1 ",
                resultClass = SongTmp.class
        )
})
public class SongTmp {
    @Id
    @Column(name="SONG_ID")
    private String songId;

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }
}
