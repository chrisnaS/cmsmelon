package com.cmsmelon.Model;

import javax.persistence.*;

@Entity
@NamedNativeQueries({
        @NamedNativeQuery(name = "TmpMlbCode.getData",
                query = "select LABEL_CODE, LABEL_NAME, MLB_CODE from TMP_MLB_CODE where TX_ID = :txId order by LABEL_CODE,MLB_CODE ",
                resultClass = TmpMlbCode.class
        )
})
public class TmpMlbCode {
    @Id
    @Column(name = "MLB_CODE")
    private String mlbCode;

    @Column(name = "LABEL_CODE")
    private String labelCode;

    @Column(name = "LABEL_NAME")
    private String labelName;

    public String getMlbCode() {
        return mlbCode;
    }

    public void setMlbCode(String mlbCode) {
        this.mlbCode = mlbCode;
    }

    public String getLabelCode() {
        return labelCode;
    }

    public void setLabelCode(String labelCode) {
        this.labelCode = labelCode;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }
}
