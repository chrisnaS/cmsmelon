package com.cmsmelon.Model;

import javax.persistence.*;

@Entity
@Table(name="`MLB_LABEL`")
@NamedNativeQueries({
        @NamedNativeQuery(name = "MlbLabel.listMlbLabel",
                query = "select LABEL_CODE, UPPER(LABEL_NAME) as LABEL_NAME, INITIAL from MLB_LABEL order by LABEL_NAME  ",
                resultClass = MlbLabel.class
        ),
        @NamedNativeQuery(name = "MlbLabel.listMlbLabelJoin",
                query = "select a.LABEL_CD as LABEL_CODE, a.LABEL_NAME, b.INITIAL from LABEL a \n" +
                        "left outer join MLB_LABEL b \n" +
                        "on a.LABEL_CD = b.LABEL_CODE where a.LABEL_NAME like concat('%', :search ,'%') " +
                        "order by REG_DATE desc " +
                        "limit :offset,10 ",
                resultClass = MlbLabel.class
        )
})
public class MlbLabel {
    @Id
    @Column(name = "LABEL_CODE")
    private String labelCode;

    @Column(name = "LABEL_NAME")
    private String labelName;

    @Column(name="INITIAL")
    private String initial;

    public String getLabelCode() {
        return labelCode;
    }

    public void setLabelCode(String labelCode) {
        this.labelCode = labelCode;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getInitial() {
        return initial;
    }

    public void setInitial(String initial) {
        this.initial = initial;
    }
}
