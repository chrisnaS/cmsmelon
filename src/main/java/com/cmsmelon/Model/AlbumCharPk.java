package com.cmsmelon.Model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by IT19 on 22/08/2017.
 */
@Embeddable
public class AlbumCharPk implements Serializable {

    @Column(name = "ALBUM_ID")
    private Integer albumId;

    @Column(name = "REG_DATE")
    private String regDate;

    @Column(name="MAIN_SONG_ID")
    private String mainSongId;

    @Column(name="MAIN_ARTIST_ID")
    private float mainArtistId;

    public AlbumCharPk(){}
    public AlbumCharPk(String regDate, Integer albumId, String mainSongId, float mainArtistId) {
        super();
        this.regDate = regDate;
        this.albumId = albumId;
        this.mainSongId = mainSongId;
        this.mainArtistId = mainArtistId;
    }


    public Integer getAlbumId() {
        return albumId;
    }

    public void setAlbumId(Integer albumId) {
        this.albumId = albumId;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getMainSongId() {
        return mainSongId;
    }

    public void setMainSongId(String mainSongId) {
        this.mainSongId = mainSongId;
    }

    public float getMainArtistId() {
        return mainArtistId;
    }

    public void setMainArtistId(float mainArtistId) {
        this.mainArtistId = mainArtistId;
    }
}
