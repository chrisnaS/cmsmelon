package com.cmsmelon.Model;

import javax.persistence.*;

/**
 * Created by IT19 on 11/10/2017.
 */
@Entity
@NamedNativeQueries({
        @NamedNativeQuery(name = "AlbumArtist.albumArtistByAlbum",
                query = "select aa.ARTIST_ID, bb.ARTIST_NAME, bb.PROD_ARTIST_ID from ARTIST_ALBUM aa\n" +
                        "inner join ARTIST bb\n" +
                        "on aa.ARTIST_ID = bb.ARTIST_ID\n" +
                        "where ALBUM_ID = :albumId ",
                resultClass = AlbumArtist.class
        )
})
public class AlbumArtist {
    @Id
    @Column(name="ARTIST_ID")
    private Integer artistId;

    @Column(name="ARTIST_NAME")
    private String artistName;

    @Column(name="PROD_ARTIST_ID")
    private Integer prodArtistId;

    public Integer getProdArtistId() {
        return prodArtistId;
    }

    public void setProdArtistId(Integer prodArtistId) {
        this.prodArtistId = prodArtistId;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public Integer getArtistId() {
        return artistId;
    }

    public void setArtistId(Integer artistId) {
        this.artistId = artistId;
    }
}
