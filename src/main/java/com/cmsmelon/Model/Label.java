package com.cmsmelon.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by IT19 on 18/08/2017.
 */

@Entity
@Table(name="`LABEL`")
@NamedNativeQueries({
        @NamedNativeQuery(name = "Label.listLabel",
                query = "select * from LABEL where STATUS ='A' order by LABEL_NAME  ",
                resultClass = Label.class
        ),
        @NamedNativeQuery(name = "Label.listLabelbyCode",
                query = "select * from LABEL where STATUS ='A' and LABEL_CD = :labelCd ",
                resultClass = Label.class
        )
})
public class Label {
    @Id
    @Column(name="LABEL_CD")
    private String labelCd;

    @Column(name="LABEL_NAME")
    private String labelName;

    @Column(name="STATUS")
    private String status;

    @Column(name="WORKER_ID")
    private String workerId;

    @Column(name="REG_DATE")
    private Date regDate;

    @Column(name="UPD_DATE")
    private Date updDate;

    public String getLabelCd() {
        return labelCd;
    }

    public void setLabelCd(String labelCd) {
        this.labelCd = labelCd;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWorkerId() {
        return workerId;
    }

    public void setWorkerId(String workerId) {
        this.workerId = workerId;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public Date getUpdDate() {
        return updDate;
    }

    public void setUpdDate(Date updDate) {
        this.updDate = updDate;
    }
}
