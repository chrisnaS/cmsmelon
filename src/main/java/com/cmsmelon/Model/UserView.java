package com.cmsmelon.Model;

import javax.persistence.*;

@Entity
@NamedNativeQueries(
        @NamedNativeQuery(
                name="UserView.getUsers",
                query="select a.ID, a.USER_NAME,a.USER_PASSWORD,a.FULL_NAME,\n" +
                        "a.LABEL_CD,ROLE_TYPE, b.LABEL_NAME \n" +
                        "from CMS_USER a \n" +
                        "left join LABEL b \n" +
                        "on a.LABEL_CD = b.LABEL_CD\n" +
                        "where USER_NAME like concat('%',:search ,'%') \n" +
                        "order by a.USER_NAME limit :offset,10",
                resultClass = UserView.class
        )
)
public class UserView {
    @Id
    @Column(name = "ID")
    private String userId;

    @Column(name = "USER_NAME")
    private String username;

    @Column(name = "USER_PASSWORD")
    private String password;

    @Column(name = "FULL_NAME")
    private String fullname;

    @Column(name = "LABEL_CD")
    private String labelCd;

    @Column(name = "ROLE_TYPE")
    private String roleType;

    @Column(name = "LABEL_NAME")
    private String labelName;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getLabelCd() {
        return labelCd;
    }

    public void setLabelCd(String labelCd) {
        this.labelCd = labelCd;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }
}
