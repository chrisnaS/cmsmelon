package com.cmsmelon.Model;

import javax.persistence.*;

@Entity
@NamedNativeQueries({
        @NamedNativeQuery(name = "MlbCode.listMlb",
                query = "select * from V_MLB_CODE where LABEL_NAME like concat('%', :search ,'%') limit :offset,10  ",
                resultClass = MlbCode.class
        ),
        @NamedNativeQuery(name = "MlbCode.listGenerateMlb",
                query = "select TX_ID,'' as LABEL_NAME, 0 as JUMLAH, REG_DATE,  FILE_URL " +
                        "from TX_MLB where FILE_URL is null order by REG_DATE limit 1 ",
                resultClass = MlbCode.class
        )
})
public class MlbCode {
    @Id
    @Column(name = "TX_ID")
    private Integer txId;

    @Column(name = "LABEL_NAME")
    private String labelName;

    @Column(name = "jumlah")
    private Integer jumlah;

    @Column(name = "REG_DATE")
    private String regDate;

    @Column(name = "FILE_URL")
    private String fileUrl;

    public Integer getTxId() {
        return txId;
    }

    public void setTxId(Integer txId) {
        this.txId = txId;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public Integer getJumlah() {
        return jumlah;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }
}
