package com.cmsmelon.Model;

import javax.persistence.*;

/**
 * Created by IT19 on 08/09/2017.
 */
@Entity
@NamedNativeQueries({
        @NamedNativeQuery(name="Genre.listGenre",
                query="select \n" +
                        "*,\n" +
                        "CONCAT(GENRE_NAME,' ',(select GENRE_NAME from MD_GENRE where GENRE_ID = SUBSTRING(a.GENRE_ID,1,2))) as GENRE_FULL_NAME \n" +
                        "from MD_GENRE a where GENRE_ID > 1000 and STATUS = 'A' order by GENRE_NAME",
                resultClass = Genre.class)
})

public class Genre {
    @Id
    @Column(name="GENRE_ID")
    public String genreId;

    @Column(name="GENRE_NAME")
    public String genreName;

    @Column(name="ORDER_NUM")
    public Integer orderNum;

    @Column(name="STATUS")
    public String status;

    @Column(name="REG_DATE")
    public String regDate;

    @Column(name="UPD_DATE")
    public String updDate;

    @Column(name="S_IMG_PATH")
    public String sImgPath;

    @Column(name="M_IMG_PATH")
    public String mImgPath;

    @Column(name="L_IMG_PATH")
    public String lImgPath;

    @Column(name="GENRE_DESC")
    public String genreDesc;

    @Column(name="GENRE_FULL_NAME")
    public String genreFullName;

    public String getGenreFullName() {
        return genreFullName;
    }

    public void setGenreFullName(String genreFullName) {
        this.genreFullName = genreFullName;
    }

    public String getGenreId() {
        return genreId;
    }

    public void setGenreId(String genreId) {
        this.genreId = genreId;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getsImgPath() {
        return sImgPath;
    }

    public void setsImgPath(String sImgPath) {
        this.sImgPath = sImgPath;
    }

    public String getmImgPath() {
        return mImgPath;
    }

    public void setmImgPath(String mImgPath) {
        this.mImgPath = mImgPath;
    }

    public String getlImgPath() {
        return lImgPath;
    }

    public void setlImgPath(String lImgPath) {
        this.lImgPath = lImgPath;
    }

    public String getGenreDesc() {
        return genreDesc;
    }

    public void setGenreDesc(String genreDesc) {
        this.genreDesc = genreDesc;
    }
}
