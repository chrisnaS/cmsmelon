package com.cmsmelon.Model;

/**
 * Created by IT19 on 09/10/2017.
 */

import javax.persistence.*;

@Entity
@NamedNativeQueries({
        @NamedNativeQuery(name = "InsertProcedure.saveAlbumArtist",
                query = "SELECT INS_ALBUM_ARTIST(:albumId,:artistId) AS msg FROM DUAL ",
                resultClass = InsertProcedure.class
        ),
        @NamedNativeQuery(name = "InsertProcedure.deleteAlbumArtist",
                query = "SELECT DLT_ALBUM_ARTIST(:albumId) AS msg FROM DUAL ",
                resultClass = InsertProcedure.class
        ),
        @NamedNativeQuery(name = "InsertProcedure.approvedSong",
                query = "SELECT APPROVED_SONG(:songId,:lcStatusCd) AS msg FROM DUAL ",
                resultClass = InsertProcedure.class
        ),
        @NamedNativeQuery(name = "InsertProcedure.approvedArtist",
                query = "SELECT APPROVE_ARTIST(:songId,:prodArtistId) AS msg FROM DUAL ",
                resultClass = InsertProcedure.class
        ),
        @NamedNativeQuery(name = "InsertProcedure.approvedAlbum",
                query = "SELECT APPROVE_ALBUM(:albumId,:prodAlbumId) AS msg FROM DUAL ",
                resultClass = InsertProcedure.class
        ),
        @NamedNativeQuery(name = "InsertProcedure.approveSong",
                query = "SELECT APPROVE_SONG(:songId,:prodSongId) AS msg FROM DUAL ",
                resultClass = InsertProcedure.class
        ),
        @NamedNativeQuery(name = "InsertProcedure.checkArtist",
                query = "SELECT CHECK_ARTIST_PROD(:artistId) AS msg FROM DUAL ",
                resultClass = InsertProcedure.class
        ),
        @NamedNativeQuery(name = "InsertProcedure.checkAlbum",
                query = "SELECT CHECK_ALBUM_PROD(:albumId) AS msg FROM DUAL ",
                resultClass = InsertProcedure.class
        ),
        @NamedNativeQuery(name = "InsertProcedure.setMainAlbum",
                query = "SELECT SET_MAIN_ALBUM(:songId,:albumId) AS msg FROM DUAL ",
                resultClass = InsertProcedure.class
        ),
        @NamedNativeQuery(name = "InsertProcedure.setSongEncoder",
                query = "SELECT SET_SONG_ENCODER(:songId,:category) AS msg FROM DUAL ",
                resultClass = InsertProcedure.class
        ),
        @NamedNativeQuery(name = "InsertProcedure.insTxMlb",
                query = "SELECT INS_TX_MLB() AS msg FROM DUAL ",
                resultClass = InsertProcedure.class
        ),
        @NamedNativeQuery(name = "InsertProcedure.insTdMlb",
                query = "SELECT INS_TD_MLB(:txId,:labelCode,:jumlah) AS msg FROM DUAL ",
                resultClass = InsertProcedure.class
        ),
        @NamedNativeQuery(name = "InsertProcedure.updTxMlb",
                query = "SELECT UPD_TX_MLB(:txId,:fileUrl) AS msg FROM DUAL ",
                resultClass = InsertProcedure.class
        )
})
public class InsertProcedure {
    @Id
    @Column(name="MSG")
    private Integer msg;

    public Integer getMsg() {
        return msg;
    }

    public void setMsg(Integer msg) {
        this.msg = msg;
    }
}
