package com.cmsmelon.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by IT19 on 18/08/2017.
 */

@Entity
@NamedNativeQueries({
        @NamedNativeQuery(name = "Song.listSongByName",
                query = "select a.*, " +
                        "        b.ALBUM_NAME, " +
                        "        c.ARTIST_NAME, " +
                        "        case when a.TITLE_SONG_YN = 'Y' then 'Yes' else 'No' end as TITLE_SONG, " +
                        "        case when a.HIT_SONG_YN = 'Y' then 'Yes' else 'No' end as HIT_SONG, " +
                        "        case when a.MOD_YN = 'Y' then 'Yes' else 'No' end as `MOD`, " +
                        "        case when a.VOD_YN = 'Y' then 'Yes' else 'No' end as VOD, " +
                        "        case when a.RT_YN = 'Y' then 'Yes' else 'No' end as RT, " +
                        "        case when a.RBT_YN = 'Y' then 'Yes' else 'No' end as RBT, " +
                        "        case when a.TEXT_LYRIC_YN = 'Y' then 'Yes' else 'No' end as TEXT_LYRIC, " +
                        "        case when a.SLF_LYRIC_YN = 'Y' then 'Yes' else 'No' end as SLF_LYRIC, " +
                        "        CONCAT(SUBSTRING(a.ISSUE_DATE,1,4),'-',SUBSTRING(a.ISSUE_DATE,5,2),'-',SUBSTRING(a.ISSUE_DATE,7,2)) as ISSUE_DATE_STR, " +
                        "        DATE_FORMAT(a.REG_DATE,'%d-%m-%Y') as REG_DATE_STR, " +
                        "        DATE_FORMAT(a.UPD_DATE,'%d-%m-%Y') as UPD_DATE_STR,  " +
                        "        g.LABEL_NAME as LABEL_NAME, " +
                        "        h.GENRE_NAME as GENRE_NAME, " +
                        "        f.COMMON_CD_NAME " +
                        "        from SONG a " +
                        "        left outer join ALBUM_MELON b " +
                        "             on a.ALBUM_ID = b.ALBUM_ID " +
                        "        left outer join ARTIST c " +
                        "                    on a.ARTIST_ID = c.ARTIST_ID " +
                        "        left outer join MD_COMMON_CD f " +
                        "                    on a.LC_STATUS_CD = f.COMMON_CD " +
                        "        left outer join LABEL g " +
                        "                    on a.LABEL_CD = g.LABEL_CD " +
                        "        left outer join MD_GENRE h  " +
                        "            on a.GENRE_ID = h.GENRE_ID " +
                        "where a.SONG_NAME like concat(:search ,'%') " +
                        "and a.IS_COMMERCIAL_YN like concat('%',:isCommercialYn ,'%') " +
                        "and a.LABEL_CD = :labelCd \n" +
                        "and a.LC_STATUS_CD like concat('%',:lcStatusCd ,'%')  \n" +
                        " limit :offset,10  ",
                resultClass = Song.class
        ),
        @NamedNativeQuery(name = "Song.listSongById",
                query = "select a.*,  \n" +
                        "        b.ALBUM_NAME,   \n" +
                        "        c.ARTIST_NAME,   \n" +
                        "        case when a.TITLE_SONG_YN = 'Y' then 'Yes' else 'No' end as TITLE_SONG, \n" +
                        "        case when a.HIT_SONG_YN = 'Y' then 'Yes' else 'No' end as HIT_SONG, \n" +
                        "        case when a.MOD_YN = 'Y' then 'Yes' else 'No' end as `MOD`, \n" +
                        "        case when a.VOD_YN = 'Y' then 'Yes' else 'No' end as VOD, \n" +
                        "        case when a.RT_YN = 'Y' then 'Yes' else 'No' end as RT, \n" +
                        "        case when a.RBT_YN = 'Y' then 'Yes' else 'No' end as RBT, \n" +
                        "        case when a.TEXT_LYRIC_YN = 'Y' then 'Yes' else 'No' end as TEXT_LYRIC, \n" +
                        "        case when a.SLF_LYRIC_YN = 'Y' then 'Yes' else 'No' end as SLF_LYRIC, \n" +
                        "        CONCAT(SUBSTRING(a.ISSUE_DATE,1,4),'-',SUBSTRING(a.ISSUE_DATE,5,2),'-',SUBSTRING(a.ISSUE_DATE,7,2)) as ISSUE_DATE_STR, \n" +
                        "        DATE_FORMAT(a.REG_DATE,'%d-%m-%Y') as REG_DATE_STR, \n" +
                        "        DATE_FORMAT(a.UPD_DATE,'%d-%m-%Y') as UPD_DATE_STR,  \n" +
                        "        g.LABEL_NAME as LABEL_NAME, \n" +
                        "        h.GENRE_NAME as GENRE_NAME, \n" +
                        "        f.COMMON_CD_NAME\n" +
                        "        from SONG a\n" +
                        "        left outer join ALBUM_MELON b     \n" +
                        "             on a.ALBUM_ID = b.ALBUM_ID \n" +
                        "        left outer join ARTIST c  \n" +
                        "                    on a.ARTIST_ID = c.ARTIST_ID \n" +
                        "        left outer join MD_COMMON_CD f \n" +
                        "                    on a.LC_STATUS_CD = f.COMMON_CD\n" +
                        "        left outer join LABEL g  \n" +
                        "                    on a.LABEL_CD = g.LABEL_CD\n" +
                        "        left outer join MD_GENRE h  \n" +
                        "            on a.GENRE_ID = h.GENRE_ID " +
                        "where a.SONG_ID like concat(:search ,'%') " +
                        "and a.IS_COMMERCIAL_YN like concat('%',:isCommercialYn ,'%') " +
                        "and a.LABEL_CD = :labelCd \n" +
                        "and a.LC_STATUS_CD like concat('%',:lcStatusCd ,'%')  \n" +
                        " limit :offset,10  ",
                resultClass = Song.class
        ),
        @NamedNativeQuery(name = "Song.listAdminSongByName",
                query = "select a.*, \n" +
                        "b.ALBUM_NAME,  \n" +
                        "c.ARTIST_NAME,  \n" +
                        "case when a.TITLE_SONG_YN = 'Y' then 'Yes' else 'No' end as TITLE_SONG,\n" +
                        "case when a.HIT_SONG_YN = 'Y' then 'Yes' else 'No' end as HIT_SONG,\n" +
                        "case when a.MOD_YN = 'Y' then 'Yes' else 'No' end as `MOD`,\n" +
                        "case when a.VOD_YN = 'Y' then 'Yes' else 'No' end as VOD,\n" +
                        "case when a.RT_YN = 'Y' then 'Yes' else 'No' end as RT,\n" +
                        "case when a.RBT_YN = 'Y' then 'Yes' else 'No' end as RBT,\n" +
                        "case when a.TEXT_LYRIC_YN = 'Y' then 'Yes' else 'No' end as TEXT_LYRIC,\n" +
                        "case when a.SLF_LYRIC_YN = 'Y' then 'Yes' else 'No' end as SLF_LYRIC,\n" +
                        "CONCAT(SUBSTRING(a.ISSUE_DATE,1,4),'-',SUBSTRING(a.ISSUE_DATE,5,2),'-',SUBSTRING(a.ISSUE_DATE,7,2)) as ISSUE_DATE_STR,\n" +
                        "DATE_FORMAT(a.REG_DATE,'%d-%m-%Y') as REG_DATE_STR,\n" +
                        "DATE_FORMAT(a.UPD_DATE,'%d-%m-%Y') as UPD_DATE_STR, \n" +
                        "g.LABEL_NAME as LABEL_NAME,\n" +
                        "h.GENRE_NAME as GENRE_NAME,\n" +
                        "f.COMMON_CD_NAME from SONG a \n" +
                        "left outer join  ALBUM_MELON b  \n" +
                        "    on a.ALBUM_ID = b.ALBUM_ID \n" +
                        "    left outer join  ARTIST c  \n" +
                        "    on a.ARTIST_ID = c.ARTIST_ID \n" +
                        "    left outer join  MD_COMMON_CD f \n" +
                        "    on a.LC_STATUS_CD = f.COMMON_CD \n" +
                        "    left outer join  LABEL g  \n" +
                        "    on a.LABEL_CD = g.LABEL_CD \n" +
                        "    left outer join  MD_GENRE h  \n" +
                        "    on a.GENRE_ID = h.GENRE_ID  "+
                        "where a.SONG_NAME like concat(:search ,'%') and a.IS_COMMERCIAL_YN = :isCommercialYn \n" +
//                        "ORDER BY a.REG_DATE DESC" +
                        " limit :offset,10  ",
                resultClass = Song.class
        ),
        @NamedNativeQuery(name = "Song.listAdminSongById",
                query = "select a.*, \n" +
                        "b.ALBUM_NAME,  \n" +
                        "c.ARTIST_NAME,  \n" +
                        "case when a.TITLE_SONG_YN = 'Y' then 'Yes' else 'No' end as TITLE_SONG,\n" +
                        "case when a.HIT_SONG_YN = 'Y' then 'Yes' else 'No' end as HIT_SONG,\n" +
                        "case when a.MOD_YN = 'Y' then 'Yes' else 'No' end as `MOD`,\n" +
                        "case when a.VOD_YN = 'Y' then 'Yes' else 'No' end as VOD,\n" +
                        "case when a.RT_YN = 'Y' then 'Yes' else 'No' end as RT,\n" +
                        "case when a.RBT_YN = 'Y' then 'Yes' else 'No' end as RBT,\n" +
                        "case when a.TEXT_LYRIC_YN = 'Y' then 'Yes' else 'No' end as TEXT_LYRIC,\n" +
                        "case when a.SLF_LYRIC_YN = 'Y' then 'Yes' else 'No' end as SLF_LYRIC,\n" +
                        "CONCAT(SUBSTRING(a.ISSUE_DATE,1,4),'-',SUBSTRING(a.ISSUE_DATE,5,2),'-',SUBSTRING(a.ISSUE_DATE,7,2)) as ISSUE_DATE_STR,\n" +
                        "DATE_FORMAT(a.REG_DATE,'%d-%m-%Y') as REG_DATE_STR,\n" +
                        "DATE_FORMAT(a.UPD_DATE,'%d-%m-%Y') as UPD_DATE_STR, \n" +
                        "g.LABEL_NAME as LABEL_NAME,\n" +
                        "h.GENRE_NAME as GENRE_NAME,\n" +
                        "f.COMMON_CD_NAME from SONG a \n" +
                        "left outer join  ALBUM_MELON b  \n" +
                        "    on a.ALBUM_ID = b.ALBUM_ID \n" +
                        "    left outer join  ARTIST c  \n" +
                        "    on a.ARTIST_ID = c.ARTIST_ID \n" +
                        "    left outer join  MD_COMMON_CD f \n" +
                        "    on a.LC_STATUS_CD = f.COMMON_CD \n" +
                        "    left outer join  LABEL g  \n" +
                        "    on a.LABEL_CD = g.LABEL_CD \n" +
                        "    left outer join  MD_GENRE h  \n" +
                        "    on a.GENRE_ID = h.GENRE_ID  "+
                        "where a.SONG_ID like concat(:search ,'%') and a.IS_COMMERCIAL_YN = :isCommercialYn \n" +
//                        "ORDER BY a.REG_DATE DESC" +
                        " limit :offset,10  ",
                resultClass = Song.class
        ),
        @NamedNativeQuery(name = "Song.listAdminSongByLabel",
                query = "select a.*, \n" +
                        "b.ALBUM_NAME,  \n" +
                        "c.ARTIST_NAME,  \n" +
                        "case when a.TITLE_SONG_YN = 'Y' then 'Yes' else 'No' end as TITLE_SONG,\n" +
                        "case when a.HIT_SONG_YN = 'Y' then 'Yes' else 'No' end as HIT_SONG,\n" +
                        "case when a.MOD_YN = 'Y' then 'Yes' else 'No' end as `MOD`,\n" +
                        "case when a.VOD_YN = 'Y' then 'Yes' else 'No' end as VOD,\n" +
                        "case when a.RT_YN = 'Y' then 'Yes' else 'No' end as RT,\n" +
                        "case when a.RBT_YN = 'Y' then 'Yes' else 'No' end as RBT,\n" +
                        "case when a.TEXT_LYRIC_YN = 'Y' then 'Yes' else 'No' end as TEXT_LYRIC,\n" +
                        "case when a.SLF_LYRIC_YN = 'Y' then 'Yes' else 'No' end as SLF_LYRIC,\n" +
                        "CONCAT(SUBSTRING(a.ISSUE_DATE,1,4),'-',SUBSTRING(a.ISSUE_DATE,5,2),'-',SUBSTRING(a.ISSUE_DATE,7,2)) as ISSUE_DATE_STR,\n" +
                        "DATE_FORMAT(a.REG_DATE,'%d-%m-%Y') as REG_DATE_STR,\n" +
                        "DATE_FORMAT(a.UPD_DATE,'%d-%m-%Y') as UPD_DATE_STR, \n" +
                        "g.LABEL_NAME as LABEL_NAME,\n" +
                        "h.GENRE_NAME as GENRE_NAME,\n" +
                        "f.COMMON_CD_NAME from SONG a \n" +
                        "left outer join  ALBUM_MELON b  \n" +
                        "    on a.ALBUM_ID = b.ALBUM_ID \n" +
                        "    left outer join  ARTIST c  \n" +
                        "    on a.ARTIST_ID = c.ARTIST_ID \n" +
                        "    left outer join  MD_COMMON_CD f \n" +
                        "    on a.LC_STATUS_CD = f.COMMON_CD \n" +
                        "    left outer join  LABEL g  \n" +
                        "    on a.LABEL_CD = g.LABEL_CD \n" +
                        "    left outer join  MD_GENRE h  \n" +
                        "    on a.GENRE_ID = h.GENRE_ID  "+
                        "where a.LABEL_CD = :search and a.IS_COMMERCIAL_YN = :isCommercialYn  \n" +
//                        "ORDER BY a.REG_DATE DESC" +
                        " limit :offset,10  ",
                resultClass = Song.class
        ),
        @NamedNativeQuery(name = "Song.listAdminSongByAlbum",
                query = "select a.*, \n" +
                        "b.ALBUM_NAME,  \n" +
                        "c.ARTIST_NAME,  \n" +
                        "case when a.TITLE_SONG_YN = 'Y' then 'Yes' else 'No' end as TITLE_SONG,\n" +
                        "case when a.HIT_SONG_YN = 'Y' then 'Yes' else 'No' end as HIT_SONG,\n" +
                        "case when a.MOD_YN = 'Y' then 'Yes' else 'No' end as `MOD`,\n" +
                        "case when a.VOD_YN = 'Y' then 'Yes' else 'No' end as VOD,\n" +
                        "case when a.RT_YN = 'Y' then 'Yes' else 'No' end as RT,\n" +
                        "case when a.RBT_YN = 'Y' then 'Yes' else 'No' end as RBT,\n" +
                        "case when a.TEXT_LYRIC_YN = 'Y' then 'Yes' else 'No' end as TEXT_LYRIC,\n" +
                        "case when a.SLF_LYRIC_YN = 'Y' then 'Yes' else 'No' end as SLF_LYRIC,\n" +
                        "CONCAT(SUBSTRING(a.ISSUE_DATE,1,4),'-',SUBSTRING(a.ISSUE_DATE,5,2),'-',SUBSTRING(a.ISSUE_DATE,7,2)) as ISSUE_DATE_STR,\n" +
                        "DATE_FORMAT(a.REG_DATE,'%d-%m-%Y') as REG_DATE_STR,\n" +
                        "DATE_FORMAT(a.UPD_DATE,'%d-%m-%Y') as UPD_DATE_STR, \n" +
                        "upper(e.LABEL_NAME) as LABEL_NAME,\n" +
                        "upper(d.GENRE_NAME) as GENRE_NAME "+
                        "from SONG a \n" +
                        "inner join ALBUM_MELON b \n" +
                        "on a.ALBUM_ID = b.ALBUM_ID\n" +
                        "inner join ARTIST c \n" +
                        "on a.ARTIST_ID = c.ARTIST_ID\n" +
                        "left outer join MD_GENRE d \n" +
                        "on a.GENRE_ID = d.GENRE_ID\n" +
                        "left outer join LABEL e \n" +
                        "on a.LABEL_CD = e.LABEL_CD "+
                        "where a.ALBUM_ID = :albumId \n" +
                        " " ,
                resultClass = Song.class
        )
})

public class Song {
    @Id
    @Column(name="SONG_ID")
    private String songId;

    @Column(name="PROD_SONG_ID")
    private Long prodSongId;

    @Column(name="SONG_NAME")
    private String songName;

    @Column(name="SONG_NAME_ORIGIN")
    private String songNameOrigin;

    @Column(name="MLB_CD")
    private String mlbCd;

    @Column(name="GENID_CD")
    private String genidCd;

    @Column(name="CP1_CD")
    private String cp1Cd;

    @Column(name="CP2_CD")
    private String cp2Cd;

    @Column(name="CP3_CD")
    private String cp3Cd;

    @Column(name="CP4_CD")
    private String cp4Cd;

    @Column(name="CP5_CD")
    private String cp5Cd;

    @Column(name="HINT")
    private String hint;

    @Column(name="DISK_NO")
    private Float diskNo;

    @Column(name="TRACK_NO")
    private Float trackNo;

    @Column(name="PLAY_TIME")
    private Float playTime;

    @Column(name="TITLE_SONG_YN")
    private String titleSongYn;

    @Column(name="HIT_SONG_YN")
    private String hitSongYn;

    @Column(name="MOD_YN")
    private String modYn;

    @Column(name="VOD_YN")
    private String vodYn;

    @Column(name="RT_YN")
    private String rtYn;

    @Column(name="RBT_YN")
    private String rbtYn;

    @Column(name="TEXT_LYRIC_YN")
    private String textLyricYn;

    @Column(name="SLF_LYRIC_YN")
    private String slfLyricYn;

    @Column(name="GENRE_ID")
    private String genreId;

    @Column(name="ALBUM_ID")
    private Float albumId;

    @Column(name="ARTIST_ID")
    private Float artistId;

    @Column(name="ISSUE_DATE")
    private String issueDate;

    @Column(name="ORDER_ISSUE_DATE")
    private String orderIssueDate;

    @Column(name="ALBUM_REG_DATE")
    private Date albumRegDate;

    @Column(name="SELL_STREAM_YN")
    private String sellStreamYn;

    @Column(name="SELL_DRM_YN")
    private String sellDrmYn;

    @Column(name="SELL_NON_DRM_YN")
    private String sellNonDrmYn;

    @Column(name="SELL_ALACARTE_YN")
    private String sellAlacarteYn;

    @Column(name="DRM_PAYMENT_PROD_ID")
    private String drmPaymentProdId;

    @Column(name="DRM_PRICE")
    private Float drmPrice;

    @Column(name="NON_DRM_PAYMENT_PROD_ID")
    private String nonDrmPaymentProdId;

    @Column(name="NON_DRM_PRICE")
    private String nonDrmPrice;

    @Column(name="SEARCH_KEYWORD")
    private String searchKeyword;

    @Column(name="ADULT_YN")
    private String adultYn;

    @Column(name="LC_STATUS_CD")
    private String lcStatusCd;

    @Column(name="LABEL_CD")
    private String labelCd;

    @Column(name="CHANNEL_GROUP_CD")
    private String channelGroupCd;

    @Column(name="WORKER_ID")
    private String workerId;

    @Column(name="REG_DATE")
    private Date regDate;

    @Column(name="UPD_DATE")
    private Date updDate;

    @Column(name="ALBUM_NAME")
    private String albumName;

    @Column(name="ARTIST_NAME")
    private String artistName;

    @Column(name="REG_DATE_STR")
    private String regDateStr;


    public Long getProdSongId() {
        return prodSongId;
    }

    public void setProdSongId(Long prodSongId) {
        this.prodSongId = prodSongId;
    }

    @Column(name="TITLE_SONG")
    private String titleSong;
    @Column(name="HIT_SONG")
    private String hitSong;
    @Column(name="MOD")
    private String mod;
    @Column(name="VOD")
    private String vod;
    @Column(name="RT")
    private String rt;
    @Column(name="RBT")
    private String rbt;
    @Column(name="TEXT_LYRIC")
    private String textLyric;
    @Column(name="SLF_LYRIC")
    private String SlfLyric;
    @Column(name="ISSUE_DATE_STR")
    private String issueDateStr;

    @Column(name="IS_COMMERCIAL_YN")
    private String isCommercialYn;

    @Column(name="ORIGINAL_FILE_NAME")
    private String originalFileName;

    @Column(name="FULL_FILE_PATH")
    private String fullFilePath;

    @Column(name="LYRIC_PATH")
    private String lyricPath;

    @Column(name="LABEL_NAME")
    private String labelName;

    @Column(name="GENRE_NAME")
    private String genreName;

    @Column(name="MAIN_SONG_YN")
    private String mainSongYn;


    public String getMainSongYn() {
        return mainSongYn;
    }

    public void setMainSongYn(String mainSongYn) {
        this.mainSongYn = mainSongYn;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public String getFullFilePath() {
        return fullFilePath;
    }

    public void setFullFilePath(String fullFilePath) {
        this.fullFilePath = fullFilePath;
    }

    public String getLyricPath() {
        return lyricPath;
    }

    public void setLyricPath(String lyricPath) {
        this.lyricPath = lyricPath;
    }

    public String getIsCommercialYn() {
        return isCommercialYn;
    }

    public void setIsCommercialYn(String isCommercialYn) {
        this.isCommercialYn = isCommercialYn;
    }

    public String getTitleSong() {
        return titleSong;
    }

    public void setTitleSong(String titleSong) {
        this.titleSong = titleSong;
    }

    public String getHitSong() {
        return hitSong;
    }

    public void setHitSong(String hitSong) {
        this.hitSong = hitSong;
    }

    public String getMod() {
        return mod;
    }

    public void setMod(String mod) {
        this.mod = mod;
    }

    public String getVod() {
        return vod;
    }

    public void setVod(String vod) {
        this.vod = vod;
    }

    public String getRt() {
        return rt;
    }

    public void setRt(String rt) {
        this.rt = rt;
    }

    public String getRbt() {
        return rbt;
    }

    public void setRbt(String rbt) {
        this.rbt = rbt;
    }

    public String getTextLyric() {
        return textLyric;
    }

    public void setTextLyric(String textLyric) {
        this.textLyric = textLyric;
    }

    public String getSlfLyric() {
        return SlfLyric;
    }

    public void setSlfLyric(String slfLyric) {
        SlfLyric = slfLyric;
    }

    public String getIssueDateStr() {
        return issueDateStr;
    }

    public void setIssueDateStr(String issueDateStr) {
        this.issueDateStr = issueDateStr;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getRegDateStr() {
        return regDateStr;
    }

    public void setRegDateStr(String regDateStr) {
        this.regDateStr = regDateStr;
    }

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getSongNameOrigin() {
        return songNameOrigin;
    }

    public void setSongNameOrigin(String songNameOrigin) {
        this.songNameOrigin = songNameOrigin;
    }

    public String getMlbCd() {
        return mlbCd;
    }

    public void setMlbCd(String mlbCd) {
        this.mlbCd = mlbCd;
    }

    public String getGenidCd() {
        return genidCd;
    }

    public void setGenidCd(String genidCd) {
        this.genidCd = genidCd;
    }

    public String getCp1Cd() {
        return cp1Cd;
    }

    public void setCp1Cd(String cp1Cd) {
        this.cp1Cd = cp1Cd;
    }

    public String getCp2Cd() {
        return cp2Cd;
    }

    public void setCp2Cd(String cp2Cd) {
        this.cp2Cd = cp2Cd;
    }

    public String getCp3Cd() {
        return cp3Cd;
    }

    public void setCp3Cd(String cp3Cd) {
        this.cp3Cd = cp3Cd;
    }

    public String getCp4Cd() {
        return cp4Cd;
    }

    public void setCp4Cd(String cp4Cd) {
        this.cp4Cd = cp4Cd;
    }

    public String getCp5Cd() {
        return cp5Cd;
    }

    public void setCp5Cd(String cp5Cd) {
        this.cp5Cd = cp5Cd;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public Float getDiskNo() {
        return diskNo;
    }

    public void setDiskNo(Float diskNo) {
        this.diskNo = diskNo;
    }

    public Float getTrackNo() {
        return trackNo;
    }

    public void setTrackNo(Float trackNo) {
        this.trackNo = trackNo;
    }

    public Float getPlayTime() {
        return playTime;
    }

    public void setPlayTime(Float playTime) {
        this.playTime = playTime;
    }

    public String getTitleSongYn() {
        return titleSongYn;
    }

    public void setTitleSongYn(String titleSongYn) {
        this.titleSongYn = titleSongYn;
    }

    public String getHitSongYn() {
        return hitSongYn;
    }

    public void setHitSongYn(String hitSongYn) {
        this.hitSongYn = hitSongYn;
    }

    public String getModYn() {
        return modYn;
    }

    public void setModYn(String modYn) {
        this.modYn = modYn;
    }

    public String getVodYn() {
        return vodYn;
    }

    public void setVodYn(String vodYn) {
        this.vodYn = vodYn;
    }

    public String getRtYn() {
        return rtYn;
    }

    public void setRtYn(String rtYn) {
        this.rtYn = rtYn;
    }

    public String getRbtYn() {
        return rbtYn;
    }

    public void setRbtYn(String rbtYn) {
        this.rbtYn = rbtYn;
    }

    public String getTextLyricYn() {
        return textLyricYn;
    }

    public void setTextLyricYn(String textLyricYn) {
        this.textLyricYn = textLyricYn;
    }

    public String getSlfLyricYn() {
        return slfLyricYn;
    }

    public void setSlfLyricYn(String slfLyricYn) {
        this.slfLyricYn = slfLyricYn;
    }

    public String getGenreId() {
        return genreId;
    }

    public void setGenreId(String genreId) {
        this.genreId = genreId;
    }

    public Float getAlbumId() {
        return albumId;
    }

    public void setAlbumId(Float albumId) {
        this.albumId = albumId;
    }

    public Float getArtistId() {
        return artistId;
    }

    public void setArtistId(Float artistId) {
        this.artistId = artistId;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getOrderIssueDate() {
        return orderIssueDate;
    }

    public void setOrderIssueDate(String orderIssueDate) {
        this.orderIssueDate = orderIssueDate;
    }

    public Date getAlbumRegDate() {
        return albumRegDate;
    }

    public void setAlbumRegDate(Date albumRegDate) {
        this.albumRegDate = albumRegDate;
    }

    public String getSellStreamYn() {
        return sellStreamYn;
    }

    public void setSellStreamYn(String sellStreamYn) {
        this.sellStreamYn = sellStreamYn;
    }

    public String getSellDrmYn() {
        return sellDrmYn;
    }

    public void setSellDrmYn(String sellDrmYn) {
        this.sellDrmYn = sellDrmYn;
    }

    public String getSellNonDrmYn() {
        return sellNonDrmYn;
    }

    public void setSellNonDrmYn(String sellNonDrmYn) {
        this.sellNonDrmYn = sellNonDrmYn;
    }

    public String getSellAlacarteYn() {
        return sellAlacarteYn;
    }

    public void setSellAlacarteYn(String sellAlacarteYn) {
        this.sellAlacarteYn = sellAlacarteYn;
    }

    public String getDrmPaymentProdId() {
        return drmPaymentProdId;
    }

    public void setDrmPaymentProdId(String drmPaymentProdId) {
        this.drmPaymentProdId = drmPaymentProdId;
    }

    public Float getDrmPrice() {
        return drmPrice;
    }

    public void setDrmPrice(Float drmPrice) {
        this.drmPrice = drmPrice;
    }

    public String getNonDrmPaymentProdId() {
        return nonDrmPaymentProdId;
    }

    public void setNonDrmPaymentProdId(String nonDrmPaymentProdId) {
        this.nonDrmPaymentProdId = nonDrmPaymentProdId;
    }

    public String getNonDrmPrice() {
        return nonDrmPrice;
    }

    public void setNonDrmPrice(String nonDrmPrice) {
        this.nonDrmPrice = nonDrmPrice;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getAdultYn() {
        return adultYn;
    }

    public void setAdultYn(String adultYn) {
        this.adultYn = adultYn;
    }

    public String getLcStatusCd() {
        return lcStatusCd;
    }

    public void setLcStatusCd(String lcStatusCd) {
        this.lcStatusCd = lcStatusCd;
    }

    public String getLabelCd() {
        return labelCd;
    }

    public void setLabelCd(String labelCd) {
        this.labelCd = labelCd;
    }

    public String getChannelGroupCd() {
        return channelGroupCd;
    }

    public void setChannelGroupCd(String channelGroupCd) {
        this.channelGroupCd = channelGroupCd;
    }

    public String getWorkerId() {
        return workerId;
    }

    public void setWorkerId(String workerId) {
        this.workerId = workerId;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public Date getUpdDate() {
        return updDate;
    }

    public void setUpdDate(Date updDate) {
        this.updDate = updDate;
    }
}
