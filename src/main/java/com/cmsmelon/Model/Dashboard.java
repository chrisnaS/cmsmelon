package com.cmsmelon.Model;

/**
 * Created by IT19 on 20/10/2017.
 */

import javax.persistence.*;

@Entity
@NamedNativeQueries({
        @NamedNativeQuery(name = "Dashboard.listDashboard",
                query = "select TOTAL_ALBUM,TOTAL_ARTIST,TOTAL_SONG from (\n" +
                        "(select count(0) as TOTAL_ALBUM, 1 as DESCR from ALBUM_MELON) a\n" +
                        "inner join\n" +
                        "(select count(0) as TOTAL_ARTIST, 1 as DESCR from ARTIST) b \n" +
                        "on a.descr = b.descr\n" +
                        "inner join\n" +
                        "(select COUNT(0) as TOTAL_SONG, 1 as DESCR from SONG where LABEL_CD = :labelCd ) c \n" +
                        "on a.descr = c.descr)",
                resultClass = Dashboard.class
        )
})
public class Dashboard{
    @Id
    @Column(name="TOTAL_ALBUM")
    private Integer totalAlbum;

    @Column(name="TOTAL_ARTIST")
    private Integer totalArtist;

    @Column(name="TOTAL_SONG")
    private Integer totalSong;

    public Integer getTotalAlbum() {
        return totalAlbum;
    }

    public void setTotalAlbum(Integer totalAlbum) {
        this.totalAlbum = totalAlbum;
    }

    public Integer getTotalArtist() {
        return totalArtist;
    }

    public void setTotalArtist(Integer totalArtist) {
        this.totalArtist = totalArtist;
    }

    public Integer getTotalSong() {
        return totalSong;
    }

    public void setTotalSong(Integer totalSong) {
        this.totalSong = totalSong;
    }
}
