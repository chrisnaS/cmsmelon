package com.cmsmelon.Model;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by IT19 on 29/08/2017.
 */
@Entity
@Table(name="`SONG`")
@NamedNativeQueries({
        @NamedNativeQuery(name = "SongAdd.songById",
                query = "SELECT * FROM SONG where SONG_ID = :songId  " ,
                resultClass = SongAdd.class
        )
})
@GenericGenerator(
        name = "hilo_sequence_generator",
        strategy = "enhanced-table",
        parameters = {
                @Parameter(name = "table_name", value = "TABLE_SEQ"),
                @Parameter(name = "value_column_name", value = "seq_id"),
                @Parameter(name = "segment_column_name", value = "seq_name"),
                @Parameter(name = "segment_value", value = "SONG")
        }
)
public class SongAdd {
    @Id
    @GeneratedValue(generator = "hilo_sequence_generator")
    @Column(name="SONG_ID")
    private Integer songId;

    @Column(name="SONG_NAME")
    private String songName;

    @Column(name="SONG_NAME_ORIGIN")
    private String songNameOrigin;

    @Column(name="MLB_CD")
    private String mlbCd;

    @Column(name="GENID_CD")
    private String genidCd;

    @Column(name="CP1_CD")
    private String cp1Cd;

    @Column(name="CP2_CD")
    private String cp2Cd;

    @Column(name="CP3_CD")
    private String cp3Cd;

    @Column(name="CP4_CD")
    private String cp4Cd;

    @Column(name="CP5_CD")
    private String cp5Cd;

    @Column(name="HINT")
    private String hint;

    @Column(name="DISK_NO")
    private Integer diskNo;

    @Column(name="TRACK_NO")
    private Integer trackNo;

    @Column(name="PLAY_TIME")
    private Integer playTime;

    @Column(name="TITLE_SONG_YN")
    private String titleSongYn;

    @Column(name="HIT_SONG_YN")
    private String hitSongYn;

    @Column(name="MOD_YN")
    private String modYn;

    @Column(name="VOD_YN")
    private String vodYn;

    @Column(name="RT_YN")
    private String rtYn;

    @Column(name="RBT_YN")
    private String rbtYn;

    @Column(name="TEXT_LYRIC_YN")
    private String textLyricYn;

    @Column(name="SLF_LYRIC_YN")
    private String slfLyricYn;

    @Column(name="GENRE_ID")
    private String genreId;

    @Column(name="ALBUM_ID")
    private Integer albumId;

    @Column(name="ARTIST_ID")
    private Integer artistId;

    @Column(name="ISSUE_DATE")
    private String issueDate;

    @Column(name="ORDER_ISSUE_DATE")
    private String orderIssueDate;

    @Column(name="ALBUM_REG_DATE")
    private Date albumRegDate;

    @Column(name="SELL_STREAM_YN")
    private String sellStreamYn;

    @Column(name="SELL_DRM_YN")
    private String sellDrmYn;

    @Column(name="SELL_NON_DRM_YN")
    private String sellNonDrmYn;

    @Column(name="SELL_ALACARTE_YN")
    private String sellAlacarteYn;

    @Column(name="DRM_PAYMENT_PROD_ID")
    private String drmPaymentProdId;

    @Column(name="DRM_PRICE")
    private Integer drmPrice;

    @Column(name="NON_DRM_PAYMENT_PROD_ID")
    private String nonDrmPaymentProdId;

    @Column(name="NON_DRM_PRICE")
    private String nonDrmPrice;

    @Column(name="SEARCH_KEYWORD")
    private String searchKeyword;

    @Column(name="ADULT_YN")
    private String adultYn;

    @Column(name="LC_STATUS_CD")
    private String lcStatusCd;

    @Column(name="LABEL_CD")
    private String labelCd;

    @Column(name="CHANNEL_GROUP_CD")
    private String channelGroupCd;

    @Column(name="WORKER_ID")
    private String workerId;

    @Column(name="REG_DATE")
    private Date regDate;

    @Column(name="UPD_DATE")
    private Date updDate;

    @Column(name="IS_COMMERCIAL_YN")
    private String isCommercialYn;

    @Column(name="ORIGINAL_FILE_NAME")
    private String originalFileName;

    @Column(name="FULL_FILE_PATH")
    private String fullFilePath;

    @Column(name="LYRIC_PATH")
    private String lyricPath;

    @Column(name="MAIN_SONG_YN")
    private String mainSongYn;

    @Column(name="PROD_SONG_ID")
    private Integer prodSongId;

    public Integer getProdSongId() {
        return prodSongId;
    }

    public void setProdSongId(Integer prodSongId) {
        this.prodSongId = prodSongId;
    }

    public String getMainSongYn() {
        return mainSongYn;
    }

    public void setMainSongYn(String mainSongYn) {
        this.mainSongYn = mainSongYn;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public String getFullFilePath() {
        return fullFilePath;
    }

    public void setFullFilePath(String fullFilePath) {
        this.fullFilePath = fullFilePath;
    }

    public String getLyricPath() {
        return lyricPath;
    }

    public void setLyricPath(String lyricPath) {
        this.lyricPath = lyricPath;
    }

    public String getIsCommercialYn() {
        return isCommercialYn;
    }

    public void setIsCommercialYn(String isCommercialYn) {
        this.isCommercialYn = isCommercialYn;
    }

    public Integer getSongId() {
        return songId;
    }

    public void setSongId(Integer songId) {
        this.songId = songId;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getSongNameOrigin() {
        return songNameOrigin;
    }

    public void setSongNameOrigin(String songNameOrigin) {
        this.songNameOrigin = songNameOrigin;
    }

    public String getMlbCd() {
        return mlbCd;
    }

    public void setMlbCd(String mlbCd) {
        this.mlbCd = mlbCd;
    }

    public String getGenidCd() {
        return genidCd;
    }

    public void setGenidCd(String genidCd) {
        this.genidCd = genidCd;
    }

    public String getCp1Cd() {
        return cp1Cd;
    }

    public void setCp1Cd(String cp1Cd) {
        this.cp1Cd = cp1Cd;
    }

    public String getCp2Cd() {
        return cp2Cd;
    }

    public void setCp2Cd(String cp2Cd) {
        this.cp2Cd = cp2Cd;
    }

    public String getCp3Cd() {
        return cp3Cd;
    }

    public void setCp3Cd(String cp3Cd) {
        this.cp3Cd = cp3Cd;
    }

    public String getCp4Cd() {
        return cp4Cd;
    }

    public void setCp4Cd(String cp4Cd) {
        this.cp4Cd = cp4Cd;
    }

    public String getCp5Cd() {
        return cp5Cd;
    }

    public void setCp5Cd(String cp5Cd) {
        this.cp5Cd = cp5Cd;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public Integer getDiskNo() {
        return diskNo;
    }

    public void setDiskNo(Integer diskNo) {
        this.diskNo = diskNo;
    }

    public Integer getTrackNo() {
        return trackNo;
    }

    public void setTrackNo(Integer trackNo) {
        this.trackNo = trackNo;
    }

    public Integer getPlayTime() {
        return playTime;
    }

    public void setPlayTime(Integer playTime) {
        this.playTime = playTime;
    }

    public String getTitleSongYn() {
        return titleSongYn;
    }

    public void setTitleSongYn(String titleSongYn) {
        this.titleSongYn = titleSongYn;
    }

    public String getHitSongYn() {
        return hitSongYn;
    }

    public void setHitSongYn(String hitSongYn) {
        this.hitSongYn = hitSongYn;
    }

    public String getModYn() {
        return modYn;
    }

    public void setModYn(String modYn) {
        this.modYn = modYn;
    }

    public String getVodYn() {
        return vodYn;
    }

    public void setVodYn(String vodYn) {
        this.vodYn = vodYn;
    }

    public String getRtYn() {
        return rtYn;
    }

    public void setRtYn(String rtYn) {
        this.rtYn = rtYn;
    }

    public String getRbtYn() {
        return rbtYn;
    }

    public void setRbtYn(String rbtYn) {
        this.rbtYn = rbtYn;
    }

    public String getTextLyricYn() {
        return textLyricYn;
    }

    public void setTextLyricYn(String textLyricYn) {
        this.textLyricYn = textLyricYn;
    }

    public String getSlfLyricYn() {
        return slfLyricYn;
    }

    public void setSlfLyricYn(String slfLyricYn) {
        this.slfLyricYn = slfLyricYn;
    }

    public String getGenreId() {
        return genreId;
    }

    public void setGenreId(String genreId) {
        this.genreId = genreId;
    }

    public Integer getAlbumId() {
        return albumId;
    }

    public void setAlbumId(Integer albumId) {
        this.albumId = albumId;
    }

    public Integer getArtistId() {
        return artistId;
    }

    public void setArtistId(Integer artistId) {
        this.artistId = artistId;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getOrderIssueDate() {
        return orderIssueDate;
    }

    public void setOrderIssueDate(String orderIssueDate) {
        this.orderIssueDate = orderIssueDate;
    }

    public Date getAlbumRegDate() {
        return albumRegDate;
    }

    public void setAlbumRegDate(Date albumRegDate) {
        this.albumRegDate = albumRegDate;
    }

    public String getSellStreamYn() {
        return sellStreamYn;
    }

    public void setSellStreamYn(String sellStreamYn) {
        this.sellStreamYn = sellStreamYn;
    }

    public String getSellDrmYn() {
        return sellDrmYn;
    }

    public void setSellDrmYn(String sellDrmYn) {
        this.sellDrmYn = sellDrmYn;
    }

    public String getSellNonDrmYn() {
        return sellNonDrmYn;
    }

    public void setSellNonDrmYn(String sellNonDrmYn) {
        this.sellNonDrmYn = sellNonDrmYn;
    }

    public String getSellAlacarteYn() {
        return sellAlacarteYn;
    }

    public void setSellAlacarteYn(String sellAlacarteYn) {
        this.sellAlacarteYn = sellAlacarteYn;
    }

    public String getDrmPaymentProdId() {
        return drmPaymentProdId;
    }

    public void setDrmPaymentProdId(String drmPaymentProdId) {
        this.drmPaymentProdId = drmPaymentProdId;
    }

    public Integer getDrmPrice() {
        return drmPrice;
    }

    public void setDrmPrice(Integer drmPrice) {
        this.drmPrice = drmPrice;
    }

    public String getNonDrmPaymentProdId() {
        return nonDrmPaymentProdId;
    }

    public void setNonDrmPaymentProdId(String nonDrmPaymentProdId) {
        this.nonDrmPaymentProdId = nonDrmPaymentProdId;
    }

    public String getNonDrmPrice() {
        return nonDrmPrice;
    }

    public void setNonDrmPrice(String nonDrmPrice) {
        this.nonDrmPrice = nonDrmPrice;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getAdultYn() {
        return adultYn;
    }

    public void setAdultYn(String adultYn) {
        this.adultYn = adultYn;
    }

    public String getLcStatusCd() {
        return lcStatusCd;
    }

    public void setLcStatusCd(String lcStatusCd) {
        this.lcStatusCd = lcStatusCd;
    }

    public String getLabelCd() {
        return labelCd;
    }

    public void setLabelCd(String labelCd) {
        this.labelCd = labelCd;
    }

    public String getChannelGroupCd() {
        return channelGroupCd;
    }

    public void setChannelGroupCd(String channelGroupCd) {
        this.channelGroupCd = channelGroupCd;
    }

    public String getWorkerId() {
        return workerId;
    }

    public void setWorkerId(String workerId) {
        this.workerId = workerId;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public Date getUpdDate() {
        return updDate;
    }

    public void setUpdDate(Date updDate) {
        this.updDate = updDate;
    }
}
