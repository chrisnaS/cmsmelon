package com.cmsmelon.Model;

import javax.persistence.*;

/**
 * Created by IT19 on 18/08/2017.
 */
@Entity
@NamedNativeQueries({
        @NamedNativeQuery(name = "Cint.listAlbumCntByName",
                query = "SELECT COUNT(0) as total FROM ALBUM_MELON a \n" +
                        "where a.ALBUM_NAME like concat(:search ,'%') " +
                        "and a.LC_STATUS_CD like concat('%', :lcStatusCd ,'%') and a.LABEL_CD like concat('%', :labelCd ,'%') ",
                resultClass = Cint.class
        ),
        @NamedNativeQuery(name = "Cint.listAlbumCntById",
                query = "SELECT COUNT(0) as total FROM ALBUM_MELON a \n" +
                        "where a.ALBUM_ID like concat(:search ,'%') " +
                        "and a.LC_STATUS_CD like concat('%', :lcStatusCd ,'%') and a.LABEL_CD like concat('%', :labelCd ,'%') ",
                resultClass = Cint.class
        ),
        @NamedNativeQuery(name = "Cint.listArtistCntByName",
                query = "SELECT COUNT(0) as total FROM ARTIST where ARTIST_NAME like concat(:search ,'%') " +
                        "and LC_STATUS_CD like concat('%',:lcStatusCd ,'%')",
                resultClass = Cint.class
        ),
        @NamedNativeQuery(name = "Cint.listArtistCntById",
                query = "SELECT COUNT(0) as total FROM ARTIST where ARTIST_ID like concat(:search ,'%') " +
                        "and LC_STATUS_CD like concat('%',:lcStatusCd ,'%')",
                resultClass = Cint.class
        ),
        @NamedNativeQuery(name = "Cint.listSongCntByName",
        query = "select count(0) as total \n" +
                "                from SONG a " +
                "where a.SONG_NAME like concat(:search ,'%') " +
                "and a.IS_COMMERCIAL_YN = :isCommercialYn " +
                "and a.LC_STATUS_CD like concat('%',:lcStatusCd ,'%')  \n" +
                "AND a.LABEL_CD = :labelCd ",
        resultClass = Cint.class
        ),
        @NamedNativeQuery(name = "Cint.listSongCntById",
                query = "select count(0) as total \n" +
                        "                from SONG a " +
                        "where a.SONG_ID like concat(:search ,'%') " +
                        "and a.IS_COMMERCIAL_YN = :isCommercialYn " +
                        "and a.LC_STATUS_CD like concat('%',:lcStatusCd ,'%')  \n" +
                        "AND a.LABEL_CD = :labelCd ",
                resultClass = Cint.class
        ),
        @NamedNativeQuery(name = "Cint.listAdminSongCntById",
                query = "select count(0) as total \n" +
                        "    from SONG a "+
                        "where a.SONG_ID like concat(:search ,'%') and a.IS_COMMERCIAL_YN = :isCommercialYn ",
                resultClass = Cint.class
        ),
        @NamedNativeQuery(name = "Cint.listAdminSongCntByName",
                query = "select count(0) as total \n" +
                        "    from SONG a "+
                        "where a.SONG_NAME like concat(:search ,'%') and a.IS_COMMERCIAL_YN = :isCommercialYn \n",
                resultClass = Cint.class
        ),
        @NamedNativeQuery(name = "Cint.listAdminSongCntByLabel",
            query = "select count(0) as total \n" +
                    "    from SONG a "+
                    "where a.LABEL_CD = :search and a.IS_COMMERCIAL_YN = :isCommercialYn ",
                resultClass = Cint.class
        ),
        @NamedNativeQuery(name = "Cint.listUsers",
                query = "select count(0) as total\n" +
                        "from CMS_USER a \n" +
                        "left join LABEL b \n" +
                        "on a.LABEL_CD = b.LABEL_CD\n" +
                        "where USER_NAME like concat('%',:search ,'%') ",
                resultClass = Cint.class
        ),
        @NamedNativeQuery(name = "Cint.listMlb",
                query = "select count(0) as total from V_MLB_CODE where LABEL_NAME like concat('%', :search ,'%')  ",
                resultClass = Cint.class
        ),

        @NamedNativeQuery(name = "Cint.listMlbLabelList",
                query = "select count(0) as total from LABEL a \n" +
                        "left outer join MLB_LABEL b \n" +
                        "on a.LABEL_CD = b.LABEL_CODE where a.LABEL_NAME like concat('%',:search ,'%') ",
                resultClass = Cint.class
        ),
        @NamedNativeQuery(name = "Cint.filterArtistName",
                query = "select count(0) as total " +
                        "from ARTIST where trim(artist_name) = trim(:search)",
                resultClass = Cint.class
        )
})
public class Cint {
    @Id
    @Column(name = "total")
    private Integer totalSize;

    public Integer getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Integer totalSize) {
        this.totalSize = totalSize;
    }
}
