package com.cmsmelon.Model;

/**
 * Created by IT19 on 30/08/2017.
 */
public class Success {
    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Success{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
