package com.cmsmelon.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by IT19 on 08/09/2017.
 */
@Entity
@NamedNativeQueries({
        @NamedNativeQuery(name="ArtistRole.listArtistRole",
                        query="select * from MD_ARTIST_ROLE order by ROLE_NAME",
                        resultClass = ArtistRole.class)
})
public class ArtistRole {
    @Id
    @Column(name="ROLE_TYPE_CD")
    public String roleTypeCd;

    @Column(name="ROLE_NAME")
    public String roleName;

    @Column(name="REG_DATE")
    public Date regDate;

    @Column(name="UPD_DATE")
    public Date updDate;

    public String getRoleTypeCd() {
        return roleTypeCd;
    }

    public void setRoleTypeCd(String roleTypeCd) {
        this.roleTypeCd = roleTypeCd;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public Date getUpdDate() {
        return updDate;
    }

    public void setUpdDate(Date updDate) {
        this.updDate = updDate;
    }
}
