package com.cmsmelon.Model;

import javax.persistence.*;

/**
 * Created by IT19 on 09/10/2017.
 */
@Entity
@NamedNativeQueries({
        @NamedNativeQuery(name = "AlbumSong.listAlbumSong",
                query = "select \n" +
                        "`a`.`ALBUM_ID` AS `ALBUM_ID`,\n" +
                        "`a`.`ALBUM_NAME` AS `ALBUM_NAME`,\n" +
                        "`a`.`LABEL_CD` AS `LABEL_CD`,\n" +
                        "a.SERIES_NO - (select count(0) from SONG where ALBUM_ID = a.ALBUM_ID) AS `AVAILABLE`,\n" +
                        "`b`.`ARTIST_NAME` AS `ARTIST_NAME`,\n" +
                        "`b`.`ARTIST_ID` AS `ARTIST_ID`,\n" +
                        "`a`.`SERIES_NO` AS `SERIES_NO`\n" +
                        "from ALBUM_MELON a LEFT OUTER join ARTIST b\n" +
                        "on a.MAIN_ARTIST_ID = b.ARTIST_ID\n" +
                        "where a.IS_COMPLETED = 0\n" +
                        "and a.ALBUM_ID like concat('%', :albumId ,'%') and a.LABEL_CD like concat('%', :labelCd ,'%') ",
                resultClass = AlbumSong.class
        )
})
public class AlbumSong {
    @Id
    @Column(name = "ALBUM_ID")
    private Integer albumId;

    @Column(name = "ALBUM_NAME")
    private String albumName;

    @Column(name="AVAILABLE")
    private Integer available;

    @Column(name="ARTIST_ID")
    private Integer artistId;

    @Column(name="ARTIST_NAME")
    private String artistName;

    @Column(name="SERIES_NO")
    private Integer seriesNo;

    public Integer getSeriesNo() {
        return seriesNo;
    }

    public void setSeriesNo(Integer seriesNo) {
        this.seriesNo = seriesNo;
    }

    public Integer getArtistId() {
        return artistId;
    }

    public void setArtistId(Integer artistId) {
        this.artistId = artistId;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public Integer getAlbumId() {
        return albumId;
    }

    public void setAlbumId(Integer albumId) {
        this.albumId = albumId;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }
}
