package com.cmsmelon.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="`MLB_SEQ`")
public class MblSeq {
    @Id
    @Column(name="LABEL_CODE")
    private String labelCd;

    @Column(name="SEQ")
    private long seq;

    public String getLabelCd() {
        return labelCd;
    }

    public void setLabelCd(String labelCd) {
        this.labelCd = labelCd;
    }

    public long getSeq() {
        return seq;
    }

    public void setSeq(long seq) {
        this.seq = seq;
    }
}
