package com.cmsmelon.Model;

import javax.persistence.*;

@Entity
@NamedNativeQueries({
        @NamedNativeQuery(name = "MlbDetail.listMlbDetail",
                query = "select LABEL_CODE,LABEL_NAME,JUMLAH from V_MLB_DETAIL where TX_ID = :txId  ",
                resultClass = MlbDetail.class
        )
})
public class MlbDetail {
    @Id
    @Column(name = "LABEL_CODE")
    private String labelCode;
    @Column(name = "LABEL_NAME")
    private String labelName;
    @Column(name = "JUMLAH")
    private Integer jumlah;

    public String getLabelCode() {
        return labelCode;
    }

    public void setLabelCode(String labelCode) {
        this.labelCode = labelCode;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public Integer getJumlah() {
        return jumlah;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }
}
