package com.cmsmelon.Model;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by IT19 on 04/09/2017.
 */
@Entity
@Table(name="`ALBUM_MELON`")

@NamedNativeQueries({
        @NamedNativeQuery(name = "AlbumAdd.albumById",
                query = "SELECT * FROM ALBUM_MELON where ALBUM_ID = :albumId  " ,
                resultClass = AlbumAdd.class
        )
})

@GenericGenerator(
        name = "hilo_sequence_generator",
        strategy = "enhanced-table",
        parameters = {
                @Parameter(name = "table_name", value = "TABLE_SEQ"),
                @Parameter(name = "value_column_name", value = "seq_id"),
                @Parameter(name = "segment_column_name", value = "seq_name"),
                @Parameter(name = "segment_value", value = "ALBUM")
        }
)
public class AlbumAdd {
    @Id
    @GeneratedValue(generator = "hilo_sequence_generator")
    @Column(name = "ALBUM_ID")
    private Integer albumId;

    @Column(name="ALBUM_NAME")
    private String albumName;

    @Column(name="ALBUM_NAME_ORIGIN")
    private String albumNameOrigin;

    @Column(name="MAIN_SONG_ID")
    private String mainSongId;

    @Column(name="MAIN_ARTIST_ID")
    private Integer mainArtistId;

    @Column(name="HINT")
    private String hint;

    @Column(name="ALBUM_TYPE_CD")
    private String albumTypeCd;

    @Column(name="SERIES_NO")
    private String seriesNo;

    @Column(name="DISK_CNT")
    private String diskCnt;

    @Column(name="TOTAL_SONG_CNT")
    private String totalSongCnt;

    @Column(name="ISSUE_DATE")
    private String issueDate;

    @Column(name="ORDER_ISSUE_DATE")
    private String orderIssueDate;

    @Column(name="ADULT_YN")
    private String adultYn;

    @Column(name="DOMESTIC_YN")
    private String domesticYn;

    @Column(name="ISSUE_COUNTRY_CD")
    private String issueCountryCd;

    @Column(name="GENRE_ID")
    private String genreId;

    @Column(name="AGENCY")
    private String agency;

    @Column(name="SELL_COMPANY")
    private String sellCompany;

    @Column(name="SEARCH_KEYWORD")
    private String searchKeyword;

    @Column(name="ALBUM_L_IMG_PATH")
    private String albumLImgPath;

    @Column(name="ALBUM_M_IMG_PATH")
    private String albumMImgPath;

    @Column(name="ALBUM_S_IMG_PATH")
    private String albumSImgPath;

    @Column(name="ALBUM_REVIEW")
    private String albumReview;

    @Column(name="RECOMMEND_CNT")
    private Integer RecommendCnt;

    @Column(name="OPPOSE_CNT")
    private Integer opposeCnt;

    @Column(name="STATUS")
    private String status;

    @Column(name="LABEL_CD")
    private String labelCd;

    @Column(name="CHANNEL_GROUP_CD")
    private String channelGroupCd;

    @Column(name="WORKER_ID")
    private String workerId;

    @Column(name = "REG_DATE")
    private Date regDate;

    @Column(name="UPD_DATE")
    private Date updDate;

    @Column(name="PROD_ALBUM_ID")
    private Integer prodAlbumId;

    @Column(name="LC_STATUS_CD")
    private String lcStatusCd;

    public String getLcStatusCd() {
        return lcStatusCd;
    }

    public void setLcStatusCd(String lcStatusCd) {
        this.lcStatusCd = lcStatusCd;
    }

    public Integer getProdAlbumId() {
        return prodAlbumId;
    }

    public void setProdAlbumId(Integer prodAlbumId) {
        this.prodAlbumId = prodAlbumId;
    }

    public Integer getAlbumId() {
        return albumId;
    }

    public void setAlbumId(Integer albumId) {
        this.albumId = albumId;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public String getAlbumNameOrigin() {
        return albumNameOrigin;
    }

    public void setAlbumNameOrigin(String albumNameOrigin) {
        this.albumNameOrigin = albumNameOrigin;
    }

    public String getMainSongId() {
        return mainSongId;
    }

    public void setMainSongId(String mainSongId) {
        this.mainSongId = mainSongId;
    }

    public Integer getMainArtistId() {
        return mainArtistId;
    }

    public void setMainArtistId(Integer mainArtistId) {
        this.mainArtistId = mainArtistId;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getAlbumTypeCd() {
        return albumTypeCd;
    }

    public void setAlbumTypeCd(String albumTypeCd) {
        this.albumTypeCd = albumTypeCd;
    }

    public String getSeriesNo() {
        return seriesNo;
    }

    public void setSeriesNo(String seriesNo) {
        this.seriesNo = seriesNo;
    }

    public String getDiskCnt() {
        return diskCnt;
    }

    public void setDiskCnt(String diskCnt) {
        this.diskCnt = diskCnt;
    }

    public String getTotalSongCnt() {
        return totalSongCnt;
    }

    public void setTotalSongCnt(String totalSongCnt) {
        this.totalSongCnt = totalSongCnt;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getOrderIssueDate() {
        return orderIssueDate;
    }

    public void setOrderIssueDate(String orderIssueDate) {
        this.orderIssueDate = orderIssueDate;
    }

    public String getAdultYn() {
        return adultYn;
    }

    public void setAdultYn(String adultYn) {
        this.adultYn = adultYn;
    }

    public String getDomesticYn() {
        return domesticYn;
    }

    public void setDomesticYn(String domesticYn) {
        this.domesticYn = domesticYn;
    }

    public String getIssueCountryCd() {
        return issueCountryCd;
    }

    public void setIssueCountryCd(String issueCountryCd) {
        this.issueCountryCd = issueCountryCd;
    }

    public String getGenreId() {
        return genreId;
    }

    public void setGenreId(String genreId) {
        this.genreId = genreId;
    }

    public String getAgency() {
        return agency;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public String getSellCompany() {
        return sellCompany;
    }

    public void setSellCompany(String sellCompany) {
        this.sellCompany = sellCompany;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getAlbumLImgPath() {
        return albumLImgPath;
    }

    public void setAlbumLImgPath(String albumLImgPath) {
        this.albumLImgPath = albumLImgPath;
    }

    public String getAlbumMImgPath() {
        return albumMImgPath;
    }

    public void setAlbumMImgPath(String albumMImgPath) {
        this.albumMImgPath = albumMImgPath;
    }

    public String getAlbumSImgPath() {
        return albumSImgPath;
    }

    public void setAlbumSImgPath(String albumSImgPath) {
        this.albumSImgPath = albumSImgPath;
    }

    public String getAlbumReview() {
        return albumReview;
    }

    public void setAlbumReview(String albumReview) {
        this.albumReview = albumReview;
    }

    public Integer getRecommendCnt() {
        return RecommendCnt;
    }

    public void setRecommendCnt(Integer recommendCnt) {
        RecommendCnt = recommendCnt;
    }

    public Integer getOpposeCnt() {
        return opposeCnt;
    }

    public void setOpposeCnt(Integer opposeCnt) {
        this.opposeCnt = opposeCnt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLabelCd() {
        return labelCd;
    }

    public void setLabelCd(String labelCd) {
        this.labelCd = labelCd;
    }

    public String getChannelGroupCd() {
        return channelGroupCd;
    }

    public void setChannelGroupCd(String channelGroupCd) {
        this.channelGroupCd = channelGroupCd;
    }

    public String getWorkerId() {
        return workerId;
    }

    public void setWorkerId(String workerId) {
        this.workerId = workerId;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public Date getUpdDate() {
        return updDate;
    }

    public void setUpdDate(Date updDate) {
        this.updDate = updDate;
    }
}
