package com.cmsmelon.Model;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by IT19 on 29/08/2017.
 */
@Entity
@Table(name="`ARTIST`")
@NamedNativeQueries({
        @NamedNativeQuery(name = "ArtistCrud.artistById",
                query = "SELECT * FROM ARTIST where ARTIST_ID = :artistId  " ,
                resultClass = ArtistCrud.class
        )
})
@GenericGenerator(
        name = "hilo_sequence_generator",
        strategy = "enhanced-table",
        parameters = {
                @Parameter(name = "table_name", value = "TABLE_SEQ"),
                @Parameter(name = "value_column_name", value = "seq_id"),
                @Parameter(name = "segment_column_name", value = "seq_name"),
                @Parameter(name = "segment_value", value = "ARTIST")
        }
)
public class ArtistCrud {
    @Id
    @GeneratedValue(generator = "hilo_sequence_generator")
    @Column(name="ARTIST_ID")
    private Integer artistId;

    @Column(name="ARTIST_NAME")
    private String artistName;

    @Column(name="ARTIST_NAME_ORIGIN")
    private String artistNameOrigin;

    @Column(name="NICKNAME")
    private String nickname;

    @Column(name="HINT")
    private String hint;

    @Column(name="GROUP_YN")
    private String groupYn;

    @Column(name="ARTIST_ROLE_TYPE_CD")
    private String artistRoleTypeCd;

    @Column(name="DOMESTIC_YN")
    private String domesticYn;

    @Column(name="NATIONALITY_CD")
    private String nationalityCd;

    @Column(name="GENRE_ID")
    private String genreId;

    @Column(name="BIRTHDAY")
    private String birthday;

    @Column(name="DEATHDAY")
    private String deathday;

    @Column(name="DEBUT_DAY")
    private String debutDay;

    @Column(name="DEBUT_SONG")
    private String debutSong;

    @Column(name="PERFORM_YEARS")
    private String performYears;

    @Column(name="GENDER")
    private String gender;

    @Column(name="BLOOD_TYPE")
    private String bloodType;

    @Column(name="RELIGION")
    private String religion;

    @Column(name="HOBBY")
    private String hobby;

    @Column(name="ARTIST_PRIORITY")
    private Integer artistPriority;

    @Column(name="HOMEPAGE")
    private String homepage;

    @Column(name="TWITTER_URL")
    private String twitterUrl;

    @Column(name="FACEBOOK_URL")
    private String facebookUrl;

    @Column(name="SEARCH_YN")
    private String searchYn;

    @Column(name="SEARCH_KEYWORD")
    private String searchKeyword;

    @Column(name="PROFILE")
    private String profile;

    @Column(name="ARTIST_L_IMG_PATH")
    private String artistLImgPath;

    @Column(name="ARTIST_M_IMG_PATH")
    private String artistMImgPath;

    @Column(name="ARTIST_S_IMG_PATH")
    private String artistSImgPath;

    @Column(name="STATUS")
    private String status;

    @Column(name="LABEL_CD")
    private String labelCd;

    @Column(name="CHANNEL_GROUP_CD")
    private String channelGroupCd;

    @Column(name="WORKER_ID")
    private String workerId;

    @Column(name="REG_DATE")
    private Date regDate;

    @Column(name="UPD_DATE")
    private Date updDate;



    public Integer getArtistId() {
        return artistId;
    }

    public void setArtistId(Integer artistId) {
        this.artistId = artistId;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getArtistNameOrigin() {
        return artistNameOrigin;
    }

    public void setArtistNameOrigin(String artistNameOrigin) {
        this.artistNameOrigin = artistNameOrigin;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getGroupYn() {
        return groupYn;
    }

    public void setGroupYn(String groupYn) {
        this.groupYn = groupYn;
    }

    public String getArtistRoleTypeCd() {
        return artistRoleTypeCd;
    }

    public void setArtistRoleTypeCd(String artistRoleTypeCd) {
        this.artistRoleTypeCd = artistRoleTypeCd;
    }

    public String getDomesticYn() {
        return domesticYn;
    }

    public void setDomesticYn(String domesticYn) {
        this.domesticYn = domesticYn;
    }

    public String getNationalityCd() {
        return nationalityCd;
    }

    public void setNationalityCd(String nationalityCd) {
        this.nationalityCd = nationalityCd;
    }

    public String getGenreId() {
        return genreId;
    }

    public void setGenreId(String genreId) {
        this.genreId = genreId;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getDeathday() {
        return deathday;
    }

    public void setDeathday(String deathday) {
        this.deathday = deathday;
    }

    public String getDebutDay() {
        return debutDay;
    }

    public void setDebutDay(String debutDay) {
        this.debutDay = debutDay;
    }

    public String getDebutSong() {
        return debutSong;
    }

    public void setDebutSong(String debutSong) {
        this.debutSong = debutSong;
    }

    public String getPerformYears() {
        return performYears;
    }

    public void setPerformYears(String performYears) {
        this.performYears = performYears;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public Integer getArtistPriority() {
        return artistPriority;
    }

    public void setArtistPriority(Integer artistPriority) {
        this.artistPriority = artistPriority;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getTwitterUrl() {
        return twitterUrl;
    }

    public void setTwitterUrl(String twitterUrl) {
        this.twitterUrl = twitterUrl;
    }

    public String getFacebookUrl() {
        return facebookUrl;
    }

    public void setFacebookUrl(String facebookUrl) {
        this.facebookUrl = facebookUrl;
    }

    public String getSearchYn() {
        return searchYn;
    }

    public void setSearchYn(String searchYn) {
        this.searchYn = searchYn;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getArtistLImgPath() {
        return artistLImgPath;
    }

    public void setArtistLImgPath(String artistLImgPath) {
        this.artistLImgPath = artistLImgPath;
    }

    public String getArtistMImgPath() {
        return artistMImgPath;
    }

    public void setArtistMImgPath(String artistMImgPath) {
        this.artistMImgPath = artistMImgPath;
    }

    public String getArtistSImgPath() {
        return artistSImgPath;
    }

    public void setArtistSImgPath(String artistSImgPath) {
        this.artistSImgPath = artistSImgPath;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLabelCd() {
        return labelCd;
    }

    public void setLabelCd(String labelCd) {
        this.labelCd = labelCd;
    }

    public String getChannelGroupCd() {
        return channelGroupCd;
    }

    public void setChannelGroupCd(String channelGroupCd) {
        this.channelGroupCd = channelGroupCd;
    }

    public String getWorkerId() {
        return workerId;
    }

    public void setWorkerId(String workerId) {
        this.workerId = workerId;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public Date getUpdDate() {
        return updDate;
    }

    public void setUpdDate(Date updDate) {
        this.updDate = updDate;
    }
}
