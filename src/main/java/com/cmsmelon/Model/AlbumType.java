package com.cmsmelon.Model;

import javax.persistence.*;

/**
 * Created by IT19 on 07/09/2017.
 */
@Entity
@NamedNativeQueries({
        @NamedNativeQuery(name = "AlbumType.listAlbumType",
                query = "select * from MD_ALBUM_TYPE WHERE STATUS = 1 order by type_name  ",
                resultClass = AlbumType.class
        )
})

public class AlbumType {
    @Id
    @Column(name="TYPE_CD")
    public String typeCd;

    @Column(name="TYPE_NAME")
    public String typeName;

    @Column(name="REG_DATE")
    public String regDate;

    @Column(name="UPD_DATE")
    public String updDate;



    public String getTypeCd() {
        return typeCd;
    }

    public void setTypeCd(String typeCd) {
        this.typeCd = typeCd;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }
}
