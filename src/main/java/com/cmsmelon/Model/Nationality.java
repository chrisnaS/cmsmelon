package com.cmsmelon.Model;

import javax.persistence.*;

/**
 * Created by IT19 on 16/10/2017.
 */
@Entity
@NamedNativeQueries({
        @NamedNativeQuery(name = "Nationality.listNationality",
                query = "SELECT COMMON_CD,COMMON_CD_NAME FROM MD_COMMON_CD " +
                        "WHERE COMMON_CD_GROUP = 'NC' order by COMMON_CD_NAME  ",
                resultClass = Nationality.class
        )
})
public class Nationality {
    @Id
    @Column(name="COMMON_CD")
    private String commonCd;

    @Column(name="COMMON_CD_NAME")
    private String commonCdName;

    public String getCommonCd() {
        return commonCd;
    }

    public void setCommonCd(String commonCd) {
        this.commonCd = commonCd;
    }

    public String getCommonCdName() {
        return commonCdName;
    }

    public void setCommonCdName(String commonCdName) {
        this.commonCdName = commonCdName;
    }
}
