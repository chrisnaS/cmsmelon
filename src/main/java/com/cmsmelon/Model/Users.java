package com.cmsmelon.Model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by IT19 on 19/09/2017.
 */
@Entity
@Table(name="`CMS_USER`")
@NamedNativeQueries({
        @NamedNativeQuery(
                name = "Users.Login",
                query = "select * from CMS_USER " +
                        "where USER_NAME = :username and USER_PASSWORD = :password",
                resultClass = Users.class
        ),
        @NamedNativeQuery(
                name = "Users.listById",
                query = "select * from CMS_USER  " +
                        "where ID = :id  ",
                resultClass = Users.class
        ),
        @NamedNativeQuery(
                name = "Users.listByUsername",
                query = "select * from CMS_USER  " +
                        "where USER_NAME = :username  ",
                resultClass = Users.class
        )
})
@GenericGenerator(
        name = "hilo_sequence_generator",
        strategy = "enhanced-table",
        parameters = {
                @org.hibernate.annotations.Parameter(name = "table_name", value = "TABLE_SEQ"),
                @org.hibernate.annotations.Parameter(name = "value_column_name", value = "seq_id"),
                @org.hibernate.annotations.Parameter(name = "segment_column_name", value = "seq_name"),
                @org.hibernate.annotations.Parameter(name = "segment_value", value = "USER")
        }
)
public class Users {
    @Id
    @GeneratedValue(generator = "hilo_sequence_generator")
    @Column(name = "ID")
    private Integer id;

    @Column(name = "USER_NAME")
    private String userName;

    @Column(name = "USER_PASSWORD")
    private String userPassword;

    @Column(name = "FULL_NAME")
    private String fullName;

    @Column(name = "COMPANY_ID")
    private String companyId;

    @Column(name = "DATA1")
    private String data1;

    @Column(name = "DATA2")
    private String data2;

    @Column(name = "DATA3")
    private String data3;

    @Column(name = "REG_BY")
    private String regBy;

    @Column(name = "REG_DATE")
    private Date regDate;

    @Column(name = "UPD_BY")
    private String updBy;

    @Column(name = "UPD_DATE")
    private Date updDate;

    @Column(name = "LABEL_CD")
    private String labelCd;

    @Column(name = "ROLE_TYPE")
    private String roleType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getData1() {
        return data1;
    }

    public void setData1(String data1) {
        this.data1 = data1;
    }

    public String getData2() {
        return data2;
    }

    public void setData2(String data2) {
        this.data2 = data2;
    }

    public String getData3() {
        return data3;
    }

    public void setData3(String data3) {
        this.data3 = data3;
    }

    public String getRegBy() {
        return regBy;
    }

    public void setRegBy(String regBy) {
        this.regBy = regBy;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public String getUpdBy() {
        return updBy;
    }

    public void setUpdBy(String updBy) {
        this.updBy = updBy;
    }

    public Date getUpdDate() {
        return updDate;
    }

    public void setUpdDate(Date updDate) {
        this.updDate = updDate;
    }

    public String getLabelCd() {
        return labelCd;
    }

    public void setLabelCd(String labelCd) {
        this.labelCd = labelCd;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }
}
