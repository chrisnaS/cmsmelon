package com.cmsmelon.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by IT19 on 25/10/2017.
 */
@Entity
@Table(name="`SONG_MOD`")
@NamedNativeQueries({
        @NamedNativeQuery(name = "SongModAdd.songModById",
                query = "select * from SONG_MOD where SONG_ID =  :songId  " ,
                resultClass = SongModAdd.class
        )
})
public class SongModAdd {
    @Id
    @Column(name="SONG_MOD_ID")
    private Integer songModId;

    @Column(name="SONG_ID")
    private String songId;

    @Column(name="SAMPLING")
    private Integer sampling;

    @Column(name="CODEC_TYPE_CD")
    private String codecTypeCd;

    @Column(name="BIT_RATE_CD")
    private String bitRateCd;

    @Column(name="PLAY_TIME")
    private Integer playTime;

    @Column(name="FILE_SIZE")
    private Integer fileSize;

    @Column(name="ORIGINAL_FILE_NAME")
    private String originalFileName;

    @Column(name="FULL_FILE_PATH")
    private String fullFilePath;

    @Column(name="STATUS")
    private String status;

    @Column(name="WORKER_ID")
    private String workerId;

    @Column(name="REG_DATE")
    private Date regDate;

    @Column(name="UPD_DATE")
    private Date updDate;

    public Integer getSongModId() {
        return songModId;
    }

    public void setSongModId(Integer songModId) {
        this.songModId = songModId;
    }

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }

    public Integer getSampling() {
        return sampling;
    }

    public void setSampling(Integer sampling) {
        this.sampling = sampling;
    }

    public String getCodecTypeCd() {
        return codecTypeCd;
    }

    public void setCodecTypeCd(String codecTypeCd) {
        this.codecTypeCd = codecTypeCd;
    }

    public String getBitRateCd() {
        return bitRateCd;
    }

    public void setBitRateCd(String bitRateCd) {
        this.bitRateCd = bitRateCd;
    }

    public Integer getPlayTime() {
        return playTime;
    }

    public void setPlayTime(Integer playTime) {
        this.playTime = playTime;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public String getFullFilePath() {
        return fullFilePath;
    }

    public void setFullFilePath(String fullFilePath) {
        this.fullFilePath = fullFilePath;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWorkerId() {
        return workerId;
    }

    public void setWorkerId(String workerId) {
        this.workerId = workerId;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public Date getUpdDate() {
        return updDate;
    }

    public void setUpdDate(Date updDate) {
        this.updDate = updDate;
    }
}
