package com.cmsmelon.ModelScheduler;

import com.cmsmelon.BO.SongBo;
import com.cmsmelon.Model.*;
import com.cmsmelon.Repository.*;
import com.cmsmelon.helper.GenerateExcell;
import com.cmsmelon.helper.GenerateTxt;
import com.cmsmelon.helper.UnirestClient;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Component
public class JobSchedule {
    @Autowired
    private SongModAddRepository songModAddRepository;
    @Autowired
    private SongTmpRepository songTmpRepository;
    @Autowired
    private SongAddRepository songAddRepository;
    @Autowired
    private UnirestClient unirestClient;
    @Autowired
    private SongBo songBo;

    @Autowired
    private MlbCodeRepository mlbCodeRepository;
    @Autowired
    private TmpMlbCodeRepository tmpMlbCodeRepository;
    @Autowired
    private InsertProcedureRepository insertProcedureRepository;

    private static final Logger log = LoggerFactory.getLogger(JobSchedule.class);

    @Scheduled(fixedDelay = 60000)
    public void Process() throws UnirestException, IOException, UnsupportedAudioFileException {
        SongTmp songTmp = songTmpRepository.listSongModQue();
        log.info("------ Job Start  ------------------");
        //log.info(songTmp.toString());
        if(songTmp != null){
            SongAdd songAdd = songAddRepository.songById(Integer.parseInt(songTmp.getSongId()));
            List<SongModAdd> songModAdds = songModAddRepository.songModById(Integer.parseInt(songTmp.getSongId()));
            for(SongModAdd songMod : songModAdds){
                if(!songMod.getCodecTypeCd().equals("CT0000")) {
                    if(songMod.getFullFilePath().equals("")){
                        String typeBit = null;
                        String targetFile = null;
                        if(songMod.getBitRateCd().equals("BR0320")){
                            typeBit="320";
                        }else if(songMod.getBitRateCd().equals("BR0128")){
                            typeBit="128";
                        }else if(songMod.getBitRateCd().equals("BR0192")){
                            typeBit="192";
                        }else if(songMod.getBitRateCd().equals("BR0096")){
                            typeBit="96";
                        }else if(songMod.getBitRateCd().equals("BR1411")){
                            typeBit="1411";
                        }else if(songMod.getBitRateCd().equals("BR0256")){
                            typeBit="256";
                        }

                        if(songMod.getCodecTypeCd().equals("CT0001")){
                            targetFile="mp3";
                        }else if(songMod.getCodecTypeCd().equals("CT0007")){
                            targetFile="m4a";
                        }else if(songMod.getCodecTypeCd().equals("CT0008")){
                            targetFile="flac";
                        }else if(songMod.getCodecTypeCd().equals("CT0002")){
                            targetFile="m4a";
                        }

                        String fileFix = unirestClient.GetEncoder(songAdd.getFullFilePath(),
                                typeBit,
                                songMod.getSongId(),
                                targetFile);
                        log.info(fileFix);
                        if(fileFix.length() <= 80){
                            if(!fileFix.equals("-1")){
                                songMod.setFullFilePath(fileFix);
                                songMod.setOriginalFileName(fileFix);
                                songMod.setUpdDate(new Date());
                                songMod.setStatus("A");
                                Success success = songBo.AddSongMod(songMod);
                            }
                        }
                        log.info(songMod.getSongId() + " " + typeBit +" "+targetFile+" Success !");
                    }
                }
            }
            Success resp = songBo.SetSongModJob(songAdd.getSongId(),"DEL");
            log.info(songAdd.getSongId() + " " + resp.getCode());
        }
    }

    @Scheduled(fixedDelay = 100000)
    public void Generate() {
        MlbCode mlbCode = mlbCodeRepository.listGenerateMlb();
        log.info("------ Job Start Generate Excell ------------------");
        //log.info(songTmp.toString());
        if(mlbCode != null){
            List<TmpMlbCode> tmpMlbCodes = tmpMlbCodeRepository.getData(mlbCode.getTxId());
//            GenerateExcell generateExcell = new GenerateExcell();
            GenerateTxt generateTxt = new GenerateTxt();
            String fileName = generateTxt.Generate(tmpMlbCodes);

            InsertProcedure insertProcedure = insertProcedureRepository.updTxMlb(mlbCode.getTxId(),fileName);
            log.info(String.valueOf(insertProcedure.getMsg()));
            if(insertProcedure.getMsg() > 0){
                log.info("Generate Success");
            }else{
                log.info("Generate Failed");
            }
        }
    }
}
