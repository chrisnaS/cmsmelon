package com.cmsmelon.storage;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("storage")
public class StorageProperties {

    /**
     * Folder location for storing files
     */
    //private String location = "upload-dir";
//    private String location = "src/main/resources/static/upload/image";
//
//    private String locationMp3 = "src/main/resources/static/upload/music";
//
//    private String locationLyric = "src/main/resources/static/upload/lyric";
    @Value( "${home.dir}" )
    private String pathHome;

    private String location = "image/";

    private String locationMp3 = "music";

    private String locationLyric = "lyric";

    public String getLocation() {
        return pathHome+location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationMp3() {
        return pathHome+locationMp3;
    }

    public void setLocationMp3(String locationMp3) {
        this.locationMp3 = locationMp3;
    }

    public String getLocationLyric() {
        return pathHome+locationLyric;
    }

    public void setLocationLyric(String locationLyric) {
        this.locationLyric = locationLyric;
    }
}
