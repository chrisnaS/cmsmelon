package com.cmsmelon.BO;

import com.cmsmelon.Model.*;
import com.cmsmelon.Repository.*;
import com.cmsmelon.helper.CopyFile;
import com.cmsmelon.helper.ImageResize;
import com.cmsmelon.helper.JdbcOracle;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by IT19 on 29/08/2017.
 */
@Component
public class ArtistBo {
    @Autowired
    private ArtistRepository artistRepository;

    @Autowired
    private ArtistAddRepository artistAddRepository;

    @Autowired
    private CintRepository cintRepository;

    @Autowired
    private InsertProcedureRepository insertProcedureRepository;

    @Autowired
    private ImageResize imageResize;

    @Autowired
    private CopyFile copyFile;

    @Autowired
    private LabelRepository labelRepository;

    private static final Logger log = LoggerFactory.getLogger(ArtistBo.class);

    public PagingResponse<List<Artist>> getArtistList(Integer offset, String search, String category,String labelCd,String lcStatusCd){
        PagingResponse<List<Artist>> response = new PagingResponse<>();
        response.setCode("0");
        response.setStatusCode(200);
        response.setMsg("Report generated successfully");
        response.setData(null);

        Cint cnt;
        List<Artist> artistList = null;

        if(category.equals("artist_name")){
            cnt = (Cint) cintRepository.listArtistCntByName(search,lcStatusCd);
            response.setTotalSize(cnt.getTotalSize());
            artistList =
                    artistRepository.listArtistByName(offset, search,lcStatusCd);
        }else{
            cnt = (Cint) cintRepository.listArtistCntById(search,lcStatusCd);
            response.setTotalSize(cnt.getTotalSize());
            artistList =
                    artistRepository.listArtistById(offset, search,lcStatusCd);
        }

        if(artistList!=null){
            response.setOffset(offset);
            response.setSize(artistList.size());
            response.setData(artistList);
        }else{
            response.setCode("1");
            response.setMsg("No data found in repository");
        }
        return response;
    }

    public Success AddArtist(ArtistCrud artistCrud){
        Success response = new Success();
        try{
            imageResize.ResizeAllArtistImage(artistCrud.getArtistLImgPath(),artistCrud);
            System.out.println(artistCrud.getArtistId());
            System.out.println(cintRepository.filterArtistName(artistCrud.getArtistName()).getTotalSize());
            if(artistCrud.getArtistId() == null &&
                   cintRepository.filterArtistName(artistCrud.getArtistName()).getTotalSize() > 0 ){
                response.setCode("0");
                response.setMessage("Artist sudah tersedia silahkan lanjut untuk membuat album !");
            }else{
                artistAddRepository.save(artistCrud);
                response.setCode("1");
                response.setMessage("Data berhasil disimpan");
            }

//            ThreadImageArtist threadImageArtist = new ThreadImageArtist(artistCrud.getArtistLImgPath(),
//                    artistCrud.getArtistId());
//            Thread t = new Thread(threadImageArtist);
        }catch (Exception Ex){
            response.setCode("0");
            response.setMessage(Ex.getMessage());
        }
        return response;
    }

    public Success Delete(ArtistCrud artistCrud){
        Success response = null;
        try{
            artistAddRepository.delete(artistCrud);
            response.setCode("1");
            response.setMessage("Data berhasil dihapus");
        }catch (Exception Ex){
            response.setCode("0");
            response.setMessage(Ex.getMessage());
        }
        return response;
    }

    public Success ApproveArtist (Integer artistId) throws SQLException, ClassNotFoundException {
        Success response = new Success();
        JdbcOracle ora = new JdbcOracle();
        try{

            ArtistCrud artist = new ArtistCrud();
            artist = artistAddRepository.artistById(artistId);
            Label label = labelRepository.listLabelbyCode(artist.getLabelCd());
            copyFile.CopyFileArtistImg(artist);



            Integer artistOraId = ora.ValidationArtist(artist.getArtistName(),artist.getNationalityCd());
            //ora.ConClose();
            if(artistOraId > 0){
                insertProcedureRepository.approvedArtist(artist.getArtistId(),artistOraId);
            }else{
                Integer prodArtistId = ora.getSeqArtist();
                ora.InsertArtist(artist,prodArtistId,label.getLabelName());
                insertProcedureRepository.approvedArtist(artist.getArtistId(),prodArtistId);
            }
            ora.ConClose();
            response.setCode("1");
            response.setMessage("Data berhasil diapprove");
        }catch (Exception Ex){
            ora.ConClose();
            response.setCode("0");
            response.setMessage(Ex.getMessage());
            log.info(Ex.getMessage());
        }
        return response;
    }
}
