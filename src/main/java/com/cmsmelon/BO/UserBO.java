package com.cmsmelon.BO;

import com.cmsmelon.Model.*;
import com.cmsmelon.Repository.CintRepository;
import com.cmsmelon.Repository.UserViewRepository;
import com.cmsmelon.Repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserBO
{
    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private CintRepository cintRepository;

    @Autowired
    private UserViewRepository userViewRepository;

    public Success AddUser(Users users){
        Success response = new Success();
        try{
            usersRepository.save(users);
            response.setCode("1");
            response.setMessage("Data berhasil disimpan");
        }catch (Exception ex){
            response.setCode("0");
            response.setMessage(ex.getMessage());
        }

        return response;
    }

    public PagingResponse<List<UserView>> getUser(Integer offset,
                                  String search){
        PagingResponse<List<UserView>> response = new PagingResponse<>();
        response.setCode("0");
        response.setStatusCode(200);
        response.setMsg("Report generated successfully");
        response.setData(null);
        Cint cnt;
        List<UserView> userViews = null;

        if(search == null){
            search = "";
        }

        cnt = (Cint) cintRepository.listUsers(search);
        response.setTotalSize(cnt.getTotalSize());
        userViews =
                userViewRepository.getUsers(offset, search);

        if(userViews!=null){
            response.setOffset(offset);
            response.setSize(userViews.size());
            response.setData(userViews);
        }else{
            response.setCode("1");
            response.setMsg("No data found in repository");
        }

        return response;
    }
}
