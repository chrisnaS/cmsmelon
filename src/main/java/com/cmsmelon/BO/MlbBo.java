package com.cmsmelon.BO;

import com.cmsmelon.Model.*;
import com.cmsmelon.Repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class MlbBo {
    @Autowired
    private InsertProcedureRepository insertProcedureRepository;
    @Autowired
    private CintRepository cintRepository;
    @Autowired
    private MlbCodeRepository mlbCodeRepository;

    @Autowired
    private MlbLabelRespository mlbLabelRespository;

    @Autowired
    private LabelRepository labelRepository;

    @Autowired
    private MlbSeqRepository mlbSeqRepository;

    public Success SaveMlbCode(String param){
        Success s = new Success();
        InsertProcedure tx = insertProcedureRepository.insTxMlb();
        String[] p1 = param.split("_");
        for(String w:p1){
            String[] p2 = w.split("#");
            InsertProcedure td = insertProcedureRepository.insTdMlb(
                Integer.parseInt(String.valueOf(tx.getMsg())),
                p2[0],
                Integer.parseInt(p2[1])
            );
        }
        s.setCode("1");
        s.setMessage("Data Berhasil disimpan, Silahkan didownload setelah -+ 10 menit");
        return s;
    }

    public PagingResponse<List<MlbCode>> getMlbCodeList(
            Integer offset, String search
    ){
        PagingResponse<List<MlbCode>> response = new PagingResponse<>();
        response.setCode("0");
        response.setStatusCode(200);
        response.setMsg("Report generated successfully");
        response.setData(null);

        Cint cnt;
        List<MlbCode> mlbCodeList = null;
        cnt = (Cint) cintRepository.listMlb(search);
        response.setTotalSize(cnt.getTotalSize());
        mlbCodeList =
                mlbCodeRepository.listMlb(offset, search);
        if(mlbCodeList!=null){
            response.setOffset(offset);
            response.setSize(mlbCodeList.size());
            response.setData(mlbCodeList);
        }else{
            response.setCode("1");
            response.setMsg("No data found in repository");
        }
        return response;
    }

    public List<MlbLabel> getMlbLabel(){
           List<MlbLabel> mlbLabels = mlbLabelRespository.listMlbLabel();
           return  mlbLabels;
    }

    public Success saveLabel(String labelCode,String labelName,String initial){
        Success s = new Success();
        Label label = new Label();
        label = labelRepository.listLabelbyCode(labelCode);
        if(label == null){
            label = new Label();
            label.setRegDate(new Date());
            label.setStatus("A");
        }
        label.setLabelCd(labelCode);
        label.setLabelName(labelName);

        MlbLabel mlbLabel = new MlbLabel();
        mlbLabel = mlbLabelRespository.findOne(labelCode);
        if(mlbLabel == null){
            mlbLabel = new MlbLabel();
        }
        mlbLabel.setInitial(initial);
        mlbLabel.setLabelCode(labelCode);
        mlbLabel.setLabelName(labelName);

        MblSeq mblSeq = mlbSeqRepository.findOne(labelCode);

        if(mblSeq == null){
            MblSeq mblSeq1 = new MblSeq();
            mblSeq1.setLabelCd(labelCode);
            mblSeq1.setSeq(1);

            mlbSeqRepository.save(mblSeq1);
        }

        labelRepository.save(label);
        mlbLabelRespository.save(mlbLabel);

        s.setCode("1");
        s.setMessage("Data Berhasil disimpan");
        return s;
    }

    public PagingResponse<List<MlbLabel>> getMlbLabelList(Integer offset,
                                                  String search){
        PagingResponse<List<MlbLabel>> response = new PagingResponse<>();
        response.setCode("0");
        response.setStatusCode(200);
        response.setMsg("Report generated successfully");
        response.setData(null);
        Cint cnt;
        List<MlbLabel> mlbLabels = null;

        if(search == null){
            search = "";
        }

        cnt = (Cint) cintRepository.listMlbLabelList(search);
        response.setTotalSize(cnt.getTotalSize());
        mlbLabels =
                mlbLabelRespository.listMlbLabelJoin(search,offset);

        if(mlbLabels!=null){
            response.setOffset(offset);
            response.setSize(mlbLabels.size());
            response.setData(mlbLabels);
        }else{
            response.setCode("1");
            response.setMsg("No data found in repository");
        }

        return response;
    }
}
