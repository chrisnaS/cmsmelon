package com.cmsmelon.BO;

import com.cmsmelon.Model.*;
import com.cmsmelon.ModelScheduler.JobSchedule;
import com.cmsmelon.Repository.*;
import com.cmsmelon.helper.CopyFile;
import com.cmsmelon.helper.JdbcOracle;
import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.AudioHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by IT19 on 29/08/2017.
 */

@Component
public class SongBo {

    @Autowired
    SongRepository songRepository;

    @Autowired
    AlbumAddRepository albumAddRepository ;

    @Autowired
    CintRepository cintRepository;

    @Autowired
    SongAddRepository songAddRepository;

    @Autowired
    SongModRepository songModRepository;

    @Autowired
    SongModAddRepository songModAddRepository;

    @Autowired
    InsertProcedureRepository insertProcedureRepository;

    @Autowired
    private CopyFile copyFile;

    @Autowired
    private LabelRepository labelRepository;

    private static final Logger log = LoggerFactory.getLogger(SongBo.class);


    public PagingResponse<List<Song>> getSongList(Integer offset,
                                                  String search,
                                                  String category,
                                                  String isCommercialYn,
                                                  String labelCd,
                                                  String lcStatusCd){
        PagingResponse<List<Song>> response = new PagingResponse<>();
        response.setCode("0");
        response.setStatusCode(200);
        response.setMsg("Report generated successfully");
        response.setData(null);
        Cint cnt;
        List<Song> songList = null;
        if(search == null){
            search = "";
        }
        if(category.equals("song_name")){
            cnt = (Cint) cintRepository.listSongCntByName(search,isCommercialYn,labelCd,lcStatusCd);
            response.setTotalSize(cnt.getTotalSize());
            songList =
                    songRepository.listSongByName(offset, search,isCommercialYn,labelCd,lcStatusCd);
        }else{
            cnt = (Cint) cintRepository.listSongCntById(search,isCommercialYn,labelCd,lcStatusCd);
            response.setTotalSize(cnt.getTotalSize());
            songList =
                    songRepository.listSongById(offset, search,isCommercialYn,labelCd,lcStatusCd);
        }

        if(songList!=null){
            response.setOffset(offset);
            response.setSize(songList.size());
            response.setData(songList);
        }else{
            response.setCode("1");
            response.setMsg("No data found in repository");
        }
        return response;
    }

    public PagingResponse<List<Song>> getAdminSongList(Integer offset, String search, String category, String isCommercialYn){
        PagingResponse<List<Song>> response = new PagingResponse<>();
        response.setCode("0");
        response.setStatusCode(200);
        response.setMsg("Report generated successfully");
        response.setData(null);
        Cint cnt;
        List<Song> songList = null;
        if(search == null){
            search = "";
        }
        if(category.equals("song_name")){
            cnt = (Cint) cintRepository.listAdminSongCntByName(search,isCommercialYn);
            response.setTotalSize(cnt.getTotalSize());
            songList =
                    songRepository.listAdminSongByName(offset, search,isCommercialYn);
        }else if(category.equals("label")){
            cnt = (Cint) cintRepository.listAdminSongCntByLabel(search,isCommercialYn);
            response.setTotalSize(cnt.getTotalSize());
            songList =
                    songRepository.listAdminSongByLabel(offset, search,isCommercialYn);
        }else{
            cnt = (Cint) cintRepository.listAdminSongCntById(search,isCommercialYn);
            response.setTotalSize(cnt.getTotalSize());
            songList =
                    songRepository.listAdminSongById(offset, search,isCommercialYn);
        }


        if(songList!=null){
            response.setOffset(offset);
            response.setSize(songList.size());
            response.setData(songList);
        }else{
            response.setCode("1");
            response.setMsg("No data found in repository");
        }
        return response;
    }

    public Success AddSong(SongAdd song) throws IOException, UnsupportedAudioFileException {
        Success response = new Success();
        Integer playTime = this.GetPlayTime(song.getFullFilePath());

         song.setPlayTime(playTime);
        try{
            if(song.getMainSongYn().equals("Y")){
                insertProcedureRepository.setMainAlbum(song.getSongId(), song.getAlbumId());
            }
            songAddRepository.save(song);
            response.setCode("1");
            response.setMessage("Data berhasil disimpan");
        }catch (Exception Ex){
            response.setCode("0");
            response.setMessage(Ex.getMessage());
        }
        return response;
    }

    public Success SetSongModJob(Integer songId, String category) throws IOException, UnsupportedAudioFileException {
        Success response = new Success();
        log.info(songId + " " + category);
        try{
            InsertProcedure insertProcedure = insertProcedureRepository.setSongEncoder(songId, category);
            if(insertProcedure.getMsg() == 1){
                response.setCode(String.valueOf(insertProcedure.getMsg()));
                response.setMessage("Song sudah ada dalam antrian, estimasi proses sekitar 1 jam silahkan dibuka kembali setelah 1 jam");
            }else{
                response.setCode(String.valueOf(insertProcedure.getMsg()));
                response.setMessage("Song sudah ada dalam antrian, estimasi proses sekitar 1 jam silahkan dibuka kembali setelah 1 jam");
            }

        }catch (Exception Ex){
            response.setCode("0");
            response.setMessage(Ex.getMessage());
        }
        return response;
    }

    public Success AddSongMod(SongModAdd songmod) throws IOException, UnsupportedAudioFileException {
        Success response = new Success();
//        Integer playTime = this.GetPlayTime(songmod.getFullFilePath());
//        songmod.setPlayTime(playTime);
        try{
            songModAddRepository.save(songmod);
            response.setCode("1");
            response.setMessage("Data berhasil disimpan");
        }catch (Exception Ex){
            response.setCode("0");
            response.setMessage(Ex.getMessage());
        }
        return response;
    }

    public Success Delete(SongAdd song){
        Success response = null;
        try{
            songAddRepository.delete(song);
            response.setCode("1");
            response.setMessage("Data berhasil dihapus");
        }catch (Exception Ex){
            response.setCode("0");
            response.setMessage(Ex.getMessage());
        }
        return response;
    }

    public Success DeleteSong(Integer songId){
        Success success = new Success();
        SongAdd songAdd = songAddRepository.findOne(songId);
        if(songAdd.getProdSongId() == null){
            songAddRepository.delete(songId);
            List<SongModAdd> songMods = songModAddRepository.songModById(songId);
            for(SongModAdd songModAdd : songMods ){
                copyFile.DeleteFile(songModAdd.getFullFilePath());
                songModAddRepository.delete(songModAdd.getSongModId());
                success.setCode("1");
                success.setMessage("Song Berhasil dihapus");
            }
        }else{
            success.setCode("0");
            success.setMessage("Song sudah diapprove tidak bisa didelete");
        }
        return success;

    }

    public Success ApproveSong(Integer songId){
        Success response = new Success();
        try{
            SongAdd songAdd = new SongAdd();

            songAdd = songAddRepository.songById(songId);
            Label label = labelRepository.listLabelbyCode(songAdd.getLabelCd());
            AlbumAdd albumAdd = albumAddRepository.albumById(songAdd.getAlbumId());

            InsertProcedure checkArtist = insertProcedureRepository.checkArtist(songAdd.getArtistId());
            InsertProcedure checkAlbum = insertProcedureRepository.checkAlbum(songAdd.getAlbumId());
            if(checkArtist.getMsg() != 0 && checkAlbum.getMsg() != 0){
                Integer checkFile = copyFile.checkFile(songId);
                if(checkFile == 1){
                    if(songAdd.getProdSongId() != null){
                        copyFile.CopyFileMp3(songId,songAdd.getProdSongId(),albumAdd.getProdAlbumId());
                        JdbcOracle ora = new JdbcOracle();
                        try{
                            Integer oraSongId = songAdd.getProdSongId();
                            List<SongModAdd> songMods = songModAddRepository.songModById(songId);
                            ora.EditSong(oraSongId,songAdd,checkAlbum.getMsg(),checkArtist.getMsg());
                            ora.UpdateSongMod(songId,oraSongId,songMods);
                            ora.InsertSongMod(songId,oraSongId,songMods);
                            if(songAdd.getMainSongYn().equals("Y")){
                                ora.setSongAlbum(checkAlbum.getMsg(),oraSongId);
                            }
                            insertProcedureRepository.approveSong(songAdd.getSongId(),oraSongId);
                            ora.ConClose();
                            response.setCode("1");
                            response.setMessage("Song Berhasil di Approve");
                        }catch(Exception ex){
                            ora.ConClose();
                            response.setCode("0");
                            response.setMessage(ex.getMessage());
                        }
                    }else{
                        JdbcOracle ora = new JdbcOracle();
                        try{
                            Integer oraSongId = ora.getSeqSong();
                            //Integer playTime = this.GetPlayTime(songAdd.getFullFilePath());
                            List<SongModAdd> songMods = songModAddRepository.songModById(songId);
                            ora.InsertSong(oraSongId,songAdd,checkAlbum.getMsg(),checkArtist.getMsg(),label.getLabelName(),albumAdd.getSearchKeyword());
                            copyFile.CopyFileMp3(songId,oraSongId,albumAdd.getProdAlbumId());
                            ora.InsertSongMod(songId,oraSongId,songMods);
                            if(songAdd.getMainSongYn().equals("Y")){
                                ora.setSongAlbum(checkAlbum.getMsg(),oraSongId);
                            }
                            insertProcedureRepository.approveSong(songAdd.getSongId(),oraSongId);

                            ora.ConClose();
                            response.setCode("1");
                            response.setMessage("Song Berhasil di Approve");
                        }catch (Exception ex){
                            ora.ConClose();
                            response.setCode("0");
                            response.setMessage(ex.getMessage());
                        }
                    }
                }else{
                    response.setCode("0");
                    response.setMessage("File Tidak Lengkap, Silahkan dicek kembali");
                }
            }else{
                if(checkArtist.getMsg() == 0 && checkAlbum.getMsg() != 0){
                    response.setCode("0");
                    response.setMessage("Artist Belum di Approve");
                }else if(checkArtist.getMsg() != 0 && checkAlbum.getMsg() == 0 ){
                    response.setCode("0");
                    response.setMessage("Album Belum di Approve");
                }else{
                    response.setCode("0");
                    response.setMessage("Album dan Artist Belum di Approve");
                }
            }
        }catch (Exception Ex){
            response.setCode("0");
            response.setMessage(Ex.getMessage());
        }
        return response;
    }

    private Integer GetPlayTime(String filename) {
        Integer playTime = 0;
        File song = new File(filename);
        try{
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(song);
            AudioFormat format = audioInputStream.getFormat();
            double frames = Math.ceil(audioInputStream.getFrameLength() / audioInputStream.getFormat().getFrameRate());
            playTime = (int) frames;
        }catch(IOException ioEx){
            log.info(ioEx.getMessage());
        }catch (UnsupportedAudioFileException uEx){
            log.info(uEx.getMessage());
        }
        if(playTime == 0){
            try{
                AudioFile f = AudioFileIO.read(song);
                AudioHeader tag = f.getAudioHeader();
                playTime = tag.getTrackLength();
            }catch (Exception ex){
                log.info(ex.getMessage());
            }
        }
        return playTime;

    }
}
