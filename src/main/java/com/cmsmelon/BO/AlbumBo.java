package com.cmsmelon.BO;

import com.cmsmelon.Model.*;
import com.cmsmelon.Repository.*;
import com.cmsmelon.helper.CopyFile;
import com.cmsmelon.helper.ImageResize;
import com.cmsmelon.helper.JdbcOracle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IT19 on 21/08/2017.
 */
@Component
public class AlbumBo {
    @Autowired
    AlbumRepository albumRepository;

    @Autowired
    AlbumAddRepository albumAddRepository;

    @Autowired
    CintRepository cintRepository;

    @Autowired
    private InsertProcedureRepository insertProcedureRepository;

    @Autowired
    private ArtistAddRepository artistAddRepository;

    @Autowired
    private AlbumArtistRepository albumArtistRepository;

    @Autowired
    private ImageResize imageResize;

    @Autowired
    private CopyFile copyFile;

    @Autowired
    private LabelRepository labelRepository;


    public PagingResponse<List<Album>> getAlbumList(Integer offset, String search, String category, String labelCd,String lcStatusCd){
        PagingResponse<List<Album>> response = new PagingResponse<>();
        response.setCode("0");
        response.setStatusCode(200);
        response.setMsg("Report generated successfully");
        response.setData(null);


        Cint cnt;
        List<Album> albumList = null;
        List<AlbumArtist> albumArtists;
        if(category.equals("album_name")){
            cnt = (Cint) cintRepository.listAlbumCntByName(search,lcStatusCd,labelCd);
            response.setTotalSize(cnt.getTotalSize());
            albumList =
                    albumRepository.listAlbumByName(offset, search,lcStatusCd,labelCd);
        }else{
            cnt = (Cint) cintRepository.listAlbumCntById(search,lcStatusCd,labelCd);
            response.setTotalSize(cnt.getTotalSize());
            albumList =
                    albumRepository.listAlbumById(offset, search,lcStatusCd,labelCd);
        }

        if(albumList!=null){
            response.setOffset(offset);
            response.setSize(albumList.size());
            response.setData(albumList);
        }else{
            response.setCode("1");
            response.setMsg("No data found in repository");
        }
        return response;
    }

    public Success AddAlbum(AlbumAdd album){
        Success response = new Success();
        try{
            //System.out.print(album.getAlbumId());
            if(album.getAlbumId() == null){
                imageResize.ResizeAllAlbumImage(album.getAlbumLImgPath(),album);
            }else{
                AlbumAdd albumAddx = albumAddRepository.albumById(album.getAlbumId());
                if( !album.getAlbumLImgPath().equals(albumAddx.getAlbumLImgPath())){
                    imageResize.ResizeAllAlbumImage(album.getAlbumLImgPath(),album);
                }
            }
            albumAddRepository.save(album);
            insertProcedureRepository.deleteAlbumArtist(album.getAlbumId());
            response.setCode("1");
            response.setMessage("Data berhasil disimpan|" + album.getAlbumId());
        }catch (Exception Ex){
            response.setCode("0");
            response.setMessage(Ex.getMessage());
        }
        return response;
    }

    public Success Delete(AlbumAdd albumAdd){
        Success response = null;
        try{
            albumAddRepository.delete(albumAdd);
            response.setCode("1");
            response.setMessage("Data berhasil dihapus");
        }catch (Exception Ex){
            response.setCode("0");
            response.setMessage(Ex.getMessage());
        }
        return response;
    }

    private  void InsertAlbumArtist(Integer albumId,Integer albumOraId, String albumName, String issueDate, JdbcOracle ora){
        List<AlbumArtist> albumArtists = albumArtistRepository.albumArtistByAlbum(albumId);
        if(albumArtists.size() > 0){
            for(AlbumArtist ss: albumArtists){
                ora.InsertArtistAlbum(ss.getProdArtistId(),albumOraId,ss.getArtistName(),albumName,issueDate);
            }
        }

    }
    public Success DeleteAlbum(Integer albumId){
        Success success = new Success();
        AlbumAdd albumAdd = albumAddRepository.findOne(albumId);
        if(albumAdd.getProdAlbumId() == null){
            albumAddRepository.delete(albumAdd.getAlbumId());
            copyFile.DeleteFile(albumAdd.getAlbumLImgPath());
            copyFile.DeleteFile(albumAdd.getAlbumMImgPath());
            copyFile.DeleteFile(albumAdd.getAlbumSImgPath());
            success.setCode("1");
            success.setMessage("Album berhasil dihapus");
        }else{
            success.setCode("0");
            success.setMessage("Album sudah diapprove tidak bisa dihapus");
        }
        return success;
    }
    public Success ApproveAlbum (Integer albumId){
        Success response = new Success();
        try{
            AlbumAdd albumAdd = new AlbumAdd();

            albumAdd = albumAddRepository.albumById(albumId);
            Label label = labelRepository.listLabelbyCode(albumAdd.getLabelCd());
            copyFile.CopyFileAlbumImg(albumAdd);
            //ArtistCrud artistCrud = artistAddRepository.artistById();
            InsertProcedure checkArtist = insertProcedureRepository.checkArtist(albumAdd.getMainArtistId());
                if(checkArtist.getMsg() > 0){
                JdbcOracle ora = new JdbcOracle();
                try{
                    Integer albumOraId = ora.ValidationAlbum(albumAdd.getAlbumName(),String.valueOf(checkArtist.getMsg()));
                    if(albumAdd.getProdAlbumId() != null){
                        ora.EditAlbum(albumAdd);
                        insertProcedureRepository.approvedAlbum(albumAdd.getAlbumId(),albumOraId);
                        ora.DeleteArtistAlbum(albumOraId);
                        this.InsertAlbumArtist(albumId, albumOraId, albumAdd.getAlbumName()
                                , albumAdd.getIssueDate(), ora);
                    }else{
                        if(albumOraId > 0){
                            insertProcedureRepository.approvedAlbum(albumAdd.getAlbumId(),albumOraId);
                            this.InsertAlbumArtist(albumId, albumOraId, albumAdd.getAlbumName()
                                    , albumAdd.getIssueDate(), ora);
                        }else{
                            //ArtistCrud artist = artistAddRepository.artistById(albumAdd.getMainArtistId());
                            Integer prodAlbumId = ora.getSeqAlbum();
                                if(ora.InsertAlbum
                                    (albumAdd,prodAlbumId,checkArtist.getMsg(),label.getLabelName())==1){
                                    this.InsertAlbumArtist(albumId,prodAlbumId, albumAdd.getAlbumName()
                                            , albumAdd.getIssueDate(), ora);
                                    insertProcedureRepository.approvedAlbum(albumAdd.getAlbumId(),prodAlbumId);
                                }

                        }
                    }

                    ora.ConClose();
                    response.setCode("1");
                    response.setMessage("Data berhasil diapprove");
                }catch (Exception ex){
                    ora.ConClose();
                    response.setCode("0");
                    response.setMessage(ex.getMessage());
                }

            }else{
                response.setCode("1");
                response.setMessage("Main Artist Belum di Approve");
            }

        }catch (Exception Ex){
            response.setCode("0");
            response.setMessage(Ex.getMessage());
        }
        return response;
    }
}
