package com.cmsmelon.Config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by IT19 on 27/09/2017.
 */
@Configuration
public class StaticResourceConfiguration extends WebMvcConfigurerAdapter {
    @Value( "${home.basedir}" )
    private String baseDir;
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/"+baseDir+"/**").addResourceLocations("file:///"+baseDir+"/");
//        registry.addResourceHandler("/music/**").addResourceLocations("file:///home/dceh/music/");
//        registry.addResourceHandler("/imagex/**").addResourceLocations("file:///mediaNASX/content/img/");
//        registry.addResourceHandler("/musix/**").addResourceLocations("file:///mediaNASX/content/mod/");
//        registry.addResourceHandler("/filex/**").addResourceLocations("file:///mediaNASX/exel/");

//        registry.addResourceHandler("/filex/**").addResourceLocations("file:///F:/");
//        registry.addResourceHandler("/images/**").addResourceLocations("file:///D:/Work/SourceCodeGit/cmsmelon/src/main/resources/static/upload/image/");
//        registry.addResourceHandler("/music/**").addResourceLocations("file:///D:/Work/SourceCodeGit/cmsmelon/src/main/resources/static/upload/music/");

//        registry.addResourceHandler("/mediaNAS1/**").addResourceLocations("file:///mediaNAS1/");
//        registry.addResourceHandler("/mediaNAS11/**").addResourceLocations("file:///mediaNAS11/");
//        registry.addResourceHandler("/mediaNAS12/**").addResourceLocations("file:///mediaNAS12/");
//        registry.addResourceHandler("/mediaNAS13/**").addResourceLocations("file:///mediaNAS13/");
//        registry.addResourceHandler("/mediaNAS2/**").addResourceLocations("file:///mediaNAS2/");
//        registry.addResourceHandler("/mediaNAS3/**").addResourceLocations("file:///mediaNAS3/");
//        registry.addResourceHandler("/mediaNAS4/**").addResourceLocations("file:///mediaNAS4/");
//        registry.addResourceHandler("/mediaNAS5/**").addResourceLocations("file:///mediaNAS5/");
//        registry.addResourceHandler("/mediaNAS6/**").addResourceLocations("file:///mediaNAS6/");
//        registry.addResourceHandler("/mediaNAS7/**").addResourceLocations("file:///mediaNAS7/");
//        registry.addResourceHandler("/mediaNAS8/**").addResourceLocations("file:///mediaNAS8/");
//        registry.addResourceHandler("/mediaNAS9/**").addResourceLocations("file:///mediaNAS9/");
//        registry.addResourceHandler("/mediaNASA/**").addResourceLocations("file:///mediaNASA/");
//        registry.addResourceHandler("/mediaNASB/**").addResourceLocations("file:///mediaNASB/");
//        registry.addResourceHandler("/mediaNASC/**").addResourceLocations("file:///mediaNASC/");
//        registry.addResourceHandler("/mediaNASD/**").addResourceLocations("file:///mediaNASD/");
//        registry.addResourceHandler("/mediaNASE/**").addResourceLocations("file:///mediaNASE/");
//        registry.addResourceHandler("/mediaNASF/**").addResourceLocations("file:///mediaNASF/");
//        registry.addResourceHandler("/mediaNASG/**").addResourceLocations("file:///mediaNASG/");
//        registry.addResourceHandler("/mediaNASH/**").addResourceLocations("file:///mediaNASH/");
//        registry.addResourceHandler("/mediaNASI/**").addResourceLocations("file:///mediaNASI/");
//        registry.addResourceHandler("/mediaNASJ/**").addResourceLocations("file:///mediaNASJ/");
//        registry.addResourceHandler("/mediaNASK/**").addResourceLocations("file:///mediaNASK/");
//        registry.addResourceHandler("/mediaNASL/**").addResourceLocations("file:///mediaNASL/");
//        registry.addResourceHandler("/mediaNASM/**").addResourceLocations("file:///mediaNASM/");
//        registry.addResourceHandler("/mediaNASN/**").addResourceLocations("file:///mediaNASN/");
//        registry.addResourceHandler("/mediaNASO/**").addResourceLocations("file:///mediaNASO/");
//        registry.addResourceHandler("/mediaNASP/**").addResourceLocations("file:///mediaNASP/");
//        registry.addResourceHandler("/mediaNASQ/**").addResourceLocations("file:///mediaNASQ/");
//        registry.addResourceHandler("/mediaNASR/**").addResourceLocations("file:///mediaNASR/");
//        registry.addResourceHandler("/mediaNASS/**").addResourceLocations("file:///mediaNASS/");
//        registry.addResourceHandler("/mediaNAST/**").addResourceLocations("file:///mediaNAST/");
//        registry.addResourceHandler("/mediaNASU/**").addResourceLocations("file:///mediaNASU/");
//        registry.addResourceHandler("/mediaNASV/**").addResourceLocations("file:///mediaNASV/");
//        registry.addResourceHandler("/mediaNASW/**").addResourceLocations("file:///mediaNASW/");
//        registry.addResourceHandler("/mediaNASX/**").addResourceLocations("file:///mediaNASX/");

        registry.addResourceHandler("/mediaNASB/**").addResourceLocations("file:///mediaNASB/");
        registry.addResourceHandler("/mediaNAS14/**").addResourceLocations("file:///mediaNAS14/");
        registry.addResourceHandler("/mediaNASR/**").addResourceLocations("file:///mediaNASR/");
        registry.addResourceHandler("/mediaNAS12/**").addResourceLocations("file:///mediaNAS12/");
        registry.addResourceHandler("/mediaNASC/**").addResourceLocations("file:///mediaNASC/");
        registry.addResourceHandler("/mediaNASP/**").addResourceLocations("file:///mediaNASP/");
        registry.addResourceHandler("/mediaNASE/**").addResourceLocations("file:///mediaNASE/");
        registry.addResourceHandler("/mediaNASM/**").addResourceLocations("file:///mediaNASM/");
        registry.addResourceHandler("/mediaNAS13/**").addResourceLocations("file:///mediaNAS13/");
        registry.addResourceHandler("/mediaNAS15/**").addResourceLocations("file:///mediaNAS15/");
        registry.addResourceHandler("/mediaNASN/**").addResourceLocations("file:///mediaNASN/");
        registry.addResourceHandler("/mediaNASF/**").addResourceLocations("file:///mediaNASF/");
        registry.addResourceHandler("/mediaNASU/**").addResourceLocations("file:///mediaNASU/");
        registry.addResourceHandler("/mediaNASL/**").addResourceLocations("file:///mediaNASL/");
        registry.addResourceHandler("/mediaNAS16/**").addResourceLocations("file:///mediaNAS16/");
        registry.addResourceHandler("/mediaNAS11/**").addResourceLocations("file:///mediaNAS11/");
        registry.addResourceHandler("/mediaNASH/**").addResourceLocations("file:///mediaNASH/");
        registry.addResourceHandler("/mediaNAS5/**").addResourceLocations("file:///mediaNAS5/");
        registry.addResourceHandler("/mediaNAS8/**").addResourceLocations("file:///mediaNAS8/");
        registry.addResourceHandler("/mediaNASQ/**").addResourceLocations("file:///mediaNASQ/");
        registry.addResourceHandler("/mediaNASK/**").addResourceLocations("file:///mediaNASK/");
        registry.addResourceHandler("/mediaNASO/**").addResourceLocations("file:///mediaNASO/");
        registry.addResourceHandler("/mediaNASJ/**").addResourceLocations("file:///mediaNASJ/");
        registry.addResourceHandler("/mediaNASG/**").addResourceLocations("file:///mediaNASG/");
        registry.addResourceHandler("/mediaNAS2/**").addResourceLocations("file:///mediaNAS2/");
        registry.addResourceHandler("/mediaNASD/**").addResourceLocations("file:///mediaNASD/");
        registry.addResourceHandler("/mediaNAS3/**").addResourceLocations("file:///mediaNAS3/");
        registry.addResourceHandler("/mediaNAS4/**").addResourceLocations("file:///mediaNAS4/");
        registry.addResourceHandler("/mediaNAS9/**").addResourceLocations("file:///mediaNAS9/");
        registry.addResourceHandler("/mediaNASW/**").addResourceLocations("file:///mediaNASW/");
        registry.addResourceHandler("/mediaNASS/**").addResourceLocations("file:///mediaNASS/");
        registry.addResourceHandler("/mediaNASV/**").addResourceLocations("file:///mediaNASV/");
        registry.addResourceHandler("/mediaNAST/**").addResourceLocations("file:///mediaNAST/");
        registry.addResourceHandler("/mediaNASA/**").addResourceLocations("file:///mediaNASA/");
        registry.addResourceHandler("/mediaNAS6/**").addResourceLocations("file:///mediaNAS6/");
        registry.addResourceHandler("/mediaNASI/**").addResourceLocations("file:///mediaNASI/");
        registry.addResourceHandler("/mediaNAS1/**").addResourceLocations("file:///mediaNAS1/");
        registry.addResourceHandler("/mediaNAS7/**").addResourceLocations("file:///mediaNAS7/");
        registry.addResourceHandler("/mediaNASX/**").addResourceLocations("file:///mediaNASX/");


    }
}
