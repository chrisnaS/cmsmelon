package com.cmsmelon.Repository;

import com.cmsmelon.Model.ArtistCrud;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by IT19 on 05/09/2017.
 */
public interface ArtistAddRepository extends CrudRepository<ArtistCrud,Integer> {
    @Query(nativeQuery = true)
    ArtistCrud artistById(
            @Param("artistId") Integer artistId);
}
