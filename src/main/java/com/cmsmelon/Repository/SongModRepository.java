package com.cmsmelon.Repository;

import com.cmsmelon.Model.SongMod;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by IT19 on 27/09/2017.
 */
public interface SongModRepository extends CrudRepository<SongMod,Integer> {
    @Query(nativeQuery = true)
    List<SongMod> listSongMod(@Param("songId") int songId);
}
