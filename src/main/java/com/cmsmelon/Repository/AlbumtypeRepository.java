package com.cmsmelon.Repository;

import com.cmsmelon.Model.AlbumType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

/**
 * Created by IT19 on 07/09/2017.
 */
public interface AlbumtypeRepository extends Repository<AlbumType,String>{
    @Query(nativeQuery = true)
    List<AlbumType> listAlbumType();
}
