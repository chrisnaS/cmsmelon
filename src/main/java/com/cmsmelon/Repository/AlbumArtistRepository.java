package com.cmsmelon.Repository;

import com.cmsmelon.Model.AlbumArtist;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by IT19 on 11/10/2017.
 */
public interface AlbumArtistRepository extends Repository<AlbumArtist,String>
{
    List<AlbumArtist> albumArtistByAlbum(
            @Param("albumId") int albumId);
}
