package com.cmsmelon.Repository;

import com.cmsmelon.Model.MlbCode;
import com.cmsmelon.Model.MlbDetail;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MlbDetailRepository extends Repository<MlbDetail,String> {
    @Query(nativeQuery = true)
    List<MlbDetail> listMlbDetail(
            @Param("txId") int txId);
}
