package com.cmsmelon.Repository;

import com.cmsmelon.Model.Album;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by IT19 on 21/08/2017.
 */
public interface AlbumRepository extends Repository<Album,Integer> {
    @Query(nativeQuery = true)
    List<Album> listAlbumByName(
        @Param("offset") int offset,
        @Param("search") String search,
        @Param("lcStatusCd") String lcStatusCd,
        @Param("labelCd") String labelCd);

    @Query(nativeQuery = true)
    List<Album> listAlbumById(
            @Param("offset") int offset,
            @Param("search") String search,
            @Param("lcStatusCd") String lcStatusCd,
            @Param("labelCd") String labelCd);
}
