package com.cmsmelon.Repository;

import com.cmsmelon.Model.Nationality;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

/**
 * Created by IT19 on 16/10/2017.
 */
public interface NationalityRepository extends Repository<Nationality,String> {
    @Query(nativeQuery = true)
    List<Nationality> listNationality();
}

