package com.cmsmelon.Repository;

import com.cmsmelon.Model.Cint;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

/**
 * Created by IT19 on 21/08/2017.
 */
public interface CintRepository extends Repository<Cint,Integer> {
    @Query(nativeQuery = true)
    Cint listAlbumCntByName(@Param("search") String search,@Param("lcStatusCd") String lcStatusCd,@Param("labelCd") String labelCd);

    @Query(nativeQuery = true)
    Cint listAlbumCntById(@Param("search") String search,@Param("lcStatusCd") String lcStatusCd,@Param("labelCd") String labelCd);

    @Query(nativeQuery = true)
    Cint listArtistCntByName(@Param("search") String search,@Param("lcStatusCd") String lcStatusCd);

    @Query(nativeQuery = true)
    Cint listArtistCntById(@Param("search") String search,@Param("lcStatusCd") String lcStatusCd);

    @Query(nativeQuery = true)
    Cint listUsers(@Param("search") String search);

    @Query(nativeQuery = true)
    Cint listSongCntByName(@Param("search") String search,
                           @Param("isCommercialYn") String isCommercialYn,
                           @Param("labelCd") String labelCd,
                           @Param("lcStatusCd") String lcStatusCd);

    @Query(nativeQuery = true)
    Cint listSongCntById(@Param("search") String search,
                         @Param("isCommercialYn") String isCommercialYn,
                         @Param("labelCd") String labelCd,
                         @Param("lcStatusCd") String lcStatusCd);

    @Query(nativeQuery = true)
    Cint listAdminSongCntById(@Param("search") String search,
                           @Param("isCommercialYn") String isCommercialYn);

    @Query(nativeQuery = true)
    Cint listAdminSongCntByName(@Param("search") String search,
                         @Param("isCommercialYn") String isCommercialYn);
    @Query(nativeQuery = true)
    Cint listAdminSongCntByLabel(@Param("search") String search,
                           @Param("isCommercialYn") String isCommercialYn);

    @Query(nativeQuery = true)
    Cint listMlb(@Param("search") String search);

    @Query(nativeQuery = true)
    Cint listMlbLabelList(@Param("search") String search);

    @Query(nativeQuery = true)
    Cint filterArtistName(@Param("search") String search);

}
