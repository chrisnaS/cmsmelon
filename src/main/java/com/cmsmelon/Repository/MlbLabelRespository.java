package com.cmsmelon.Repository;

import com.cmsmelon.Model.MlbLabel;
import com.cmsmelon.Model.SongModAdd;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MlbLabelRespository extends CrudRepository<MlbLabel,String>{
    @Query(nativeQuery = true)
    List<MlbLabel> listMlbLabel();

    @Query(nativeQuery = true)
    List<MlbLabel> listMlbLabelJoin(
            @Param("search") String search,
            @Param("offset") int offset
    );
}
