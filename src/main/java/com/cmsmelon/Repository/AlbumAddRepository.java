package com.cmsmelon.Repository;

import com.cmsmelon.Model.AlbumAdd;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by IT19 on 04/09/2017.
 */
public interface AlbumAddRepository extends CrudRepository<AlbumAdd,Integer> {
    @Query(nativeQuery = true)
    AlbumAdd albumById(
            @Param("albumId") Integer albumId);
}
