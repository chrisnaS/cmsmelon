package com.cmsmelon.Repository;

import com.cmsmelon.Model.Users;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by IT19 on 19/09/2017.
 */
public interface UsersRepository extends CrudRepository<Users,Integer>{
    @Query(nativeQuery = true)
    List<Users> Login(@Param("username") String username,
                      @Param("password") String password);
    @Query(nativeQuery = true)
    Users listById(@Param("id") Integer id);

    @Query(nativeQuery = true)
    Users listByUsername(@Param("username") String username);
}
