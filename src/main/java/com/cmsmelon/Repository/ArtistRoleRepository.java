package com.cmsmelon.Repository;

import com.cmsmelon.Model.ArtistRole;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

/**
 * Created by IT19 on 08/09/2017.
 */
public interface ArtistRoleRepository extends Repository<ArtistRole,String>{
    @Query(nativeQuery = true)
    List<ArtistRole> listArtistRole();
}
