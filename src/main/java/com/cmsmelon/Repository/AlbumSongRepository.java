package com.cmsmelon.Repository;

import com.cmsmelon.Model.AlbumSong;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IT19 on 09/10/2017.
 */
@Component
public interface AlbumSongRepository extends Repository <AlbumSong,Integer>{
    @Query(nativeQuery = true)
    List<AlbumSong> listAlbumSong(
            @Param("labelCd") String labelCd,
            @Param("albumId") String albumId);
}
