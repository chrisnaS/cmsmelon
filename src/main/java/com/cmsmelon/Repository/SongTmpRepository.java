package com.cmsmelon.Repository;

import com.cmsmelon.Model.Song;
import com.cmsmelon.Model.SongTmp;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SongTmpRepository extends Repository<SongTmp,Integer> {
    @Query(nativeQuery = true)
    SongTmp listSongModQue();
}
