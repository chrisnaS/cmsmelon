package com.cmsmelon.Repository;

import com.cmsmelon.Model.UserView;
import com.cmsmelon.Model.Users;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface UserViewRepository extends Repository<UserView,Integer>{
    @Query(nativeQuery = true)
    List<UserView> getUsers(@Param("offset") int offset,
                            @Param("search") String search);
}
