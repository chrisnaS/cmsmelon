package com.cmsmelon.Repository;

import com.cmsmelon.Model.Genre;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

/**
 * Created by IT19 on 08/09/2017.
 */
public interface GenreRepository extends Repository<Genre,String>{
    @Query(nativeQuery = true)
    List<Genre> listGenre();
}
