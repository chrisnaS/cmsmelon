package com.cmsmelon.Repository;

import com.cmsmelon.Model.SongAdd;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by IT19 on 30/08/2017.
 */
public interface SongAddRepository extends CrudRepository<SongAdd,Integer> {
    @Query(nativeQuery = true)
    SongAdd songById(
            @Param("songId") Integer songId);
}
