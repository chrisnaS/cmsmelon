package com.cmsmelon.Repository;

import com.cmsmelon.Model.InsertProcedure;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

/**
 * Created by IT19 on 09/10/2017.
 */
public interface InsertProcedureRepository extends Repository<InsertProcedure,String>{
    @Query(nativeQuery = true)
    InsertProcedure saveAlbumArtist(
            @Param("albumId") Integer albumId,
            @Param("artistId") Integer 	artistId
            );
    @Query(nativeQuery = true)
    InsertProcedure deleteAlbumArtist(
            @Param("albumId") Integer albumId
    );

    @Query(nativeQuery = true)
    InsertProcedure approvedSong(
            @Param("songId") Integer songId,
            @Param("lcStatusCd") String lcStatusCd
    );

    @Query(nativeQuery = true)
    InsertProcedure approvedArtist(
            @Param("songId") Integer songId,
            @Param("prodArtistId") Integer prodArtistId
    );

    @Query(nativeQuery = true)
    InsertProcedure approvedAlbum(
            @Param("albumId") Integer albumId,
            @Param("prodAlbumId") Integer prodAlbumId
    );

    @Query(nativeQuery = true)
    InsertProcedure checkArtist(
            @Param("artistId") Integer artistId
    );

    @Query(nativeQuery = true)
    InsertProcedure checkAlbum(
            @Param("albumId") Integer albumId
    );

    @Query(nativeQuery = true)
    InsertProcedure approveSong(
            @Param("songId") Integer songId,
            @Param("prodSongId") Integer prodSongId
    );

    @Query(nativeQuery = true)
    InsertProcedure setMainAlbum(
            @Param("songId") Integer songId,
            @Param("albumId") Integer albumId
    );

    @Query(nativeQuery = true)
    InsertProcedure setSongEncoder(
            @Param("songId") Integer songId,
            @Param("category") String category
    );

    @Query(nativeQuery = true)
    InsertProcedure insTxMlb(
    );

    @Query(nativeQuery = true)
    InsertProcedure insTdMlb(
            @Param("txId") Integer txId,
            @Param("labelCode") String labelCode,
            @Param("jumlah") Integer jumlah
    );

    @Query(nativeQuery = true)
    InsertProcedure updTxMlb(
            @Param("txId") Integer txId,
            @Param("fileUrl") String fileUrl
    );
}
