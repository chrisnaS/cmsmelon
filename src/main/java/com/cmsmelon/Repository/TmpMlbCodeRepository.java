package com.cmsmelon.Repository;

import com.cmsmelon.Model.TmpMlbCode;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TmpMlbCodeRepository extends Repository<TmpMlbCode,String>{
    @Query(nativeQuery = true)
    List<TmpMlbCode> getData(@Param("txId") Integer txId);
}
