package com.cmsmelon.Repository;
import com.cmsmelon.Model.Dashboard;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

/**
 * Created by IT19 on 20/10/2017.
 */
public interface DashboardRepository extends Repository<Dashboard,Integer>{
    @Query(nativeQuery = true)
    Dashboard listDashboard(@Param("labelCd") String labelCd);
}
