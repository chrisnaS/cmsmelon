package com.cmsmelon.Repository;

import com.cmsmelon.Model.Label;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by IT19 on 07/09/2017.
 */
public interface LabelRepository extends CrudRepository<Label,String> {
    @Query(nativeQuery = true)
    List<Label> listLabel();

    @Query(nativeQuery = true)
    Label listLabelbyCode(@Param("labelCd") String labelCd);
}
