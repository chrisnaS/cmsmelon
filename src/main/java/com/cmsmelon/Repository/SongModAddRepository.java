package com.cmsmelon.Repository;

import com.cmsmelon.Model.SongModAdd;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by IT19 on 25/10/2017.
 */
public interface SongModAddRepository extends CrudRepository<SongModAdd,Integer> {
    @Query(nativeQuery = true)
    List<SongModAdd> songModById(@Param("songId") int songId);
}
