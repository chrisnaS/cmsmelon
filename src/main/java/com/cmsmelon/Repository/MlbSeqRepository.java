package com.cmsmelon.Repository;

import com.cmsmelon.Model.MblSeq;
import org.springframework.data.repository.CrudRepository;

public interface MlbSeqRepository extends CrudRepository<MblSeq,String> {

}
