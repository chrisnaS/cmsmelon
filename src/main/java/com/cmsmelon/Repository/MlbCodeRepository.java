package com.cmsmelon.Repository;

import com.cmsmelon.Model.Album;
import com.cmsmelon.Model.Cint;
import com.cmsmelon.Model.MlbCode;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MlbCodeRepository extends Repository<MlbCode,Integer> {
    @Query(nativeQuery = true)
    List<MlbCode> listMlb(
            @Param("offset") int offset,
            @Param("search") String search);
    @Query(nativeQuery = true)
    MlbCode listGenerateMlb();
}
