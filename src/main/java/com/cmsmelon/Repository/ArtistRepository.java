package com.cmsmelon.Repository;

import com.cmsmelon.Model.Artist;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by IT19 on 29/08/2017.
 */
public interface ArtistRepository extends Repository<Artist,Integer> {
//    @Query(nativeQuery = true)
//    List<Artist> listArtistByName(
//            @Param("offset") int offset,
//            @Param("search") String search,
//            @Param("labelCd") String labelCd);
//
//    @Query(nativeQuery = true)
//    List<Artist> listArtistById(
//            @Param("offset") int offset,
//            @Param("search") String search,
//            @Param("labelCd") String labelCd);
    @Query(nativeQuery = true)
    List<Artist> listArtistByName(
            @Param("offset") int offset,
            @Param("search") String search,
            @Param("lcStatusCd") String lcStatusCd);

    @Query(nativeQuery = true)
    List<Artist> listArtistById(
            @Param("offset") int offset,
            @Param("search") String search,
            @Param("lcStatusCd") String lcStatusCd);
}
