package com.cmsmelon.Repository;

import com.cmsmelon.Model.Song;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by IT19 on 29/08/2017.
 */
public interface SongRepository extends Repository<Song,Integer> {
    @Query(nativeQuery = true)
    List<Song> listSongByName(@Param("offset") int offset,
                              @Param("search") String search,
                              @Param("isCommercialYn") String isCommercialYn,
                              @Param("labelCd") String labelCd,
                              @Param("lcStatusCd") String lcStatusCd);

    @Query(nativeQuery = true)
    List<Song> listSongById(@Param("offset") int offset,
                            @Param("search") String search,
                            @Param("isCommercialYn") String isCommercialYn,
                            @Param("labelCd") String labelCd,
                            @Param("lcStatusCd") String lcStatusCd);

    List<Song> listAdminSongByName(@Param("offset") int offset,
                              @Param("search") String search,
                              @Param("isCommercialYn") String isCommercialYn);

    @Query(nativeQuery = true)
    List<Song> listAdminSongById(@Param("offset") int offset,
                            @Param("search") String search,
                            @Param("isCommercialYn") String isCommercialYn);

    @Query(nativeQuery = true)
    List<Song> listAdminSongByLabel(@Param("offset") int offset,
                                 @Param("search") String search,
                                 @Param("isCommercialYn") String isCommercialYn);

    @Query(nativeQuery = true)
    List<Song> listAdminSongByAlbum(@Param("albumId") Integer albumId);
}
