package com.cmsmelon.controller;

import com.cmsmelon.BO.MlbBo;
import com.cmsmelon.BO.SongBo;
import com.cmsmelon.Model.*;
import com.cmsmelon.Repository.*;
import com.cmsmelon.helper.JdbcMlb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Created by IT19 on 29/08/2017.
 */
@RestController
public class SongController {
    @Autowired
    private SongBo songBo;

    @Autowired
    private SongRepository songRepository;

    @Autowired
    private SongModRepository songModRepository;

    @Autowired
    private InsertProcedureRepository insertProcedureRepository;

    @Autowired
    private SongAddRepository songAddRepository;

    @Autowired
    private MlbBo mlbBo;

    @Autowired
    private MlbDetailRepository mlbDetailRepository;


    @RequestMapping(value = "/song/list",method = RequestMethod.GET,
            produces = "application/json")
    private PagingResponse<List<Song>> getSongList(
            @RequestParam(name = "offset",required = true) Integer offset,
            @RequestParam(name = "category",required = true) String category,
            @RequestParam(name = "labelCd",required = true) String labelCd,
            @RequestParam(name = "search",required = false) String search,
            @RequestParam(name = "isCommercialYn",required = false) String isCommercialYn,
            @RequestParam(name = "lcStatusCd",required = false) String lcStatusCd
    ){
        return songBo.getSongList(offset,search,category,isCommercialYn,labelCd,lcStatusCd);
    }
    @RequestMapping(value = "/song/listByAlbum",method = RequestMethod.GET,
            produces = "application/json")
    private List<Song> getSongListByAlbum(
            @RequestParam(name = "albumId",required = false) Integer albumId
    ){
        System.out.println(albumId);
        return songRepository.listAdminSongByAlbum(albumId);
    }

    @RequestMapping(value = "/song/getMlbCode",method = RequestMethod.GET,
            produces = "application/json")
    private PagingResponse<List<MlbModel>> getMlbsong(
            @RequestParam(name = "searchSong",defaultValue="",required = false) String searchSong,
            @RequestParam(name = "searchArtist",defaultValue="",required = false) String searchArtist,
            @RequestParam(name = "offset",defaultValue="0",required = false) Integer offset,
            @RequestParam(name = "limit",defaultValue="10",required = false) Integer limit
    ) throws SQLException {
        JdbcMlb jdbcMlb = new JdbcMlb();

        return jdbcMlb.getSearchMlb(searchSong,searchArtist,offset,limit);
    }

    @RequestMapping(value = "/adminsong/list",method = RequestMethod.GET,
            produces = "application/json")
    private PagingResponse<List<Song>> getSongAdminList(
            @RequestParam(name = "offset",required = true) Integer offset,
            @RequestParam(name = "category",required = true) String category,
            @RequestParam(name = "search",required = false) String search,
            @RequestParam(name = "isCommercialYn",required = false) String isCommercialYn
    ){
        return songBo.getAdminSongList(offset,search,category,isCommercialYn);
    }

    @RequestMapping(value = "/songMod/list",method = RequestMethod.GET,
            produces = "application/json")
    private List<SongMod> getSongModList(
            @RequestParam(name = "songId",required = true) Integer songId
    ){
        return songModRepository.listSongMod(songId);
    }

    @RequestMapping(value = "/song/add",method = RequestMethod.POST,
            produces = "application/json")
    private Success addSong(
            @RequestParam(name = "songId",required = false) Integer songId,
            @RequestParam(name = "songName",required = true) String songName,
            @RequestParam(name = "songNameOrigin",required = true) String songNameOrigin,
            @RequestParam(name = "mlbCd",required = true) String mlbCd,
            @RequestParam(name = "genidCd",required = false) String genidCd,
            @RequestParam(name = "hint",required = false) String hint,
            @RequestParam(name = "diskNo",required = false) Integer diskNo,
            @RequestParam(name = "trackNo",required = false) Integer trackNo,
            @RequestParam(name = "playTime",required = false) Integer playTime,
            @RequestParam(name = "titleSongYn",required = false) String titleSongYn,
            @RequestParam(name = "hitSongYn",required = false) String hitSongYn,
            @RequestParam(name = "modYn",required = false) String modYn,
            @RequestParam(name = "vodYn",required = false) String vodYn,
            @RequestParam(name = "rtYn",required = false) String rtYn,
            @RequestParam(name = "rbtYn",required = false) String rbtYn,
            @RequestParam(name = "textLyricYn",required = false) String textLyricYn,
            @RequestParam(name = "slfLyricYn",required = false) String slfLyricYn,
            @RequestParam(name = "genreId",required = true) String genreId,
            @RequestParam(name = "albumId",required = true) Integer albumId,
            @RequestParam(name = "artistId",required = true) Integer artistId,
            @RequestParam(name = "issueDate",required = false) String issueDate,
            @RequestParam(name = "orderIssueDate",required = false) String orderIssueDate,
            @RequestParam(name = "sellStreamYn",required = false) String sellStreamYn,
            @RequestParam(name = "sellDrmYn",required = false) String sellDrmYn,
            @RequestParam(name = "sellNonDrmYn",required = false) String sellNonDrmYn,
            @RequestParam(name = "sellAlacarteYn",required = false) String sellAlacarteYn,
            @RequestParam(name = "drmPaymentProdId",required = false) String drmPaymentProdId,
            @RequestParam(name = "drmPrice",required = false) Integer drmPrice,
            @RequestParam(name = "nonDrmPaymentProdId",required = false) String nonDrmPaymentProdId,
            @RequestParam(name = "nonDrmPrice",required = false) String nonDrmPrice,
            @RequestParam(name = "searchKeyword",required = false) String searchKeyword,
            @RequestParam(name = "adultYn",required = false) String adultYn,
            @RequestParam(name = "lcStatusCd",required = false) String lcStatusCd,
            @RequestParam(name = "workerId",required = false) String workerId,
            @RequestParam(name = "isCommercialYn",required = false) String isCommercialYn,
            @RequestParam(name = "originalFileName",required = false) String originalFileName,
            @RequestParam(name = "fullFilePath",required = false) String fullFilePath,
            @RequestParam(name = "lyricPath",required = false) String lyricPath,
            @RequestParam(name = "cp1Cd",required = false) String cp1Cd,
            @RequestParam(name = "cp2Cd",required = false) String cp2Cd,
            @RequestParam(name = "cp3Cd",required = false) String cp3Cd,
            @RequestParam(name = "cp4Cd",required = false) String cp4Cd,
            @RequestParam(name = "cp5Cd",required = false) String cp5Cd,
            @RequestParam(name = "labelCd",required = false) String labelCd,
            @RequestParam(name = "mainSongYn",required = false) String mainSongYn
    ) throws IOException, UnsupportedAudioFileException {
        String   isCm = "N";
        SongAdd song = new SongAdd();
        if(songId != null){
            //song.setSongId(songId);
            song = songAddRepository.songById(songId);
            song.setUpdDate(new Date());
            if(song.getLcStatusCd().equals("CS0001")){
                song.setLcStatusCd("CS0001");
                isCm = "N";
            }else if(song.getLcStatusCd().equals("CS0006")){
                song.setLcStatusCd("CS0002");
                isCm = "N";
            }

        }else{
            song.setRegDate(new Date());
            song.setLabelCd(labelCd);
            song.setLcStatusCd(lcStatusCd);
            isCm = "N";
        }
//        if(isCommercialYn != null){
//            isCm = isCommercialYn;
//        }

        song.setCp1Cd(cp1Cd);
        song.setCp2Cd(cp2Cd);
        song.setCp3Cd(cp3Cd);
        song.setCp4Cd(cp4Cd);
        song.setCp5Cd(cp5Cd);
        song.setOriginalFileName(originalFileName);
        song.setFullFilePath(fullFilePath);
        song.setLyricPath(lyricPath);
        song.setIsCommercialYn(isCm);
        song.setSongName(songName);
        song.setSongNameOrigin(songNameOrigin);
        song.setMlbCd(mlbCd);
        song.setGenidCd(genidCd);
        song.setHint(hint);
        song.setDiskNo(diskNo);
        song.setTrackNo(trackNo);
        song.setPlayTime(playTime);
        song.setTitleSongYn(titleSongYn);
        song.setHitSongYn(hitSongYn);
        song.setModYn(modYn);
        song.setVodYn(vodYn);
        song.setRtYn(rtYn);
        song.setRbtYn(rbtYn);
        song.setTextLyricYn(textLyricYn);
        song.setSlfLyricYn(slfLyricYn);
        song.setGenreId(genreId);
        song.setAlbumId(albumId);
        song.setArtistId(artistId);
        song.setIssueDate(issueDate);
        song.setOrderIssueDate(orderIssueDate);
        song.setSellStreamYn(sellStreamYn);
        song.setSellDrmYn(sellDrmYn);
        song.setSellNonDrmYn(sellNonDrmYn);
        song.setSellAlacarteYn(sellAlacarteYn);
        song.setDrmPaymentProdId(drmPaymentProdId);
        song.setDrmPrice(drmPrice);
        song.setNonDrmPaymentProdId(nonDrmPaymentProdId);
        song.setNonDrmPrice(nonDrmPrice);
        song.setSearchKeyword(searchKeyword);
        song.setAdultYn(adultYn);

        song.setWorkerId(workerId);
        song.setMainSongYn(mainSongYn);



        return songBo.AddSong(song);
    }

    @RequestMapping(value = "/songMod/add",method = RequestMethod.POST,
            produces = "application/json")
    private Success addSongMod(
            @RequestParam(name = "songModId",required = true) Integer songModId,
            @RequestParam(name = "songId",required = true) String songId,
            @RequestParam(name = "sampling",required = false) Integer sampling,
            @RequestParam(name = "codecTypeCd",required = true) String codecTypeCd,
            @RequestParam(name = "bitRateCd",required = false) String bitRateCd,
            @RequestParam(name = "playTime",required = false) Integer playTime,
            @RequestParam(name = "fileSize",required = false) Integer fileSize,
            @RequestParam(name = "originalFileName",required = false) String originalFileName,
            @RequestParam(name = "fullFilePath",required = false) String fullFilePath,
            @RequestParam(name = "workerId",required = false) String workerId,
            @RequestParam(name = "status",required = false) String status
    ) throws IOException, UnsupportedAudioFileException {

        SongModAdd songmod = new SongModAdd();
        songmod.setSongModId(songModId);
        songmod.setSongId(songId);
        songmod.setSampling(sampling);
        songmod.setCodecTypeCd(codecTypeCd);
        songmod.setBitRateCd(bitRateCd);
        songmod.setPlayTime(playTime);
        songmod.setFileSize(fileSize);
        songmod.setOriginalFileName(originalFileName);
        songmod.setFullFilePath(fullFilePath);
        songmod.setStatus(status);
        songmod.setWorkerId(workerId);
        songmod.setRegDate(new Date());
        songmod.setUpdDate(new Date());

        return songBo.AddSongMod(songmod);
    }

    @RequestMapping(value = "/song/approvedSong",method = RequestMethod.POST,
            produces = "application/json")
    private InsertProcedure addAlbumArtist(
            @RequestParam(name = "songId",required = true) Integer songId,
            @RequestParam(name = "lcStatusCd",required = true) String lcStatusCd
    ){
        InsertProcedure rslt = new InsertProcedure();
        try {
            rslt = insertProcedureRepository.approvedSong(songId, lcStatusCd);
        }catch (Exception ex){
            System.out.print(ex.getMessage());
        }
        return rslt;
    }

    @RequestMapping(value = "/song/approve",method = RequestMethod.POST,
            produces = "application/json")
    private Success addArtist(
            @RequestParam(name = "songId",required = true) Integer songId
    ){
        return songBo.ApproveSong(songId);
    }
    @RequestMapping(value = "/song/approvebulk",method = RequestMethod.POST,
            produces = "application/json")
    private Success addArtist(
            @RequestParam(name = "songIds",required = true) String songIds
    ){
        Success response = new Success();
        String msg = "Notification \n";
        String songId [] = songIds.split("-");
        for(int z = 0;z <= songId.length-1;z++){
            if(!songId[z].equals("")){
                Integer songIdx = Integer.parseInt(songId[z]);
                Success resp = new Success();
                        resp = songBo.ApproveSong(songIdx);
                        if(resp.getCode().equals("1")){
                            msg += "Song ID "+songIdx + " berhasil di Approve \n";
                        }else{
                            msg += "Song ID "+songIdx + " gagal di Approve("+resp.getMessage()+") \n";
                        }
            }
        }
        response.setCode("1");
        response.setMessage(msg);
        return response;
        //return songBo.ApproveSong(songId);
    }

    @RequestMapping(value = "/song/delete/{songId}",method = RequestMethod.POST,
            produces = "application/json")
    private Success deleteSong(
            @PathVariable("songId") Integer songId
    ){
        Success response = new Success();
        String msg = "Notification \n";

        response.setCode("1");
        response.setMessage(msg);
        return songBo.DeleteSong(songId);
    }

    @RequestMapping(value = "/songMod/job",method = RequestMethod.POST,
            produces = "application/json")
    private Success songModJob(
            @RequestParam(name = "songId",required = true) Integer songId
    ) throws IOException, UnsupportedAudioFileException {
        Success response = new Success();
        Success resp = new Success();
        resp = songBo.SetSongModJob(songId,"INS");
        return resp;
    }

    @RequestMapping(value = "/mlb_code/add",method = RequestMethod.POST,
            produces = "application/json")
    private Success mlbCodeAdd(
            @RequestParam(name = "params",required = true) String params
    ) throws IOException, UnsupportedAudioFileException {
        Success response = new Success();
        Success resp = new Success();
        resp = mlbBo.SaveMlbCode(params);
        return resp;
    }

    @RequestMapping(value = "/mlb_code/list",method = RequestMethod.GET,
            produces = "application/json")
    private PagingResponse<List<MlbCode>> getMlbCodeList(
            @RequestParam(name = "offset",required = true) Integer offset,
            @RequestParam(name = "search",required = false) String search
    ){
        return mlbBo.getMlbCodeList(offset,search);
    }

    @RequestMapping(value = "/mlb_code/getlabel",method = RequestMethod.GET,
            produces = "application/json")
    private List<MlbLabel> getMlbCodeList(
    ){
        return mlbBo.getMlbLabel();
    }

    @RequestMapping(value = "/mlb_code/detail",method = RequestMethod.GET,
            produces = "application/json")
    private List<MlbDetail> getMlbDetail(
            @RequestParam(name = "txId",required = true) Integer txId
    ){
        return mlbDetailRepository.listMlbDetail(txId);
    }
}
