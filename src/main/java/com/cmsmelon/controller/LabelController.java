package com.cmsmelon.controller;

import com.cmsmelon.BO.MlbBo;
import com.cmsmelon.Model.*;
import com.cmsmelon.Repository.LabelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by IT19 on 07/09/2017.
 */
@RestController
public class LabelController {
    @Autowired
    private LabelRepository labelRepository;
    @Autowired
    private MlbBo mlbBo;

    @RequestMapping(value = "/label/list",method = RequestMethod.GET,
            produces = "application/json")
    private List<Label> getLabelList(
            @RequestParam(name = "offset",required = false) Integer offset,
            @RequestParam(name = "category",required = false) String category,
            @RequestParam(name = "search",required = false) String search
    ){
        return labelRepository.listLabel();
    }

    @RequestMapping(value = "/label/add",method = RequestMethod.POST,
            produces = "application/json")
    private Success InsertLabel(
            @RequestParam(name = "labelCode",required = true) String labelCode,
            @RequestParam(name = "labelName",required = true) String labelName,
            @RequestParam(name = "initial",required = true) String initial
    ){
        return mlbBo.saveLabel(labelCode,labelName,initial);
    }

    @RequestMapping(value = "/label/mlb/list",method = RequestMethod.GET,
            produces = "application/json")
    private PagingResponse<List<MlbLabel>> GetMblLabelList(
            @RequestParam(name = "offset",required = true) Integer offset,
            @RequestParam(name = "search",required = false) String search
    ){

        return mlbBo.getMlbLabelList(offset,search);
    }
}
