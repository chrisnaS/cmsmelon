package com.cmsmelon.controller;

import com.cmsmelon.Model.Label;
import com.cmsmelon.Model.Nationality;
import com.cmsmelon.Repository.LabelRepository;
import com.cmsmelon.Repository.NationalityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by IT19 on 21/08/2017.
 */
@RestController
public class DataController {
    @Autowired
    private NationalityRepository nationalityRepository;

    @Autowired
    private LabelRepository labelRepository;
//
    @RequestMapping(value = "/nationality/list",method = RequestMethod.GET,
            produces = "application/json")
    private List<Nationality> getNationality(
    ){

        return nationalityRepository.listNationality();
    }

    @RequestMapping(value = "/label/test",method = RequestMethod.GET,
            produces = "application/json")
    private Label getNationality(
            @RequestParam(name = "labelCd",required = false) String labelCd
    ){

        return labelRepository.listLabelbyCode(labelCd);
    }

}
