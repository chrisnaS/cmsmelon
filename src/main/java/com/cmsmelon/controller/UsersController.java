package com.cmsmelon.controller;

import com.cmsmelon.BO.UserBO;
import com.cmsmelon.Model.*;
import com.cmsmelon.Repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IT19 on 19/09/2017.
 */
@RestController
public class UsersController {
    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private UserBO userBO;

    @RequestMapping(value = "/auth/user",method = RequestMethod.POST,
            produces = "application/json")
    private List<Users> getLogin(
            @RequestParam(name = "userName",required = true) String userName,
            @RequestParam(name = "password",required = true) String password
    ){
        return usersRepository.Login(userName,password);
    }

    @RequestMapping(value = "/user/add",method = RequestMethod.POST,
            produces = "application/json")
    private Success addArtist(
            @RequestParam(name = "userId",required = false) Integer userId,
            @RequestParam(name = "username",required = true) String username,
            @RequestParam(name = "password",required = true) String password,
            @RequestParam(name = "labelCd",required = true) String labelCd,
            @RequestParam(name = "fullname",required = true) String fullname,
            @RequestParam(name = "roleType",required = true) String roleType
    ){
                Users users = new Users();
                if(userId != null){
                    users = usersRepository.listById(userId);
                }
                users.setUserName(username);
                users.setUserPassword(password);
                users.setLabelCd(labelCd);
                users.setFullName(fullname);
                users.setRoleType(roleType);

        return userBO.AddUser(users);
    }

    @RequestMapping(value = "/users/list",method = RequestMethod.GET,
            produces = "application/json")
    private PagingResponse<List<UserView>> getUserList(
            @RequestParam(name = "offset",required = true) Integer offset,
            @RequestParam(name = "search",required = false) String search
    ){

        return userBO.getUser(offset,search);
    }

    @RequestMapping(value = "/users/{username}",method = RequestMethod.GET,
            produces = "application/json")
    private Users getUserListId(
            @PathVariable("username") String username
    ){
        return usersRepository.listByUsername(username);
    }

    @RequestMapping(value = "/user/changePass",method = RequestMethod.POST,
            produces = "application/json")
    private Success changePass(
            @RequestParam(name = "userId",required = true) Integer userId,
            @RequestParam(name = "newPass",required = true) String newPass
    ){
        Users users = new Users();
        users = usersRepository.listById(userId);

        users.setUserPassword(newPass);

        return userBO.AddUser(users);
    }
}
