package com.cmsmelon.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by MelOnDev1 on 04/06/2017.
 */
@Controller
public class IndexController {

    @RequestMapping(name = "index",value = "/")
    public String index(){
        return "login";
    }

    @RequestMapping(name = "home",value = "/homes")
    public String home(){
        return "index";
    }

    @RequestMapping(name = "upload",value = "/upload")
    public String upload(){
        return "upload";
    }
}
