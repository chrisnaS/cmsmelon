package com.cmsmelon.controller;

import com.cmsmelon.Model.Dashboard;
import com.cmsmelon.Repository.DashboardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IT19 on 20/10/2017.
 */
@RestController
public class DashboardController {
    @Autowired
    private DashboardRepository dashboardRepository;

    @RequestMapping(value = "/dashboard/list",method = RequestMethod.GET,
            produces = "application/json")
    private Dashboard getDashboard(
            @RequestParam(name = "labelCd",required = false) String labelCd
    ){

        return dashboardRepository.listDashboard(labelCd);
    }
}
