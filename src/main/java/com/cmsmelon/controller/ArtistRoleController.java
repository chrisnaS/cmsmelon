package com.cmsmelon.controller;

import com.cmsmelon.Model.ArtistRole;
import com.cmsmelon.Repository.ArtistRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by IT19 on 08/09/2017.
 */
@RestController
public class ArtistRoleController {
    @Autowired
    private ArtistRoleRepository artistRoleRepository;

    @RequestMapping(value="artistRole/list",method = RequestMethod.GET,
            produces = "application/json")
    private List<ArtistRole> getListAlbumType(
            @RequestParam(name = "offset",required = false) Integer offset,
            @RequestParam(name = "category",required = false) String category,
            @RequestParam(name = "search",required = false) String search
    ){
        return artistRoleRepository.listArtistRole();
    }
}
