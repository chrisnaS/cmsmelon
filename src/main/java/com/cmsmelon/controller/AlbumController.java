package com.cmsmelon.controller;

import com.cmsmelon.BO.AlbumBo;
import com.cmsmelon.Model.*;
import com.cmsmelon.Repository.AlbumAddRepository;
import com.cmsmelon.Repository.AlbumArtistRepository;
import com.cmsmelon.Repository.AlbumSongRepository;
import com.cmsmelon.Repository.InsertProcedureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * Created by IT19 on 21/08/2017.
 */
@RestController
public class AlbumController {
    @Autowired
    private AlbumBo albumBo;

    @Autowired
    private AlbumSongRepository albumSongRepository;

    @Autowired
    private InsertProcedureRepository insertProcedureRepository;

    @Autowired
    private AlbumArtistRepository albumArtistRepository;

    @Autowired
    private AlbumAddRepository albumAddRepository;

    @RequestMapping(value = "/album/list",method = RequestMethod.GET,
            produces = "application/json")
    private PagingResponse<List<Album>> getAlbumList(
            @RequestParam(name = "offset",required = true) Integer offset,
            @RequestParam(name = "category",required = true) String category,
            @RequestParam(name = "labelCd",required = true) String labelCd,
            @RequestParam(name = "search",required = false) String search,
            @RequestParam(name = "lcStatusCd",required = false) String lcStatusCd
    ){
        if(lcStatusCd == null){
            lcStatusCd = "";
        }
        return albumBo.getAlbumList(offset,search,category,labelCd,lcStatusCd);
    }
    @RequestMapping(value = "/album/artist",method = RequestMethod.GET,
            produces = "application/json")
    private List<AlbumArtist> getAlbumArtist(
            @RequestParam(name = "albumId",required = true) Integer albumId
    ){

        return albumArtistRepository.albumArtistByAlbum(albumId);
    }


    @RequestMapping(value = "/album/songAvailabel",method = RequestMethod.GET,
            produces = "application/json")
    private List<AlbumSong> getAlbumSongList(
            @RequestParam(name = "labelCd",required = false) String labelCd,
            @RequestParam(name = "albumId",required = false) String albumId
    ){
                if(albumId == null){
                    albumId = "";
                }
        return albumSongRepository.listAlbumSong(labelCd,albumId);
    }

    @RequestMapping(value = "/album/insertAlbumArtist",method = RequestMethod.POST,
            produces = "application/json")
    private InsertProcedure addAlbumArtist(
            @RequestParam(name = "artistId",required = true) Integer artistId,
            @RequestParam(name = "albumId",required = true) Integer albumId
    ){
        InsertProcedure rslt = new InsertProcedure();
        try {
            rslt = insertProcedureRepository.saveAlbumArtist(albumId, artistId);
        }catch (Exception ex){
            System.out.print(ex.getMessage());
        }
        return rslt;
    }

    @RequestMapping(value = "/album/deleteAlbumArtist",method = RequestMethod.POST,
            produces = "application/json")
    private InsertProcedure deleteAlbumArtist(
            @RequestParam(name = "albumId",required = true) Integer albumId
    ){
        return insertProcedureRepository.deleteAlbumArtist(albumId);
    }

    @RequestMapping(value = "/album/add",method = RequestMethod.POST,
            produces = "application/json")
    private Success addAlbum(
            @RequestParam(name = "albumId",required = false) Integer albumId,
            @RequestParam(name = "albumName",required = true) String albumName,
            @RequestParam(name = "albumNameOrigin",required = true) String albumNameOrigin,
            @RequestParam(name = "mainSongId",required = false) String mainSongId,
            @RequestParam(name = "mainArtistId",required = false) Integer mainArtistId,
            @RequestParam(name = "hint",required = false) String hint,
            @RequestParam(name = "albumTypeCd",required = false) String albumTypeCd,
            @RequestParam(name = "seriesNo",required = false) String seriesNo,
            @RequestParam(name = "diskCnt",required = false) String diskCnt,
            @RequestParam(name = "totalSongCnt",required = false) String totalSongCnt,
            @RequestParam(name = "issueDate",required = false) String issueDate,
            @RequestParam(name = "orderIssueDate",required = false) String orderIssueDate,
            @RequestParam(name = "adultYn",required = false) String adultYn,
            @RequestParam(name = "domesticYn",required = false) String domesticYn,
            @RequestParam(name = "issueCountryCd",required = false) String issueCountryCd,
            @RequestParam(name = "genreId",required = false) String genreId,
            @RequestParam(name = "agency",required = false) String agency,
            @RequestParam(name = "sellCompany",required = false) String sellCompany,
            @RequestParam(name = "searchKeyword",required = false) String searchKeyword,
            @RequestParam(name = "albumLImgPath",required = false) String albumLImgPath,
            @RequestParam(name = "albumMImgPath",required = false) String albumMImgPath,
            @RequestParam(name = "albumSImgPath",required = false) String albumSImgPath,
            @RequestParam(name = "albumReview",required = false) String albumReview,
            @RequestParam(name = "RecommendCnt",required = false) Integer RecommendCnt,
            @RequestParam(name = "opposeCnt",required = false) Integer opposeCnt,
            @RequestParam(name = "status",required = false) String status,
            @RequestParam(name = "labelCd",required = false) String labelCd,
            @RequestParam(name = "channelGroupCd",required = false) String channelGroupCd,
            @RequestParam(name = "workerId",required = false) String workerId,
            @RequestParam(name = "regDate",required = false) Date regDate,
            @RequestParam(name = "updDate",required = false) Date updDate
            ){

        AlbumAdd albumAdd = new AlbumAdd();
        if(albumId != null){
            //albumAdd.setAlbumId(albumId);
            albumAdd = albumAddRepository.albumById(albumId);
            albumAdd.setUpdDate(new Date());
            albumAdd.setLcStatusCd("CS0001");
        }else{
            albumAdd.setRegDate(new Date());
            albumAdd.setLabelCd(labelCd);
            albumAdd.setLcStatusCd("CS0001");
        }
        if(diskCnt.equals("")){
            diskCnt = null;
        }


        albumAdd.setAlbumName(albumName);
        albumAdd.setAlbumNameOrigin(albumNameOrigin);
        albumAdd.setMainSongId(mainSongId);
        albumAdd.setMainArtistId(mainArtistId);
        albumAdd.setHint(hint);
        albumAdd.setAlbumTypeCd(albumTypeCd);
        albumAdd.setSeriesNo(seriesNo);
        albumAdd.setDiskCnt(diskCnt);
        albumAdd.setTotalSongCnt(totalSongCnt);
        albumAdd.setIssueDate(issueDate);
        albumAdd.setOrderIssueDate(orderIssueDate);
        if(adultYn == null){
            albumAdd.setAdultYn("N");
        }else{
            albumAdd.setAdultYn(adultYn);
        }
        if(domesticYn == null){
            albumAdd.setDomesticYn("Y");
        }else{
            albumAdd.setDomesticYn(domesticYn);
        }

        albumAdd.setIssueCountryCd(issueCountryCd);
        albumAdd.setGenreId(genreId);
        albumAdd.setAgency(agency);
        albumAdd.setSellCompany(sellCompany);
        albumAdd.setSearchKeyword(searchKeyword);
        albumAdd.setAlbumLImgPath(albumLImgPath);
//        albumAdd.setAlbumMImgPath(albumMImgPath);
//        albumAdd.setAlbumSImgPath(albumSImgPath);
        albumAdd.setAlbumReview(albumReview);
        if(RecommendCnt == null){
            albumAdd.setRecommendCnt(0);
        }else{
            albumAdd.setRecommendCnt(RecommendCnt);
        }

        if(opposeCnt == null){
            albumAdd.setOpposeCnt(0);
        }else{
            albumAdd.setOpposeCnt(opposeCnt);
        }

        if(status == null){
            albumAdd.setStatus("A");
        }else{
            albumAdd.setStatus(status);
        }

        albumAdd.setChannelGroupCd(channelGroupCd);
        albumAdd.setWorkerId(workerId);
        //albumAdd.setRegDate(new Date());


        return albumBo.AddAlbum(albumAdd);
    }

    @RequestMapping(value = "/album/approve",method = RequestMethod.POST,
            produces = "application/json")
    private Success addArtist(
            @RequestParam(name = "albumId",required = true) Integer albumId
    ){
        return albumBo.ApproveAlbum(albumId);
    }

    @RequestMapping(value = "/album/delete/{albumId}",method = RequestMethod.POST,
            produces = "application/json")
    private Success deleteAlbum(
            @PathVariable("albumId") Integer albumId
    ){
        return albumBo.DeleteAlbum(albumId);
    }

    @RequestMapping(value = "/album/approvebulk",method = RequestMethod.POST,
            produces = "application/json")
    private Success approveBulk(
            @RequestParam(name = "albumIds",required = true) String albumIds
    ){
        Success response = new Success();
        String msg = "Notification \n";
        String albumId [] = albumIds.split("-");
        for(int z = 0;z <= albumId.length-1;z++){
            if(!albumId[z].equals("")){
                Integer albumIdx = Integer.parseInt(albumId[z]);
                Success resp = new Success();
                resp = albumBo.ApproveAlbum(albumIdx);
                if(resp.getCode().equals("1")){
                    msg += "Album ID "+albumIdx + " berhasil di Approve \n";
                }else{
                    msg += "Album ID "+albumIdx + " gagal di Approve("+resp.getMessage()+") \n";
                }
            }
        }
        response.setCode("1");
        response.setMessage(msg);
        return response;
        //return songBo.ApproveSong(songId);
    }
}
