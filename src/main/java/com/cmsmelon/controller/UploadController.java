package com.cmsmelon.controller;

import com.cmsmelon.Model.Success;
import com.cmsmelon.storage.StorageFileNotFoundException;
import com.cmsmelon.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.stream.Collectors;

/**
 * Created by IT19 on 07/09/2017.
 */
@Controller
public class UploadController {
    private final StorageService storageService;

    @Value( "${home.dir}" )
    private String pathHome;

    @Autowired
    public UploadController(StorageService storageService) {
        this.storageService = storageService;
    }

    @GetMapping("/fileUpload")
    public String listUploadedFiles(Model model) throws IOException {

        model.addAttribute("files", storageService.loadAll().map(
                path -> MvcUriComponentsBuilder.fromMethodName(UploadController.class,
                        "serveFile", path.getFileName().toString()).build().toString())
                .collect(Collectors.toList()));

        return "upload";
    }

    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @PostMapping("/fileUpload")
    @ResponseBody
    public Success handleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) {
        Success rslt = new Success();
        try{
            System.out.println(pathHome);
            String filenamex = StringUtils.cleanPath(file.getOriginalFilename());

            String [] ext =  filenamex.split("\\.");
            DateFormat df = new SimpleDateFormat("yyyyMMddhhmmss");
            Date today = Calendar.getInstance().getTime();
            Integer cint = ext.length - 1;
            String filename = df.format(today) + "." + ext[cint];
            String Type = "IMG";
            String filePath = pathHome+"image/";
//            String filePath ="/upload/image/";
            if(ext[cint].equals("mp3") || ext[cint].equals("wav")){
                Type = "MP3";
                //filePath = "/upload/music/";
                filePath = pathHome+"music/";
            }else if(ext[cint].equals("txt")){
                Type = "LIRYC";
//                filePath = "/upload/lyric/";
                filePath = pathHome+"lyric/";
            }
            storageService.store(file,filename, Type);
//            redirectAttributes.addFlashAttribute("message",
//                    "You successfully uploaded " + file.getOriginalFilename() + "!");
            rslt.setCode("1");
            rslt.setMessage(filePath + filename);
        }catch(Exception Ex){
            rslt.setCode("0");
            rslt.setMessage(Ex.getMessage());
        }

        return rslt;
    }

    @PostMapping("/fileDelete")
    @ResponseBody
    public Success deleteFileUpload(@RequestParam(name = "filename",required = true) String filename) {
        Success rslt = new Success();
        try{
            storageService.deleteOne(filename);
//            redirectAttributes.addFlashAttribute("message",
//                    "You successfully uploaded " + file.getOriginalFilename() + "!");
            rslt.setCode("1");
            rslt.setMessage("Delete Success");
        }catch(Exception Ex){
            rslt.setCode("0");
            rslt.setMessage(Ex.getMessage());
        }

        return rslt;
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }
}
