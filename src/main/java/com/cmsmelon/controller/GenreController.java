package com.cmsmelon.controller;

import com.cmsmelon.Model.Genre;
import com.cmsmelon.Repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by IT19 on 08/09/2017.
 */
@RestController
public class GenreController {
    @Autowired
    private GenreRepository genreRepository;

    @RequestMapping(value="genre/list", method = RequestMethod.GET,
                    produces="application/json")
    private List<Genre> getListGenre(
            @RequestParam(name = "offset",required = false) Integer offset,
            @RequestParam(name = "category",required = false) String category,
            @RequestParam(name = "search",required = false) String search
    ){
        return genreRepository.listGenre();
    }
}
