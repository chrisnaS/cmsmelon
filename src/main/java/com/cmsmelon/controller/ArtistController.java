package com.cmsmelon.controller;

import com.cmsmelon.BO.ArtistBo;
import com.cmsmelon.Model.*;
import com.cmsmelon.Repository.ArtistAddRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Created by IT19 on 29/08/2017.
 */
@RestController
public class ArtistController {
    @Autowired
    private ArtistBo artistBo;

    @Autowired
    private ArtistAddRepository artistAddRepository;

    @RequestMapping(value = "/artist/list",method = RequestMethod.GET,
            produces = "application/json")
    private PagingResponse<List<Artist>> getAlbumList(
            @RequestParam(name = "offset",required = true) Integer offset,
            @RequestParam(name = "category",required = true) String category,
            @RequestParam(name = "labelCd",required = true) String labelCd,
            @RequestParam(name = "search",required = false) String search,
            @RequestParam(name = "lcStatusCd",required = false) String lcStatusCd
    ){
        if(lcStatusCd == null){
            lcStatusCd = "";
        }
        return artistBo.getArtistList(offset,search,category,labelCd,lcStatusCd);
    }

    @RequestMapping(value = "/artist/add",method = RequestMethod.POST,
            produces = "application/json")
    private Success addArtist(
            @RequestParam(name = "artistId",required = false) Integer artistId,
            @RequestParam(name = "artistName",required = true) String artistName,
            @RequestParam(name = "artistNameOrigin",required = true) String artistNameOrigin,
            @RequestParam(name = "nickname",required = false) String nickname,
            @RequestParam(name = "hint",required = false) String hint,
            @RequestParam(name = "groupYn",required = false) String groupYn,
            @RequestParam(name = "artistRoleTypeCd",required = false) String artistRoleTypeCd,
            @RequestParam(name = "domesticYn",required = false) String domesticYn,
            @RequestParam(name = "nationalityCd",required = false) String nationalityCd,
            @RequestParam(name = "genreId",required = false) String genreId,
            @RequestParam(name = "birthday",required = false) String birthday,
            @RequestParam(name = "deathday",required = false) String deathday,
            @RequestParam(name = "debutDay",required = false) String debutDay,
            @RequestParam(name = "debutSong",required = false) String debutSong,
            @RequestParam(name = "performYears",required = false) String performYears,
            @RequestParam(name = "gender",required = false) String gender,
            @RequestParam(name = "bloodType",required = false) String bloodType,
            @RequestParam(name = "religion",required = false) String religion,
            @RequestParam(name = "hobby",required = false) String hobby,
            @RequestParam(name = "artistPriority",required = false) Integer artistPriority,
            @RequestParam(name = "homepage",required = false) String homepage,
            @RequestParam(name = "twitterUrl",required = false) String twitterUrl,
            @RequestParam(name = "facebookUrl",required = false) String facebookUrl,
            @RequestParam(name = "searchYn",required = false) String searchYn,
            @RequestParam(name = "searchKeyword",required = false) String searchKeyword,
            @RequestParam(name = "profile",required = false) String profile,
            @RequestParam(name = "artistLImgPath",required = false) String artistLImgPath,
            @RequestParam(name = "artistMImgPath",required = false) String artistMImgPath,
            @RequestParam(name = "artistSImgPath",required = false) String artistSImgPath,
            @RequestParam(name = "status",required = false) String status,
            @RequestParam(name = "labelCd",required = false) String labelCd,
            @RequestParam(name = "channelGroupCd",required = false) String channelGroupCd,
            @RequestParam(name = "workerId",required = false) String workerId,
            @RequestParam(name = "regDate",required = false) Date regDate,
            @RequestParam(name = "updDate",required = false) Date updDate
            ){

        ArtistCrud artistCrud = new ArtistCrud();
        if(artistId != null){
            artistCrud = artistAddRepository.artistById(artistId);
            artistCrud.setArtistId(artistId);
            artistCrud.setUpdDate(new Date());
        }else{
            artistCrud.setRegDate(new Date());
            artistCrud.setLabelCd(labelCd);
        }

        artistCrud.setArtistName(artistName);
        artistCrud.setArtistNameOrigin(artistNameOrigin);
        artistCrud.setNickname(nickname);
        artistCrud.setHint(hint);
        if(groupYn == null){
            artistCrud.setGroupYn("N");
        }else{
            artistCrud.setGroupYn(groupYn);
        }

        artistCrud.setArtistRoleTypeCd(artistRoleTypeCd);
        if(domesticYn == null){
            artistCrud.setDomesticYn("Y");
        }else{
            artistCrud.setDomesticYn(domesticYn);
        }

        artistCrud.setNationalityCd(nationalityCd);
        artistCrud.setGenreId(genreId);
        artistCrud.setBirthday(birthday);
        artistCrud.setDeathday(deathday);
        artistCrud.setDebutDay(debutDay);
        artistCrud.setDebutSong(debutSong);
        artistCrud.setPerformYears(performYears);

        if(gender == null){
            artistCrud.setGender("M");
        }else{
            artistCrud.setGender(gender);
        }
        artistCrud.setBloodType(bloodType);
        artistCrud.setReligion(religion);
        artistCrud.setHobby(hobby);
        artistCrud.setArtistPriority(artistPriority);
        artistCrud.setHomepage(homepage);
        artistCrud.setTwitterUrl(twitterUrl);
        artistCrud.setFacebookUrl(facebookUrl);
        if(searchYn == null){
            artistCrud.setSearchYn("Y");
        }else{
            artistCrud.setSearchYn(searchYn);
        }
        artistCrud.setSearchKeyword(searchKeyword);
        artistCrud.setProfile(profile);
        artistCrud.setArtistLImgPath(artistLImgPath);
        artistCrud.setArtistMImgPath(artistMImgPath);
        artistCrud.setArtistSImgPath(artistSImgPath);
        if(status == null){
            artistCrud.setStatus("A");
        }else{
            artistCrud.setStatus(status);
        }
        artistCrud.setChannelGroupCd(channelGroupCd);
        artistCrud.setWorkerId(workerId);
        System.out.println(artistCrud);
        return artistBo.AddArtist(artistCrud);
    }

    @RequestMapping(value = "/artist/approve",method = RequestMethod.POST,
            produces = "application/json")
    private Success addArtist(
            @RequestParam(name = "artistId",required = true) Integer artistId
    ) throws SQLException, ClassNotFoundException {
        return artistBo.ApproveArtist(artistId);
    }
}
